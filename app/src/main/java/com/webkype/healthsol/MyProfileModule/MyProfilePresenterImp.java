package com.webkype.healthsol.MyProfileModule;

import android.content.Context;

import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.utility.Preference;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.webkype.healthsol.utility.PrefManager.USER_ID;

public class MyProfilePresenterImp implements MyProfilePresenter, MyProfileInteractor.onFinishedListener {


    private final String userId;
    MyProfileView myProfileView;
    MyProfileInteractor myProfileInteractorImp;
    Context context;

    public MyProfilePresenterImp(MyProfileView myProfileView, MyProfileInteractorImp myProfileInteractorImp, Context context) {


        this.myProfileView = myProfileView;
        this.myProfileInteractorImp = myProfileInteractorImp;
        this.context = context;
        userId = Preference.getString(context, USER_ID);

    }

    @Override
    public void onFailed(String s) {
        myProfileView.hideViewProgress();
        if (myProfileView != null) {
            myProfileView.onFailed(s);
        }
    }

    @Override
    public void onSuccess(ShowMemberList showMemberList) {

        myProfileView.hideViewProgress();
        if (myProfileView != null) {
            myProfileView.recivedUserData(showMemberList);
        }
    }

    @Override
    public void onUpdateSuccess(String msg) {

        if (myProfileView != null) {
            myProfileView.hideViewProgress();
            myProfileView.onUpdateSuccecss(msg);
        }
    }

    @Override
    public void getPreviousUserData(String userId) {

        if (myProfileInteractorImp != null) {
            myProfileView.showViewProgress();
            myProfileInteractorImp.fetchData(userId, this);
        }

    }

    @Override
    public void updateUserProfile(String request_url, MultipartBody.Part fileToUpload, HashMap<String, RequestBody> hashMap) {

        if (myProfileInteractorImp != null) {
            myProfileView.showViewProgress();
            myProfileInteractorImp.submitUserData(userId, request_url, fileToUpload, hashMap, this);
        }
    }

    @Override
    public void prepareAllList() {

        ArrayList<String> maritialSatus, bloodGroup, genderList;

        maritialSatus = new ArrayList<>();
        bloodGroup = new ArrayList<>();

        genderList = new ArrayList<>();

        maritialSatus.add("Single");
        maritialSatus.add("Married");

        bloodGroup.add("O-");
        bloodGroup.add("O+");
        bloodGroup.add("A-");
        bloodGroup.add("A+");
        bloodGroup.add("B-");
        bloodGroup.add("B+");
        bloodGroup.add("AB-");
        bloodGroup.add("AB+");


        genderList.add("Male");
        genderList.add("Female");

    }

}
