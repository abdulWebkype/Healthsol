package com.webkype.healthsol.MyProfileModule;

import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;

import java.util.ArrayList;

public interface MyProfileView {


    void recivedUserData(ShowMemberList showMemberList);


    void showViewProgress();
    void hideViewProgress();

    void onFailed(String s);

    void onUpdateSuccecss(String msg);

}
