package com.webkype.healthsol.MyProfileModule;


import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface MyProfilePresenter {

   void getPreviousUserData(String userId);

   void updateUserProfile(String request_url, MultipartBody.Part fileToUpload, HashMap<String, RequestBody> hashMap);

    void prepareAllList();
}
