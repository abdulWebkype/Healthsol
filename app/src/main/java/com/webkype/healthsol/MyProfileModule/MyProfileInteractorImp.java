package com.webkype.healthsol.MyProfileModule;

import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberMain;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileInteractorImp implements MyProfileInteractor {


    ApiInterface apiInterface = new Preference().getInstance();

    @Override
    public void fetchData(String userId, onFinishedListener listener) {


        Call<ShowMemberMain> mainCall = apiInterface.showMemberProfile(userId, userId);

        mainCall.enqueue(new Callback<ShowMemberMain>() {
            @Override
            public void onResponse(Call<ShowMemberMain> call, Response<ShowMemberMain> response) {

                if (response != null) {
                    ShowMemberMain showMemberMain = response.body();
                    String status = showMemberMain.getStatus();
                    String header = showMemberMain.getHeader();
                    ShowMemberList showMemberList = showMemberMain.getMemberProfile();
                    if (showMemberList != null) {
                        listener.onSuccess(showMemberList);
                    }
                }
            }

            @Override
            public void onFailure(Call<ShowMemberMain> call, Throwable t) {

                listener.onFailed(t.toString());
            }
        });
    }

    @Override
    public void submitUserData(String userId, String request_url, MultipartBody.Part fileToUpload, HashMap<String,
            RequestBody> hashMap, onFinishedListener listener) {

        Call<ResponseBody> bodyCall = apiInterface.addMemeber(request_url, fileToUpload, hashMap);

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    String res = null;
                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String statu = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        listener.onUpdateSuccess(msg);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onFailed(t.toString());
            }
        });
    }
}
