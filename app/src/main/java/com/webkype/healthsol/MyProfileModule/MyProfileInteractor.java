package com.webkype.healthsol.MyProfileModule;

import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface MyProfileInteractor {

    void fetchData(String userId,onFinishedListener listener);

    void submitUserData(String userId, String request_url, MultipartBody.Part fileToUpload, HashMap<String,
            RequestBody> hashMap, onFinishedListener listener);

    interface onFinishedListener
    {
        void onFailed(String s);

        void onSuccess(ShowMemberList showMemberList);

        void onUpdateSuccess(String msg);
    }

}
