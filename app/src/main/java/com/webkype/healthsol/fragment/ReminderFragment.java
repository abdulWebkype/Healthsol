package com.webkype.healthsol.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.activity.AddNewReminder;
import com.webkype.healthsol.adapter.RecyclerAdapterReminder;
import com.webkype.healthsol.model.ReminderListPojo;
import com.webkype.healthsol.model.ReminderMainPojo;
import com.webkype.healthsol.model.ReminderModel;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ReminderFragment extends Fragment {

    private RecyclerView reminderRecyclerView;
    private RecyclerAdapterReminder recyclerAdapterReminder;
    private List<ReminderModel> reminderModelList = new ArrayList<>();
    ApiInterface apiInterface = new Preference().getInstance();
    String userId;

    RelativeLayout addRemindButton;

    ProgressDialog progressDialog;

    public static Fragment newInstance() {
        ReminderFragment reminderFragment = new ReminderFragment();
        return reminderFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reminder, container, false);
        reminderRecyclerView = view.findViewById(R.id.reminderRecyclerView);
        addRemindButton = view.findViewById(R.id.addRemindButton);

        reminderRecyclerView.setHasFixedSize(false);
        reminderRecyclerView.setFocusable(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        reminderRecyclerView.setLayoutManager(layoutManager);
        reminderRecyclerView.setItemAnimator(new DefaultItemAnimator());
        reminderRecyclerView.setNestedScrollingEnabled(false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        userId = Preference.getString(getActivity(), PrefManager.USER_ID);
        getReminderList();


        addRemindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddNewReminder.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void getReminderList() {
        progressDialog.show();

        apiInterface.getReminderList(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ReminderMainPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ReminderMainPojo value) {
                        progressDialog.dismiss();
                        if (value != null) {
                            String status = value.getStatus();
                            String msg = value.getMsg();

                            List<ReminderListPojo> list = value.getReminderList();

                            recyclerAdapterReminder = new RecyclerAdapterReminder(getActivity(), list,apiInterface,userId);

                            reminderRecyclerView.setAdapter(recyclerAdapterReminder);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });


    }


}
