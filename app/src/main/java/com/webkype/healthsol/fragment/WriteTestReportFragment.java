package com.webkype.healthsol.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.AddTestResultAdapter;
import com.webkype.healthsol.adapter.DescriptiveRecyclerAdapter;
import com.webkype.healthsol.adapter.OuterRecyclerAdapter;
import com.webkype.healthsol.model.DescriptiveModel;
import com.webkype.healthsol.model.TestDetails;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.utility.CommonUtils;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;
import com.webkype.healthsol.utility.TestReportBodyClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WriteTestReportFragment extends Fragment {

    @BindView(R.id.descripTopCard)
    CardView descripTopCard;

    @BindView(R.id.staticText2)
    TextView staticText2;

    @BindView(R.id.staticText3)
    TextView staticText3;

    @BindView(R.id.patientNameTest_tV)
    TextView patientNameTestTV;

    @BindView(R.id.staticText6)
    TextView staticText6;

    @BindView(R.id.staticText7)
    TextView staticText7;

    @BindView(R.id.staticText4)
    TextView staticText4;

    @BindView(R.id.staticText5)
    TextView staticText5;

    @BindView(R.id.staticText12)
    TextView staticText12;

    @BindView(R.id.staticText13)
    TextView staticText13;

    @BindView(R.id.staticText8)
    TextView staticText8;

    @BindView(R.id.staticText9)
    TextView staticText9;

    @BindView(R.id.selectDateTest_tV)
    EditText selectDateTest_tV;

    @BindView(R.id.staticText10)
    TextView staticText10;

    @BindView(R.id.staticText11)
    TextView staticText11;

    @BindView(R.id.addTextCategoryTxt)
    TextView addTextCategoryTxt;

    @BindView(R.id.addDescriptiveTxt)
    TextView addDescriptiveTxt;

    @BindView(R.id.prescriptionFormCard)
    CardView prescriptionFormCard;

    @BindView(R.id.controlButtonsLayout)
    RelativeLayout controlButtonsLayout;


    @BindView(R.id.addMoreRow)
    TextView addMoreRow;


    @BindView(R.id.testCategoryLayout)
    LinearLayout testCategoryLayout;

    @BindView(R.id.descriptiveTestLayout)
    LinearLayout descriptiveTestLayout;

    @BindView(R.id.testReportOuterRecycler)
    RecyclerView testReportRecycler;


    @BindView(R.id.submitTestButton)
    Button submitTestButton;

    @BindView(R.id.labNameTest_Edit)
    EditText labNameTest_Edit;
    @BindView(R.id.presBy_Edit)
    EditText presBy_Edit;
    @BindView(R.id.sample_Edit)
    EditText sample_Edit;

    @BindView(R.id.remarkTest_tV)
    EditText remarkTest_tV;

    @BindView(R.id.testCatEdit)
    EditText testCatEdit;


    @BindView(R.id.addMoreCategory1)
    TextView addMoreCategory1;

    @BindView(R.id.writeTestHiText)
    TextView writeTestHiText;

    @BindView(R.id.descrTestRecycler)
    RecyclerView descrTestRecycler;

    @BindView(R.id.addMoreDescriptive)
    Button addMoreDescriptive;

    private AddTestResultAdapter addMedicineRecyclerAdapter;

    Unbinder unbinder;

    int counter = 0;

    ArrayList<DescriptiveModel> descriptiveModelArrayList = new ArrayList<>();


    private DatePickerDialog datePickerDialog;
    private List<TestDetails> medicineList;
    private List<TestDetails> testDetailsList;
    private List<List<TestDetails>> allTestList;
    private LayoutInflater layoutInflater;
    private ApiInterface apiInterface;
    private ProgressDialog progressDialog;

    int count = 1;
    private DescriptiveRecyclerAdapter descriptiveRecyclerAdapter;
    private String testReposrtHiText;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CommonUtils.hashMap.clear();
        CommonUtils.stringMap.clear();
        CommonUtils.list.clear();
    }

    public static Fragment newInstance() {
        WriteTestReportFragment writeTestReportFragment = new WriteTestReportFragment();
        return writeTestReportFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_write_test_report, container, false);

        unbinder = ButterKnife.bind(this, view);


        medicineList = new ArrayList<>();
        allTestList = new ArrayList<>();
        testDetailsList = new ArrayList<>();


        selectDateTest_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDate();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        apiInterface = new Preference().getInstance();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        OuterRecyclerAdapter outerRecyclerAdapter = new OuterRecyclerAdapter(getActivity(), count);
        CommonUtils.hashMap.put(0, CommonUtils.list);
        testReportRecycler.setHasFixedSize(false);
        testReportRecycler.setFocusable(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        testReportRecycler.setLayoutManager(layoutManager);
        testReportRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        testReportRecycler.setItemAnimator(new DefaultItemAnimator());
        testReportRecycler.setAdapter(outerRecyclerAdapter);
        testReportRecycler.setNestedScrollingEnabled(false);


        layoutInflater = LayoutInflater.from(getActivity());

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        descrTestRecycler.setLayoutManager(layoutManager1);

        //  descrTestRecycler.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(descrTestRecycler, false);

        descriptiveRecyclerAdapter = new DescriptiveRecyclerAdapter(getActivity(), descriptiveModelArrayList, counter);
        descrTestRecycler.setAdapter(descriptiveRecyclerAdapter);


        testReposrtHiText = "\nPlease add your prescription in below mentioned fields to get in detailed format in 30 minutes only.";
        String patName = Preference.getString(getActivity(), PrefManager.USER_NAME);
        String profileName = ProfileData.getMemberProfile(getActivity());
        writeTestHiText.setText("Hi " + profileName + testReposrtHiText);


        patientNameTestTV.setText(profileName);

        addMoreDescriptive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //add more Row .........................
                if (descriptiveModelArrayList.size() < 5) {
                    DescriptiveModel descriptiveModel = new DescriptiveModel();
                    descriptiveModel.setDesCatName("Name");
                    descriptiveModel.setDesResult("Result");
                    descriptiveModelArrayList.add(descriptiveModel);
                    descriptiveRecyclerAdapter.notifyItemInserted(descriptiveModelArrayList.size() - 1);
                }

            }
        });


        addMoreCategory1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (count < 5) {
                    count = count + 1;
                    int size = CommonUtils.hashMap.size();
                    CommonUtils.hashMap.put(size, new ArrayList<TestDetails>());

                    OuterRecyclerAdapter outerRecyclerAdapter = new OuterRecyclerAdapter(getActivity(), count);
                    testReportRecycler.setHasFixedSize(false);
                    testReportRecycler.setFocusable(false);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    testReportRecycler.setLayoutManager(layoutManager);
                    testReportRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    testReportRecycler.setItemAnimator(new DefaultItemAnimator());
                    testReportRecycler.setAdapter(outerRecyclerAdapter);
                    testReportRecycler.setNestedScrollingEnabled(false);
                } else {
                    Toast.makeText(getActivity(), "you can not add more than 5 categories at a time", Toast.LENGTH_SHORT).show();
                }

                //     outerRecyclerAdapter.notifyDataSetChanged();
            }
        });

        // showList();


        submitTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (patientNameTestTV.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter patient name", Toast.LENGTH_SHORT).show();
                } else if (labNameTest_Edit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter lab name", Toast.LENGTH_SHORT).show();

                } else if (presBy_Edit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter prescribed by doctor name", Toast.LENGTH_SHORT).show();

                } else if (sample_Edit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter sample type", Toast.LENGTH_SHORT).show();

                } else if (selectDateTest_tV.length() == 0) {
                    Toast.makeText(getActivity(), "Please select date", Toast.LENGTH_SHORT).show();

                } else {
                    progressDialog.show();
                    prepareandSubmitTest();
                }

            }
        });
    }

    private void resetRecyclerData() {

        medicineList.clear();

        addMedicineRecyclerAdapter.notifyDataSetChanged();
        testCatEdit.setText("");
    }

    private void clearCategoryFields() {

        medicineList.clear();
        addMedicineRecyclerAdapter.notifyDataSetChanged();
        //testCatEdit.setText("");

    }

    private void clearDescriptiveFilds() {


        //descRemark.setText("");

    }

    public void getDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // selectDateTest_tV.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        selectDateTest_tV.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.addTextCategoryTxt, R.id.addDescriptiveTxt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addTextCategoryTxt:

                descriptiveTestLayout.setVisibility(View.GONE);
                // clearDescriptiveFilds();
                testCategoryLayout.setVisibility(View.VISIBLE);

                break;
            case R.id.addDescriptiveTxt:

                descriptiveTestLayout.setVisibility(View.VISIBLE);
                // clearCategoryFields();
                testCategoryLayout.setVisibility(View.GONE);

                break;
            /*case R.id.addMoreRow:


                openAddDialog();

                break;*/

            case R.id.submitTestButton:
                //prepareandSubmitTest();
                break;
        }
    }

    private void openAddDialog() {

        View view1 = layoutInflater.inflate(R.layout.add_new_report_dilog, null, false);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(view1)
                .setTitle("Add Test Result")
                .setPositiveButton("Save", null) //Set to null. We override the onclick
                .setNegativeButton("Cancel", null)
                .create();

        final EditText dialogTestNameEdit = view1.findViewById(R.id.dialogTestNameEdit);
        final EditText dialogTestResultEdit = view1.findViewById(R.id.dialogTestResultEdit);
        final EditText dialogTestRemarkEdit = view1.findViewById(R.id.dialogTestRemarkEdit);


        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {


                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                Button negButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        String item1 = dialogTestNameEdit.getText().toString();
                        String item2 = dialogTestResultEdit.getText().toString();
                        String item3 = dialogTestRemarkEdit.getText().toString();

                        if (item1.length() == 0) {
                            Toast.makeText(getActivity(), "Please Enter Test Name", Toast.LENGTH_SHORT).show();
                        } else if (item2.length() == 0) {
                            Toast.makeText(getActivity(), "Please Enter Test Result", Toast.LENGTH_SHORT).show();
                        } else {

                            testDetailsList.add(new TestDetails(item1, item3, item2, ""));
                            // addMedicineRecyclerAdapter.insert(new TestDetails(item1, item3, item2));
                            dialog.dismiss();
                        }
                    }
                });

            }
        });

        dialog.show();
    }

    private void prepareandSubmitTest() {

        String userId = Preference.getString(getActivity(), PrefManager.USER_ID);

        String patientName = patientNameTestTV.getText().toString();
        String labName = labNameTest_Edit.getText().toString();
        String prescribedByString = presBy_Edit.getText().toString();
        String sampleType = sample_Edit.getText().toString();
        String date = selectDateTest_tV.getText().toString();
        String rmark = remarkTest_tV.getText().toString();
        //String desRemark = descRemark.getText().toString();
        String testCat = "";
        //String descCategoryName = descCatName.getText().toString();
        String memberID = ProfileData.getMemberId(getActivity());

        String resportType = "report";
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("memberId", memberID);
        hashMap.put("patName", patientName);
        hashMap.put("labName", labName);
        hashMap.put("prescBy", prescribedByString);
        hashMap.put("sampleType", sampleType);
        hashMap.put("tDate", date);
        hashMap.put("remarks", rmark);
        hashMap.put("reportType", resportType);
        hashMap.put("testCategory", testCat);

        TestReportBodyClass testReportBodyClass = new TestReportBodyClass();
        testReportBodyClass.setHashMap(hashMap);
        List<TestDetails> ffList = CommonUtils.hashMap.get(0);
        List<List<TestDetails>> localList = new ArrayList<>();
        for (int i = 0; i < CommonUtils.hashMap.size(); i++) {
            localList.add(CommonUtils.hashMap.get(i));
        }

        descriptiveModelArrayList.clear();
        for (int i = 0; i < CommonUtils.descTestCatMap.size(); i++) {
            DescriptiveModel descriptiveModel = new DescriptiveModel();
            descriptiveModel.setDesCatName(CommonUtils.descTestCatMap.get(i));
            descriptiveModel.setDesResult(CommonUtils.desTestResult.get(i));
            descriptiveModelArrayList.add(descriptiveModel);
        }

        testReportBodyClass.setTestDetailsList(localList);

        testReportBodyClass.setDescriptiveModelArrayList(descriptiveModelArrayList);

        Call<ResponseBody> call = apiInterface.saveReport(testReportBodyClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                Log.v("respone", response.toString());
                if (response != null) {
                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();


                        // patientNameTestTV.setText("");
                        labNameTest_Edit.setText("");
                        presBy_Edit.setText("");
                        sample_Edit.setText("");
                        selectDateTest_tV.setText("");
                        remarkTest_tV.setText("");
                        // descRemark.setText("");
                        //testCatEdit.setText("");
                        // descCatName.setText("");

                        medicineList.clear();
                        allTestList.clear();
                        descriptiveModelArrayList.clear();
                        // addMedicineRecyclerAdapter.notifyDataSetChanged();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void refreshData(ShowMemberList showMemberList) {

        String profileName = ProfileData.getMemberProfile(getActivity());
        writeTestHiText.setText("Hi " + profileName + testReposrtHiText);
        patientNameTestTV.setText(profileName);
    }
}
