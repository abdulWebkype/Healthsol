package com.webkype.healthsol.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.RecyclerAdapterImage;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class UploadPrescpFragmnet extends Fragment implements View.OnClickListener {

    private RecyclerView reportFilesRecycler;
    private RecyclerAdapterImage recyclerAdapterImage;
    // private List<ImageList> imageListList = new ArrayList<>();
    // private RecyclerAdapterReprotFiles recyclerAdapterReprotFiles;
    private CardView topCardUploadTestReport;
    public Button submitReportImages_button;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    ApiInterface apiInterface;
    ArrayList<MultipartBody.Part> multiPartList;
    ProgressDialog progressDialog;


    private View view;
    private TextView uploadHiText;

    // private LinearLayout lnrImages;
    private Button btnAddPhots, btnSaveImages;
    private RelativeLayout addFiles;
    private ArrayList<String> imagesPathList;
    private Bitmap yourbitmap;
    private Bitmap resized;
    private final int PICK_IMAGE_MULTIPLE = 1;
    private ArrayList<String> filePaths;
    private String memberId;
    private String desText;
    //private ImageAdapter imageAdapter;
    // private GridView gridDisplayImage;


    public static Fragment newInstance() {
        UploadPrescpFragmnet uploadPrescpFragmnet = new UploadPrescpFragmnet();
        return uploadPrescpFragmnet;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_upload_test_report, container, false);
        topCardUploadTestReport = view.findViewById(R.id.topCardUploadTestReport);
        submitReportImages_button = view.findViewById(R.id.submitReportImages_button);
        addFiles = view.findViewById(R.id.addFiles);
        uploadHiText = view.findViewById(R.id.uploadHiText);

        desText = "\nPlease upload all your prescription in JPEG, PDF, PNG format only. The maximum size of document allowed is 5MB only.";

        uploadHiText.setText("Hi " + Preference.getString(getActivity(), PrefManager.USER_NAME) + desText);

        apiInterface = new Preference().getInstance();
        multiPartList = new ArrayList<>();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);


        reportFilesRecycler = view.findViewById(R.id.PresFilesRecycler);

        btnSaveImages = view.findViewById(R.id.btnSaveImages);

        addFiles.setOnClickListener(this);
        btnSaveImages.setOnClickListener(this);

        filePaths = new ArrayList<>();

        recyclerAdapterImage = new RecyclerAdapterImage(getActivity(), filePaths);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        reportFilesRecycler.setLayoutManager(gridLayoutManager);
        reportFilesRecycler.setItemAnimator(new DefaultItemAnimator());
        reportFilesRecycler.setAdapter(recyclerAdapterImage);
        reportFilesRecycler.setNestedScrollingEnabled(false);

        submitReportImages_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (filePaths.size() == 0 || filePaths == null) {
                    Toast.makeText(getActivity(), "Please select image to upload", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.show();
                    uploadImagesToServer();
                }

            }
        });

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addFiles:

                openChooserDilaog();

                break;
            case R.id.btnSaveImages:
                if (imagesPathList != null) {
                    if (imagesPathList.size() > 1) {
                        Toast.makeText(getActivity(), imagesPathList.size() + " no of images are selected", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), imagesPathList.size() + " no of image are selected", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), " no images are selected", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.submitReportImages_button:
                uploadImagesToServer();
                break;
        }
    }

    private void uploadImagesToServer() {

        String userID = Preference.getString(getActivity(), PrefManager.USER_ID);
        String memberID = ProfileData.getMemberId(getActivity());
        String documentName = ProfileData.getMemberProfile(getActivity());
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[filePaths.size()];
        MultipartBody.Part[] surveyImagesParts2 = convertImagesToMultiPart(surveyImagesParts);


        HashMap<String, RequestBody> hashMap = new HashMap<>();
        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody memberId = RequestBody.create(MediaType.parse("text/plain"), memberID);
        RequestBody docName = RequestBody.create(MediaType.parse("text/plain"), documentName);
        RequestBody docExp = RequestBody.create(MediaType.parse("text/plain"), "surgen");
        RequestBody presType = RequestBody.create(MediaType.parse("text/plain"), "document");
        hashMap.put("userId", userId);
        hashMap.put("memberId", memberId);
        hashMap.put("docName", docName);
        hashMap.put("docExp", docExp);
        hashMap.put("presType", presType);


        //Call<ResponseBody> responseBodyCall = apiInterface.sendDocuments(surveyImagesParts2, hashMap);

        apiInterface.sendDocuments(surveyImagesParts2, hashMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("onSubscribe", d.toString());
                    }

                    @Override
                    public void onNext(ResponseBody response) {

                        progressDialog.dismiss();
                        if (response != null && response instanceof ResponseBody) {
                            try {
                                String res = response.string();
                                JSONObject jsonObject = new JSONObject(res);
                                String status = jsonObject.getString("status");
                                String msg = jsonObject.getString("msg");
                                if (status.equals("200")) {
                                    //make all fields blank.................
                                    makeFiledsblank();
                                }
                                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void makeFiledsblank() {

        filePaths.clear();
        recyclerAdapterImage.notifyDataSetChanged();
    }

    private MultipartBody.Part[] convertImagesToMultiPart(MultipartBody.Part[] surveyImagesParts) {

        for (int i = 0; i < filePaths.size(); i++) {

            File file = new File(filePaths.get(i));
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            multiPartList.add(filePart);
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[i] = MultipartBody.Part.createFormData("myfile[" + i + "]", file.getName(), surveyBody);
        }

        return surveyImagesParts;
    }

    private void openChooserDilaog() {


        final CharSequence options[] = new CharSequence[]{"Capture Image", "Pick Image From Gallery", "Pick File"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (i == 0) {
                    //open Camera...................................
                    Permissions.check(getActivity(), Manifest.permission.CAMERA, null, new PermissionHandler() {
                        @Override
                        public void onGranted() {

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }
                    });
                } else if (i == 1) {

                    //open Gallery...............................

                    FilePickerBuilder.getInstance().setMaxCount(10)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .pickPhoto(getActivity());
                } else {

                    //Pick File ..................................
                    FilePickerBuilder.getInstance().setMaxCount(10)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .pickFile(getActivity());

                }

            }
        });

        builder.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePaths.clear();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

                    showData(filePaths);
                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePaths.clear();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    showData(filePaths);

                }
                break;

            case CAMERA_REQUEST: {
                if (resultCode == Activity.RESULT_OK && data != null) {

                    Bitmap photo = (Bitmap) data.getExtras().get("data");

                    Uri tempUri = getImageUri(getActivity().getApplicationContext(), photo);

                    String path = getRealPathFromURI(tempUri);

                    filePaths.add(path);
                    if (filePaths.size() != 0) {
                        showData(filePaths);
                    }
                }
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void showData(ArrayList<String> filePaths) {

        recyclerAdapterImage.notifyDataSetChanged();

    }

    public void refreshData(ShowMemberList showMemberList) {

        if (showMemberList != null) {

            String memberId = showMemberList.getMemberId();
            this.memberId = memberId;
            String profileName = ProfileData.getMemberProfile(getActivity());
            uploadHiText.setText("Hi " + profileName + desText);
        }
    }


}

