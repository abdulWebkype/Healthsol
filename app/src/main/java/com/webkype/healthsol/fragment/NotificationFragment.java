package com.webkype.healthsol.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.RecyclerAdapterNotifications;
import com.webkype.healthsol.model.NotificationListInner;
import com.webkype.healthsol.model.NotificationListMain;
import com.webkype.healthsol.model.Notify;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationFragment extends Fragment {

    private RecyclerView notificationRecyclerView;
    private RecyclerAdapterNotifications recyclerAdapterNotifications;
    private List<Notify> notifyList = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        notificationRecyclerView = view.findViewById(R.id.notificationRecyclerView);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        notificationRecyclerView.setHasFixedSize(false);
        notificationRecyclerView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        notificationRecyclerView.setLayoutManager(layoutManager);
        notificationRecyclerView.setItemAnimator(new DefaultItemAnimator());

        showNotifications();

        notificationRecyclerView.setNestedScrollingEnabled(false);



        return view;
    }

    private void showNotifications() {
        progressDialog.show();
        ApiInterface apiInterface = new Preference().getInstance();
        String userId = Preference.getString(getActivity(), PrefManager.USER_ID);
        apiInterface.getNotificationList(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NotificationListMain>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(NotificationListMain value) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        if (value != null) {
                            List<NotificationListInner> notificationListInners = value.getNotificationList();
                            if (notificationListInners != null && notificationListInners.size() != 0) {
                                recyclerAdapterNotifications = new RecyclerAdapterNotifications(getActivity(), notificationListInners);
                                notificationRecyclerView.setAdapter(recyclerAdapterNotifications);
                            } else {
                                Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

    }

    public static Fragment newInstance() {
        NotificationFragment notificationFragment = new NotificationFragment();
        return notificationFragment;
    }

}
