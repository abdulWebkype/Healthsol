package com.webkype.healthsol.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.RecyclerAdapterImage;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadTestReportFragment extends Fragment {

    private static final int CAMERA_REQUEST = 1999;
    @BindView(R.id.uploadIconImage)
    ImageView uploadIconImage;

    @BindView(R.id.topCardUploadPrescription)
    CardView topCardUploadPrescription;

    @BindView(R.id.staticText1)
    TextView staticText1;
    @BindView(R.id.uploadTestHiText)
    TextView uploadHiText;

    @BindView(R.id.prescriptionFilesRecycler)
    RecyclerView testDocumentRecycler;

    @BindView(R.id.submitReportDocument)
    Button submitReportDocument;


    Unbinder unbinder;
    private RecyclerAdapterImage recyclerAdapterImage;
    private ArrayList<String> filePaths;
    private ProgressDialog progressDialog;

    private ApiInterface apiInterface;
    private String desText;


    public static Fragment newInstance() {
        UploadPrescpFragmnet uploadPrescpFragmnet = new UploadPrescpFragmnet();
        return uploadPrescpFragmnet;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.upload_test_documents, container, false);


        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        filePaths = new ArrayList<>();
        recyclerAdapterImage = new RecyclerAdapterImage(getActivity(), filePaths);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        testDocumentRecycler.setLayoutManager(gridLayoutManager);
        testDocumentRecycler.setItemAnimator(new DefaultItemAnimator());
        testDocumentRecycler.setAdapter(recyclerAdapterImage);
        testDocumentRecycler.setNestedScrollingEnabled(false);

        apiInterface = new Preference().getInstance();

         desText = "\nPlease upload all your prescription in JPEG, PDF, PNG format only. The maximum size of document allowed is 5MB only.";
        String profileName = ProfileData.getMemberProfile(getActivity());
        uploadHiText.setText("Hi " + profileName + desText);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePaths.clear();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

                    showData(filePaths);
                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePaths.clear();
                    filePaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    showData(filePaths);
                    //Toast.makeText(getActivity(), filePaths.size() + "Files selected", Toast.LENGTH_SHORT).show();
                }
                break;

            case CAMERA_REQUEST: {
                if (resultCode == Activity.RESULT_OK && data != null) {

                    Bitmap photo = (Bitmap) data.getExtras().get("data");

                    Uri tempUri = getImageUri(getActivity().getApplicationContext(), photo);

                    String path = getRealPathFromURI(tempUri);

                    filePaths.add(path);
                    if (filePaths.size() != 0) {
                        showData(filePaths);
                    }
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void showData(ArrayList<String> filePaths) {

        recyclerAdapterImage.notifyDataSetChanged();
    }

    @OnClick({R.id.uploadIconImage, R.id.topCardUploadPrescription, R.id.submitReportDocument, R.id.addNewFiles})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.uploadIconImage:
                break;

            case R.id.submitReportDocument:

                if (filePaths.size() == 0 || filePaths == null) {
                    Toast.makeText(getActivity(), "Please select image to upload", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.show();
                    uploadImagesToServer();
                }

                break;

            case R.id.addNewFiles:
                openChooserDilaog();
                break;
        }
    }

    private void uploadImagesToServer() {

        String userID = Preference.getString(getActivity(), PrefManager.USER_ID);
        String memberID = ProfileData.getMemberId(getActivity());
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[filePaths.size()];
        MultipartBody.Part[] surveyImagesParts2 = convertImagesToMultiPart(surveyImagesParts);

        HashMap<String, RequestBody> hashMap = new HashMap<>();
        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), userID);
        RequestBody memberId = RequestBody.create(MediaType.parse("text/plain"), memberID);

        RequestBody presType = RequestBody.create(MediaType.parse("text/plain"), "document");

        hashMap.put("userId", userId);
        hashMap.put("memberId", memberId);
        hashMap.put("reportType", presType);


        Call<ResponseBody> bodyCall = apiInterface.saveReportDocuments(surveyImagesParts2, hashMap);

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressDialog.dismiss();
                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (status.equals("200")) {
                            makeFiledsblank();
                        }
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private MultipartBody.Part[] convertImagesToMultiPart(MultipartBody.Part[] surveyImagesParts) {


        for (int i = 0; i < filePaths.size(); i++) {

            File file = new File(filePaths.get(i));
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            // multiPartList.add(filePart);
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[i] = MultipartBody.Part.createFormData("testDocument[" + i + "]", file.getName(), surveyBody);
        }

        return surveyImagesParts;
    }

    private void openChooserDilaog() {

        final CharSequence options[] = new CharSequence[]{"Capture Image", "Pick Image From Gallery", "Pick File"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (i == 0) {
                    //open Camera...................................
                    Permissions.check(getActivity(), Manifest.permission.CAMERA, null, new PermissionHandler() {
                        @Override
                        public void onGranted() {

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }
                    });
                } else if (i == 1) {

                    //open Gallery...............................z

                    FilePickerBuilder.getInstance().setMaxCount(10)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .pickPhoto(getActivity());
                } else {

                    //Pick File ..................................
                    FilePickerBuilder.getInstance().setMaxCount(10)
                            .setSelectedFiles(filePaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .pickFile(getActivity());

                }

            }
        });

        builder.show();
    }

    private void makeFiledsblank() {

        filePaths.clear();
        recyclerAdapterImage.notifyDataSetChanged();
    }

    public void refreshData(ShowMemberList showMemberList) {

        String profileName = ProfileData.getMemberProfile(getActivity());
        uploadHiText.setText("Hi " + profileName + desText);


    }
}
