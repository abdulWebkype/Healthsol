package com.webkype.healthsol.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.MeFamilyModule.MeFamilyActivity;
import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.AboutUsActivity;
import com.webkype.healthsol.activity.ContactUsActivity;
import com.webkype.healthsol.activity.FaqActivity;
import com.webkype.healthsol.activity.FeedBackActivity;
import com.webkype.healthsol.activity.HealthiliciousActivity;
import com.webkype.healthsol.activity.HomeActivity;
import com.webkype.healthsol.activity.LoginActivity;
import com.webkype.healthsol.activity.NotificationActivity;
import com.webkype.healthsol.activity.PrescriptionActivity;
import com.webkype.healthsol.activity.PrescriptionRecordActivity;
import com.webkype.healthsol.activity.ProfileActivity;
import com.webkype.healthsol.activity.TestRecordActivity;
import com.webkype.healthsol.activity.TestReportActivity;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

/**
 * Created by vaio on 5/25/2016.
 */

public class SideMenuFragment extends BaseFragment {

    private View rootView;
    private RelativeLayout home, meAndFamily, prescription, prescriptionRecord, testReport, testReportRecord, notifyMe,
            heltiliciousMe, myProfile, helpAndFaq, aboutUs, feedBack, contactUs, logout;
    private ImageView imagePerson_sidebar, logoIV;
    private TextView sideUserName;


    @Override
    protected int getFragmentLayout() {
        return R.layout.ly_sidebar;
    }

    public SideMenuFragment() {
    }

    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.ly_sidebar, container, false);
        home = rootView.findViewById(R.id.home);
        meAndFamily = rootView.findViewById(R.id.meAndFamily);
        prescription = rootView.findViewById(R.id.savedPrescription);
        prescriptionRecord = rootView.findViewById(R.id.prescriptionRecord);
        testReport = rootView.findViewById(R.id.savedTestReport);
        testReportRecord = rootView.findViewById(R.id.testReportRecord);
        notifyMe = rootView.findViewById(R.id.notifyMe);
        heltiliciousMe = rootView.findViewById(R.id.heltiliciousMe);
        myProfile = rootView.findViewById(R.id.myProfile);
        helpAndFaq = rootView.findViewById(R.id.helpAndFaq);
        aboutUs = rootView.findViewById(R.id.aboutUs);
        feedBack = rootView.findViewById(R.id.feedBack);
        contactUs = rootView.findViewById(R.id.contactUs);
        logout = rootView.findViewById(R.id.logout);
        imagePerson_sidebar = rootView.findViewById(R.id.imagePerson_sidebar);
        sideUserName = rootView.findViewById(R.id.sideUserName);
        logoIV = rootView.findViewById(R.id.logoIV);

        String sideName = Preference.getString(getActivity(), PrefManager.USER_NAME);
        String sideImage = Preference.getString(getActivity(), PrefManager.USER_IMAGE);
        String appLogo = Preference.getString(getActivity(), PrefManager.APP_LOGO);
        sideUserName.setText(sideName);


       /* Glide.with(getActivity()).load(appLogo)
                .into(logoIV);*/


        Glide.with(getActivity()).load(sideImage).into(imagePerson_sidebar);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HomeActivity.class));
            }
        });

        meAndFamily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MeFamilyActivity.class));
            }
        });

        heltiliciousMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HealthiliciousActivity.class));
            }
        });

        myProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProfileActivity.class));
            }
        });

        notifyMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });

        testReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TestReportActivity.class));
            }
        });

        testReportRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TestRecordActivity.class));
            }
        });

        prescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PrescriptionActivity.class));
            }
        });

        prescriptionRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PrescriptionRecordActivity.class));
            }
        });

        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AboutUsActivity.class));
            }
        });

        helpAndFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FaqActivity.class));
            }
        });

        feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FeedBackActivity.class));
            }
        });

        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ContactUsActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setMessage("Are you sure you want to logout");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                ///make user logout from preference ...............

                                Preference.saveBoolean(getActivity(), PrefManager.IS_LOGGED_IN, false);
                                //Clear ALl Preferences...............................................
                                Preference.getPreference(getActivity()).edit().clear().apply();


                                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                                ((HomeActivity) getActivity()).finishAffinity();
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}





