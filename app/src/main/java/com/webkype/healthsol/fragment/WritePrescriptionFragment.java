package com.webkype.healthsol.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.AddMedicineRecyclerAdapter;
import com.webkype.healthsol.model.MedicDetail;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;
import com.webkype.healthsol.utility.RetrofitBodyClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WritePrescriptionFragment extends Fragment {

    private RecyclerView addMedicineRecycler;
    private AddMedicineRecyclerAdapter addMedicineRecyclerAdapter;
    private List<MedicDetail> medicineList = new ArrayList<>();

    private TextView patientNamePrescription_tV;
    private MedicDetail medicDetail;
    public TextView addMoreMedicine_tV;
    LayoutInflater layoutInflater;
    ApiInterface apiInterface;
    Button addPrescriptionRecord_btn;
    TextView hiText;
    String memberId;

    String desText = ",\nPlease add your prescription in below mentioned fields to get in detailed format in 30 minutes only.";


    ProgressDialog progressDialog;

    EditText docNameEdit, docExpEdit, docMobileEdit, docAddressEdit, docPhoneEdit, patientComplianEdit, docDiagnosisEdit,
            testAdviceEdit, procedureEdit, specialCommEdit, prescDateEdit;
    TextView patientAgeTxt, patientGenderTxt;
    private String userID;

    private DatePickerDialog datePickerDialog;

    public static Fragment newInstance() {
        WritePrescriptionFragment writePrescriptionFragment = new WritePrescriptionFragment();
        return writePrescriptionFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_write_prescription, container, false);
        findAllViews(view);

        apiInterface = new Preference().getInstance();

        layoutInflater = LayoutInflater.from(getActivity());


        return view;
    }

    private void findAllViews(View view) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        addMoreMedicine_tV = view.findViewById(R.id.addMoreMedicine_tV);
        addMedicineRecycler = view.findViewById(R.id.addMedicineRecycler);
        patientNamePrescription_tV = view.findViewById(R.id.patientNamePrescription_tV);

        docNameEdit = view.findViewById(R.id.docNameEdit);
        docExpEdit = view.findViewById(R.id.docExpEdit);
        docMobileEdit = view.findViewById(R.id.docMobileEdit);
        docAddressEdit = view.findViewById(R.id.docAddressEdit);
        docPhoneEdit = view.findViewById(R.id.docPhoneEdit);
        patientComplianEdit = view.findViewById(R.id.patientComplianEdit);
        docDiagnosisEdit = view.findViewById(R.id.docDiagnosisEdit);
        testAdviceEdit = view.findViewById(R.id.testAdviceEdit);
        procedureEdit = view.findViewById(R.id.procedureEdit);
        specialCommEdit = view.findViewById(R.id.specialCommEdit);
        prescDateEdit = view.findViewById(R.id.prescDateEdit);

        patientComplianEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        patientComplianEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);

        docAddressEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        docAddressEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);

        docDiagnosisEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        docDiagnosisEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);

        testAdviceEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        testAdviceEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);


        procedureEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        procedureEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);

        specialCommEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
        specialCommEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);


        //docAddressEdit.setSingleLine(false);
        //docAddressEdit.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);

        patientAgeTxt = view.findViewById(R.id.patientAgeTxt);
        patientGenderTxt = view.findViewById(R.id.patientGenderTxt);
        hiText = view.findViewById(R.id.hiText);

        addPrescriptionRecord_btn = view.findViewById(R.id.addPrescriptionRecord_btn);
    }


    public void getStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        //startDateTest_editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        prescDateEdit.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setTitle("Select Start Date");
        datePickerDialog.show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        addMedicineRecyclerAdapter = new AddMedicineRecyclerAdapter(getActivity(), medicineList);
        addMedicineRecycler.setHasFixedSize(false);
        addMedicineRecycler.setFocusable(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        addMedicineRecycler.setLayoutManager(layoutManager);
        addMedicineRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        addMedicineRecycler.setItemAnimator(new DefaultItemAnimator());
        addMedicineRecycler.setAdapter(addMedicineRecyclerAdapter);
        userID = Preference.getString(getActivity(), PrefManager.USER_ID);

        //........For Testing ..................................................
        memberId = userID;

        // showList();
        addMedicineRecycler.setNestedScrollingEnabled(false);

        prescDateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getStartDate();

            }
        });

        addMoreMedicine_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View view1 = layoutInflater.inflate(R.layout.add_new_med_dilog, null, false);
                final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setView(view1)
                        .setTitle("Add New Item")
                        .setPositiveButton("Save", null) //Set to null. We override the onclick
                        .setNegativeButton("Cancel", null)
                        .create();

                final EditText dialogMedicineEdit = view1.findViewById(R.id.dialogMedicineEdit);
                final EditText dialogTimeEdit = view1.findViewById(R.id.dialogTimeEdit);
                final EditText dialogDayEdit = view1.findViewById(R.id.dialogDayEdit);

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {

                        Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                        Button negButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                        button.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                // TODO Do something

                                String item1 = dialogMedicineEdit.getText().toString();
                                String item2 = dialogTimeEdit.getText().toString();
                                String item3 = dialogDayEdit.getText().toString();
                                if (item1.length() == 0) {
                                    Toast.makeText(getActivity(), "Please Enter Medicine Name", Toast.LENGTH_SHORT).show();
                                    return;
                                } else if (item2.length() == 0) {
                                    Toast.makeText(getActivity(), "Please Enter Number of time per day", Toast.LENGTH_SHORT).show();
                                    return;
                                } else if (item3.length() == 0) {
                                    Toast.makeText(getActivity(), "Please Enter Number of days", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    addMedicineRecyclerAdapter.insert(new MedicDetail(item1, item2, item3));
                                    dialog.dismiss();
                                }
                            }
                        });
                    }
                });

                dialog.show();

            }
        });

        String patName = Preference.getString(getActivity(), PrefManager.USER_NAME);

        patientNamePrescription_tV.setText(patName);

        String profileName = ProfileData.getMemberProfile(getActivity());
        hiText.setText("Hi " + profileName + desText);

        addPrescriptionRecord_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //..........................check validation.....................................

                if (docNameEdit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter doctor's name", Toast.LENGTH_SHORT).show();
                } else if (docExpEdit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter doctor's Expertise", Toast.LENGTH_SHORT).show();

                } else if (docMobileEdit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter doctor's mobile", Toast.LENGTH_SHORT).show();

                } else if (docAddressEdit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter doctor's Address", Toast.LENGTH_SHORT).show();

                } else if (patientNamePrescription_tV.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter patient name", Toast.LENGTH_SHORT).show();

                } else if (patientAgeTxt.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter patient age", Toast.LENGTH_SHORT).show();

                } else if (patientGenderTxt.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter patient gender", Toast.LENGTH_SHORT).show();

                } else if (docDiagnosisEdit.length() == 0) {
                    Toast.makeText(getActivity(), "Please enter diagnosis", Toast.LENGTH_SHORT).show();

                } else if (medicineList.size() == 0) {
                    Toast.makeText(getActivity(), "Please enter medicine times", Toast.LENGTH_SHORT).show();

                } else {
                    progressDialog.show();
                    addPrescription();
                }

            }
        });
    }

    private void addPrescription() {

        JSONArray jsonArray1 = null;
        String jsonText = new Gson().toJson(medicineList);
        try {
            jsonArray1 = new JSONArray(jsonText);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String docName = docNameEdit.getText().toString();
        String docExp = docExpEdit.getText().toString();
        String docMobile = docMobileEdit.getText().toString();
        String docAdd = docAddressEdit.getText().toString();
        String docPhone = docPhoneEdit.getText().toString();
        String patientName = patientNamePrescription_tV.getText().toString();
        String patientAge = patientAgeTxt.getText().toString();
        String patientGender = patientGenderTxt.getText().toString();
        String patientCmplain = patientComplianEdit.getText().toString();
        String docDiagnosis = docDiagnosisEdit.getText().toString();
        String testAdvi = testAdviceEdit.getText().toString();
        String procedure = procedureEdit.getText().toString();
        String comment = specialCommEdit.getText().toString();
        String presDate = prescDateEdit.getText().toString();

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("userId", userID);
        hashMap.put("memberId", memberId);
        hashMap.put("docName", docName);
        hashMap.put("doceExp", docExp);
        hashMap.put("docMobile", docMobile);
        hashMap.put("docAddress", docAdd);
        hashMap.put("docPhone", docPhone);
        hashMap.put("prescDate", presDate);
        hashMap.put("patName", patientName);
        hashMap.put("patAge", patientAge);
        hashMap.put("patGender", patientGender);
        hashMap.put("patComplain", patientCmplain);
        hashMap.put("docDiagnosis", docDiagnosis);
        hashMap.put("testAdvised", testAdvi);
        hashMap.put("procConducted", procedure);
        hashMap.put("specialComment", comment);
        hashMap.put("presType", "report");

        RetrofitBodyClass retrofitBodyClass = new RetrofitBodyClass();
        retrofitBodyClass.setHashMap(hashMap);
        retrofitBodyClass.setMedicineList(medicineList);

        Call<ResponseBody> call = apiInterface.savePrescription(retrofitBodyClass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();


                        docNameEdit.setText("");
                        docExpEdit.setText("");
                        docMobileEdit.setText("");
                        docAddressEdit.setText("");
                        docPhoneEdit.setText("");
                        prescDateEdit.setText("");
                        docDiagnosisEdit.setText("");
                        testAdviceEdit.setText("");
                        procedureEdit.setText("");
                        specialCommEdit.setText("");


                        medicineList.clear();
                        addMedicineRecyclerAdapter.notifyDataSetChanged();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void refreshData(ShowMemberList showMemberList) {

        if (showMemberList != null) {

            String memberId = showMemberList.getMemberId();
            this.memberId = memberId;
            patientGenderTxt.setText(showMemberList.getGender());
            patientAgeTxt.setText(showMemberList.getAge() + " Years");
            patientNamePrescription_tV.setText(showMemberList.getMemberName());
            String profileName = ProfileData.getMemberProfile(getActivity().getApplicationContext());
            hiText.setText("Hi " + profileName + desText);

        }
    }
}
