package com.webkype.healthsol.utility;

import android.content.Context;

public class ProfileData {

    public static String getMemberProfile(Context c) {

        return Preference.getString(c, "MemberProfile");
    }

    public static String getMemberId(Context c) {

        return Preference.getString(c, "memberId");
    }

    public static void setMemberProfile(Context c, String memberProfile) {

        Preference.saveString(c, "MemberProfile", memberProfile);
    }

    public static void setMemberId(Context c, String memeberId) {

        Preference.saveString(c, "memberId", memeberId);
    }

    public static void clearPrefernce(Context c, String memeberId) {

        Preference.saveString(c, "memberId", memeberId);
    }

}
