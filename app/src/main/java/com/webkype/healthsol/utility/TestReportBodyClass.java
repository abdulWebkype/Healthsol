package com.webkype.healthsol.utility;

import com.webkype.healthsol.model.DescriptiveModel;
import com.webkype.healthsol.model.TestDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestReportBodyClass {

    HashMap<String, String> testReportDetails;

    public HashMap<String, String> getHashMap() {
        return testReportDetails;
    }

    public void setHashMap(HashMap<String, String> hashMap) {
        this.testReportDetails = hashMap;
    }


    public ArrayList<DescriptiveModel> getDescriptiveModelArrayList() {
        return descriptiveModelArrayList;
    }

    public void setDescriptiveModelArrayList(ArrayList<DescriptiveModel> descriptiveModelArrayList) {
        this.descriptiveModelArrayList = descriptiveModelArrayList;
    }


    private ArrayList<DescriptiveModel> descriptiveModelArrayList;

    public List<List<TestDetails>> getTestDetailsList() {
        return testDetailsList;
    }

    public void setTestDetailsList(List<List<TestDetails>> testDetailsList) {
        this.testDetailsList = testDetailsList;
    }

    private List<List<TestDetails>> testDetailsList;
}
