package com.webkype.healthsol.utility;

import android.content.Context;
import android.content.SharedPreferences;


/*
* For Intro Screen
* */
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences constants
    private static final String PREF_NAME = "HealthSol";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_IMAGE = "userLogo";
    public static final String APP_LOGO = "app_logo";
    public static final String SELECTED_PATIENT_NAME = "paitianceName";
    public static final String IS_LOGGED_IN = "isUserLoggedIn";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

}

