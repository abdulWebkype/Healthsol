package com.webkype.healthsol.utility;

import com.webkype.healthsol.model.MedicDetail;

import java.util.HashMap;
import java.util.List;

public class RetrofitBodyClass {

    private List<MedicDetail> medicineList;
    private  HashMap<String, String> prescriptionDetails;

    public List<MedicDetail> getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(List<MedicDetail> medicineList) {
        this.medicineList = medicineList;
    }

    public HashMap<String, String> getHashMap() {
        return prescriptionDetails;
    }

    public void setHashMap(HashMap<String, String> hashMap) {
        this.prescriptionDetails = hashMap;
    }
}
