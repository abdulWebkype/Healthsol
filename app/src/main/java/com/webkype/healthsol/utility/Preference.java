package com.webkype.healthsol.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;

import com.webkype.healthsol.RetrofitUtils.APIClient;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;


/*
 * For saving all data's
 * */

public class Preference {

    private static SharedPreferences preferences;
    ApiInterface apiInterface;

    public Preference() {
    }


    public static SharedPreferences getPreference(Context con) {
        if (preferences == null) {
            preferences = con.getSharedPreferences("appPreference", Context.MODE_PRIVATE);
        }
        return preferences;
    }

    public static void saveInt(Context context, String key, int value) {
        getPreference(context).edit().putInt(key, value).commit();
    }

    public static void saveLong(Context context, String key, long value) {
        getPreference(context).edit().putLong(key, value).commit();
    }


    public static void saveString(Context context, String key, String value) {
        getPreference(context).edit().putString(key, value).commit();
    }

    public static void saveBoolean(Context context, String key, Boolean status) {
        getPreference(context).edit().putBoolean(key, status).commit();
    }

    public static String getString(Context context, String key) {
        return getPreference(context).getString(key, "");
    }


    public static int getInt(Context context, String key) {
        return getPreference(context).getInt(key, 0);
    }

    public static long getLong(Context context, String key) {
        return getPreference(context).getLong(key, 0);
    }


    public static boolean getBoolean(Context context, String key) {
        return getPreference(context).getBoolean(key, false);
    }


    public ApiInterface getInstance() {
        if (apiInterface == null) {
            apiInterface = APIClient.getClient().create(ApiInterface.class);
        }
        return apiInterface;
    }


    public static void showDeleteDialg(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.show();
    }

}

