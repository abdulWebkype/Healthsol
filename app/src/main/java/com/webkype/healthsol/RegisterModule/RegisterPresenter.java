package com.webkype.healthsol.RegisterModule;

public interface RegisterPresenter {


    void validateCredentials(String fullName, String email, String password,String confirmPass ,String contact);

    void onDestroy();


    void onBack();
}
