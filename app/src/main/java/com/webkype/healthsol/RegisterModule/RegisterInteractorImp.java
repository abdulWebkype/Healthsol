package com.webkype.healthsol.RegisterModule;

import android.content.Context;

import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterInteractorImp implements RegisterInteractor {

    ApiInterface apiInterface = new Preference().getInstance();

    @Override
    public void register(String fullName, String email, String password, String contactNo,
                         final OnRegisterFinishedListener listener, Context context) {


        Call<ResponseBody> responseBodyCall = apiInterface.registerUser(fullName, email, password, contactNo);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");

                        if (status.equals("200")) {
                            listener.onRegisterSuccess(msg);
                        } else {
                            listener.onRegistrationFailed(msg);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
