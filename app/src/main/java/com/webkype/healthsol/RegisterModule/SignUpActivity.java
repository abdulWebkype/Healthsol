package com.webkype.healthsol.RegisterModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.LoginActivity;

public class SignUpActivity extends AppCompatActivity implements RegisterView {

    private ImageView backFromSignUp_Login;
    private TextView alreadyAccount_tV;
    private Button signup_button;

    RegisterPresenter registerPresenter;
    ProgressDialog progressDialog;

    EditText firstNameSignUp_editText, emailSignUp_editText, passwordSignUp_editText, cnfrmPassSignUp_editText, mobileSignUp_editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        signUpViews();

        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);

        registerPresenter = new RegisterPresenterImp(this, new RegisterInteractorImp(), SignUpActivity.this);

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fullName = firstNameSignUp_editText.getText().toString();
                String email = emailSignUp_editText.getText().toString();
                String password = passwordSignUp_editText.getText().toString();
                String cPassword = cnfrmPassSignUp_editText.getText().toString();
                String contact = mobileSignUp_editText.getText().toString();

                registerPresenter.validateCredentials(fullName, email, password, cPassword, contact);

            }
        });

        backFromSignUp_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        alreadyAccount_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    private void signUpViews() {
        backFromSignUp_Login = findViewById(R.id.backFromSignUp_Login);
        alreadyAccount_tV = findViewById(R.id.alreadyAccount_tV);
        signup_button = findViewById(R.id.signup_button);
        alreadyAccount_tV = findViewById(R.id.alreadyAccount_tV);

        firstNameSignUp_editText = findViewById(R.id.firstNameSignUp_editText);
        emailSignUp_editText = findViewById(R.id.emailSignUp_editText);
        passwordSignUp_editText = findViewById(R.id.passwordSignUp_editText);
        cnfrmPassSignUp_editText = findViewById(R.id.cnfrmPassSignUp_editText);
        mobileSignUp_editText = findViewById(R.id.mobileSignUp_editText);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void setEmailError() {
        Toast.makeText(SignUpActivity.this, "Please enter your email", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void setMobileError() {
        Toast.makeText(SignUpActivity.this, "Please enter your contact number", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setUsernameError() {
        Toast.makeText(SignUpActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPasswordError() {
        Toast.makeText(SignUpActivity.this, "Please enter your passsowrd", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToLogin(String msg) {

        Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_SHORT).show();

        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onFailed(String s) {
        Toast.makeText(SignUpActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPassMatchError() {
        Toast.makeText(SignUpActivity.this, "Password and confirm password should be same", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        registerPresenter.onDestroy();
    }
}
