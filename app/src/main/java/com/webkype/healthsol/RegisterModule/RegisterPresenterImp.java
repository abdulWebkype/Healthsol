package com.webkype.healthsol.RegisterModule;

import android.content.Context;

public class RegisterPresenterImp implements RegisterPresenter, RegisterInteractor.OnRegisterFinishedListener {


    RegisterView registerView;
    RegisterInteractor registerInteractor;
    Context context;

    public RegisterPresenterImp(RegisterView registerView, RegisterInteractorImp registerInteractor, Context context) {

        this.registerInteractor = registerInteractor;
        this.registerView = registerView;
        this.context = context;
    }

    @Override
    public void validateCredentials(String fullName, String email, String password, String confirmPass, String contact) {


        if (registerView != null) {

            if (fullName.length() == 0) {
                registerView.setUsernameError();

            } else if (email.length() == 0) {
                registerView.setEmailError();

            } else if (password.length() == 0) {
                registerView.setPasswordError();

            } else if (contact.length() == 0) {
                registerView.setMobileError();

            } else if (!password.equals(confirmPass)) {
                registerView.setPassMatchError();
            } else {
                registerView.showProgress();
                registerInteractor.register(fullName, email, password, contact, this, context);
            }

        }


    }

    @Override
    public void onDestroy() {

        registerView = null;
    }

    @Override
    public void onBack() {

    }

    @Override
    public void onRegistrationFailed(String s) {

        registerView.hideProgress();
        registerView.onFailed(s);
    }

    @Override
    public void onRegisterSuccess(String msg) {

        registerView.hideProgress();
        registerView.navigateToLogin(msg);
    }
}
