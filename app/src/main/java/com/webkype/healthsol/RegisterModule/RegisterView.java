package com.webkype.healthsol.RegisterModule;

public interface RegisterView {


    void showProgress();

    void hideProgress();

    void setEmailError();
    void setMobileError();

    void setUsernameError();

    void setPasswordError();

    void navigateToLogin(String msg);

    void onFailed(String s);

    void setPassMatchError();
}
