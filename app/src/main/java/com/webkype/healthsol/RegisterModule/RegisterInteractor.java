package com.webkype.healthsol.RegisterModule;

import android.content.Context;

import org.json.JSONObject;

public interface RegisterInteractor {


    interface OnRegisterFinishedListener {

        void onRegistrationFailed(String s);

        void onRegisterSuccess(String msg);
    }

    void register(String fullName, String email, String password, String contactNo,
                  RegisterInteractor.OnRegisterFinishedListener listener, Context context);
}
