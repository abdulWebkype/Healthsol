package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.R;
import com.webkype.healthsol.model.PdfDocumentListPojo;
import com.webkype.healthsol.utility.ZoomView;

import java.util.List;

public class MedicineImagesAdapter extends PagerAdapter {

    private Context context;
    List<PdfDocumentListPojo> listPojos;

    public MedicineImagesAdapter(Context context, List<PdfDocumentListPojo> listPojos) {

        this.context = context;
        this.listPojos = listPojos;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {


        View view = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.gallery_pager_items,
                null, false);

        ZoomView zoomView = view.findViewById(R.id.gallery_image_view);
        ImageView imageView = new ImageView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(layoutParams);
        zoomView.addView(imageView);

        String imageUrl = null;

        imageUrl = listPojos.get(position).getDocument();


        if (imageUrl != null && !imageUrl.equals("")) {
            Glide.with(context).load(imageUrl).into(imageView);
        }


        container.addView(view);
        return view;


    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
    }

    /**
     * Return the number of views available.
     */


    @Override
    public int getCount() {
        if (listPojos == null) {
            return 0;
        }

        return listPojos.size();
    }

    /**
     * Determines whether a page View is associated with a specific key object
     * as returned by {@link #instantiateItem(ViewGroup, int)}. This method is
     * required for a PagerAdapter to function properly.
     *
     * @param view   Page View to check for association with <code>object</code>
     * @param object Object to check for association with <code>view</code>
     * @return true if <code>view</code> is associated with the key object <code>object</code>
     */
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
