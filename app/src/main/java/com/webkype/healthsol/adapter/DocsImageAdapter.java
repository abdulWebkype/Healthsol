package com.webkype.healthsol.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.R;

import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

public class DocsImageAdapter extends RecyclerView.Adapter {


    Context context;
    List<String> imagesList;

    public DocsImageAdapter(Context context, List<String> imagesList) {

        this.context = context;
        this.imagesList = imagesList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.docs_list_items, null, false);

        return new DocsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        DocsViewHolder docsViewHolder = (DocsViewHolder) holder;

        Glide.with(context).load("https://s-media-cache-ak0.pinimg.com/originals/af/c8/44/afc8446f11c96dc2f13645a357895de9.jpg").into(docsViewHolder.docsImageview);


        docsViewHolder.docsImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showImageZoomedDialog();
            }
        });
    }

    private void showImageZoomedDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.zommed_image_layout, null, false);

        ImageView imageView = view.findViewById(R.id.zoomed_image_view);
        ImageView clso = view.findViewById(R.id.sdadCLose);





        /*ImageView imageView = new ImageView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400,
                1200);
        imageView.setLayoutParams(layoutParams);*/
        Glide.with(context).load("https://s-media-cache-ak0.pinimg.com/originals/af/c8/44/afc8446f11c96dc2f13645a357895de9.jpg").into(imageView);


        builder.setView(view);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView);
        pAttacher.update();

      //  pAttacher.getImageView().setTranslationY(200);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        clso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    private class DocsViewHolder extends RecyclerView.ViewHolder {

        ImageView docsImageview;

        public DocsViewHolder(View itemView) {
            super(itemView);
            docsImageview = itemView.findViewById(R.id.docsImageview);
        }
    }

}
