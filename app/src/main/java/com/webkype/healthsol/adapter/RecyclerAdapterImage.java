package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.R;

import java.util.List;

public class RecyclerAdapterImage extends RecyclerView.Adapter<RecyclerAdapterImage.ViewHolder> {

    private Context context;
    //private String[] imageValues;
    private List<String> imageValues;

    public RecyclerAdapterImage(Context context, List<String> imageValues) {
        this.context = context;
        this.imageValues = imageValues;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridview_home_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final String mobileres = imageValues.get(position);

        if (mobileres.contains(".pdf")) {
            holder.imageView.setImageResource(R.drawable.ic_picture_as_pdf_black_24dp);
        } else {
            Glide.with(context).load(mobileres).into(holder.imageView);
        }


        holder.imageClose_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageValues.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, imageValues.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView, imageClose_imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.grid_item_image);
            imageClose_imageView = itemView.findViewById(R.id.imageClose_imageView);
        }
    }
}
