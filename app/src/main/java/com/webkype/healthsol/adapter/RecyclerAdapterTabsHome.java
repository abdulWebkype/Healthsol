package com.webkype.healthsol.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.MeFamilyModule.MeFamilyActivity;
import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.NotificationActivity;
import com.webkype.healthsol.activity.PrescriptionActivity;
import com.webkype.healthsol.activity.PrescriptionRecordActivity;
import com.webkype.healthsol.activity.TestRecordActivity;
import com.webkype.healthsol.activity.TestReportActivity;
import com.webkype.healthsol.model.TabsHome;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterTabsHome extends RecyclerView.Adapter<RecyclerAdapterTabsHome.ViewHolder> {

    private Context context;
    private List<TabsHome> tabsHomeList = new ArrayList<>();

    public RecyclerAdapterTabsHome(Context context, List<TabsHome> tabsHomeList){
        this.context = context;
        this.tabsHomeList = tabsHomeList;
    }

    @NonNull
    @Override
    public RecyclerAdapterTabsHome.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterTabsHome.ViewHolder holder, int position) {
        final TabsHome tabsHome = tabsHomeList.get(position);
        Glide.with(context).load(tabsHome.getTabImage()).into(holder.tabImageHome);
        holder.tabNameHome.setText(tabsHome.getTabName());
        holder.tabHomeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tabsHome.getId().equals("1")){
                    context.startActivity(new Intent(context, PrescriptionActivity.class));
                }
                else if(tabsHome.getId().equals("2")){
                    context.startActivity(new Intent(context, TestReportActivity.class));
                }
                else if(tabsHome.getId().equals("3")){
                    context.startActivity(new Intent(context, NotificationActivity.class));
                }
                else if(tabsHome.getId().equals("4")){
                    context.startActivity(new Intent(context, PrescriptionRecordActivity.class));
                }
                else if(tabsHome.getId().equals("5")){
                    context.startActivity(new Intent(context, TestRecordActivity.class));
                }
                else if(tabsHome.getId().equals("6")){
                    context.startActivity(new Intent(context, MeFamilyActivity.class));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (tabsHomeList != null ? tabsHomeList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView tabImageHome;
        private TextView tabNameHome;
        private RelativeLayout tabHomeCard;

        public ViewHolder(View itemView) {
            super(itemView);

            tabImageHome = itemView.findViewById(R.id.tabImageHome);
            tabNameHome = itemView.findViewById(R.id.tabNameHome);
            tabHomeCard = itemView.findViewById(R.id.tabHomeCard);
        }
    }
}
