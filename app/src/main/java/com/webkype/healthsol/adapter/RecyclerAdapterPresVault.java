package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.PrescriptionRecordActivity;
import com.webkype.healthsol.model.MedicDetail;
import com.webkype.healthsol.model.PrescriptionRecords.PresRecordsListPojo;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterPresVault extends RecyclerView.Adapter<RecyclerAdapterPresVault.ViewHolder> {

    private Context context;
    List<PresRecordsListPojo> prescriptionRecordList;
    int c = 0;
    private List<MedicDetail> medicDetailList = new ArrayList<>();

    PrescriptionRecordActivity prescriptionRecordActivity;

    public RecyclerAdapterPresVault(Context context, List<PresRecordsListPojo> prescriptionRecordList,
                                    PrescriptionRecordActivity prescriptionRecordActivity) {
        this.context = context;
        this.prescriptionRecordList = prescriptionRecordList;
        this.prescriptionRecordActivity = prescriptionRecordActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.prescription_record, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final PresRecordsListPojo prescriptionRecord = prescriptionRecordList.get(position);
        holder.prescriptionDate_text.setText(prescriptionRecord.getPresDate());
        holder.doctorName.setText(prescriptionRecord.getDoctorName());
        holder.doctorExpertise.setText(prescriptionRecord.getDoctorExp());

        holder.viewPrescriptionVaultDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prescriptionRecordActivity.showDetailsInDialog(prescriptionRecord.getPresId());

            }
        });

        holder.deletePrescription_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Call Delete API................

                prescriptionRecordActivity.deletePrescription(prescriptionRecord.getPresId(),position);
                /*prescriptionRecordList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, prescriptionRecordList.size());*/
            }
        });

    }


    @Override
    public int getItemCount() {
        return (prescriptionRecordList != null ? prescriptionRecordList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView recordCardPrescription;
        private TextView prescriptionDate_text, doctorName, doctorExpertise;
        private ImageView viewPrescriptionVaultDetail, deletePrescription_Image;
        private LinearLayout adapterDetailRecordVault_linLay;
        private RecyclerView medicineDetailRecycler;
        private RecyclerAdapterMedicDetail recyclerAdapterMedicDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            adapterDetailRecordVault_linLay = itemView.findViewById(R.id.adapterDetailRecordVault_linLay);
            prescriptionDate_text = itemView.findViewById(R.id.prescriptionDate_text);
            doctorName = itemView.findViewById(R.id.doctorName);
            doctorExpertise = itemView.findViewById(R.id.doctorExpertise);
            recordCardPrescription = itemView.findViewById(R.id.recordCardPrescription);
            medicineDetailRecycler = itemView.findViewById(R.id.medicineDetailRecycler);
            viewPrescriptionVaultDetail = itemView.findViewById(R.id.viewPrescriptionVaultDetail);
            deletePrescription_Image = itemView.findViewById(R.id.deletePrescription_Image);
        }
    }
}
