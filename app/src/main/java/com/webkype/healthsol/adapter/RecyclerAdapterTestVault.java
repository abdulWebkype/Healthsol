package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.TestRecordActivity;
import com.webkype.healthsol.model.MedicDetail;
import com.webkype.healthsol.model.TestReportRecords.TestRecordListPojo;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterTestVault extends RecyclerView.Adapter<RecyclerAdapterTestVault.ViewHolder> {

    private Context context;
    private List<TestRecordListPojo> testRecordList;
    int c = 0;
    private List<MedicDetail> medicDetailList = new ArrayList<>();

    TestRecordActivity testRecordActivity;

    public RecyclerAdapterTestVault(Context context, List<TestRecordListPojo> testRecordList, TestRecordActivity testRecordActivity) {
        this.context = context;
        this.testRecordList = testRecordList;
        this.testRecordActivity = testRecordActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_record, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        ViewHolder holder = (ViewHolder) viewHolder;


        final TestRecordListPojo testRecord = testRecordList.get(position);
        holder.recordDate_text.setText(testRecord.getTestDate());
        holder.labNameTest.setText(testRecord.getLabName());
        holder.doctorNameTest.setText(testRecord.getPrescrdBy());

        holder.viewTestVaultDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                testRecordActivity.showTestDetails(testRecord.getTestId());

            }
        });

        holder.deleteTest_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                  testRecordActivity.removeItem(testRecordList.get(position).getTestId(), position, RecyclerAdapterTestVault.this);


            }
        });


    }

    @Override
    public int getItemCount() {
        return (testRecordList != null ? testRecordList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView recordDate_text, labNameTest, doctorNameTest;
        private ImageView viewTestVaultDetail, deleteTest_Image;
        private LinearLayout adapterDetailRecordVault_linLay;


        public ViewHolder(View itemView) {
            super(itemView);

            recordDate_text = itemView.findViewById(R.id.recordDate_text);
            labNameTest = itemView.findViewById(R.id.labNameTest);
            doctorNameTest = itemView.findViewById(R.id.doctorNameTest);
            viewTestVaultDetail = itemView.findViewById(R.id.viewTestVaultDetail);
            deleteTest_Image = itemView.findViewById(R.id.deleteTest_Image);
            adapterDetailRecordVault_linLay = itemView.findViewById(R.id.adapterDetailRecordVault_linLay);

        }
    }
}
