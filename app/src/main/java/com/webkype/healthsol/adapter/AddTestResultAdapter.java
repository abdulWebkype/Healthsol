package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.TestDetails;

import java.util.List;

public class AddTestResultAdapter extends RecyclerView.Adapter {

    Context context;
    List<TestDetails> testDetails;

    public AddTestResultAdapter(Context context, List<TestDetails> testDetails) {
        this.context = context;
        this.testDetails = testDetails;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_medicine_list, parent, false);
        return new TestViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        TestViewHolder testViewHolder = (TestViewHolder) holder;
        testViewHolder.medicineNameEnter_edit.setText(testDetails.get(position).getTestName());
        testViewHolder.timesEnter_edit.setText(testDetails.get(position).getTestResult());
        testViewHolder.daysEnter_edit.setText(testDetails.get(position).getTestRemark());
    }

    @Override
    public int getItemCount() {
        if (testDetails!=null)
        {
            return testDetails.size();
        }
        else {
            return 0;
        }

    }

    public void insert(TestDetails data, int pos, OuterRecyclerAdapter outerRecyclerAdapter) {

      //  testDetails.add(data);
        outerRecyclerAdapter.notifyOuterADapter(pos);
        notifyItemInserted(testDetails.size());


    }


    public class TestViewHolder extends RecyclerView.ViewHolder {

        EditText medicineNameEnter_edit, daysEnter_edit, timesEnter_edit;

        public TestViewHolder(View itemView) {
            super(itemView);
            timesEnter_edit = itemView.findViewById(R.id.timesEnter_edit);
            medicineNameEnter_edit = itemView.findViewById(R.id.medicineNameEnter_edit);
            daysEnter_edit = itemView.findViewById(R.id.daysEnter_edit);
        }
    }

}
