package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.MedicDetail;

import java.util.List;

public class InnerRecyclerAdapter extends RecyclerView.Adapter {
    Context context;
    List<MedicDetail> medicineList;


    public InnerRecyclerAdapter(Context context, List<MedicDetail> medicineList) {

        this.context = context;
        this.medicineList = medicineList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_medicine_list, null, false);
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        InnerViewHolder holder1 = (InnerViewHolder) holder;

        holder1.medicineNameEnter_edit.setText(medicineList.get(position).getMedicineName());
        holder1.timesEnter_edit.setText(medicineList.get(position).getNoOfTime());
        holder1.daysEnter_edit.setText(medicineList.get(position).getNoofDays());
    }

    @Override
    public int getItemCount() {
        return medicineList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder {

        private EditText medicineNameEnter_edit, daysEnter_edit, timesEnter_edit;

        public InnerViewHolder(View itemView) {
            super(itemView);


            timesEnter_edit = itemView.findViewById(R.id.timesEnter_edit);
            medicineNameEnter_edit = itemView.findViewById(R.id.medicineNameEnter_edit);
            daysEnter_edit = itemView.findViewById(R.id.daysEnter_edit);
        }
    }
}
