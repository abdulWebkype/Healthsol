package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.TestDialogPojos.DescDetailList;

import java.util.List;

public class DesTableAdapter extends RecyclerView.Adapter {


    private Context context;
    List<DescDetailList> descDetailLists;

    public DesTableAdapter(Context context, List<DescDetailList> descDetailLists) {


        this.context = context;
        this.descDetailLists = descDetailLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.desc_table_items, parent, false);
        return new DescTableHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        DescTableHolder descTableHolder = (DescTableHolder) holder;
        descTableHolder.descTableName.setText(descDetailLists.get(position).getCategory_name());
        descTableHolder.descTableResult.setText(descDetailLists.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return descDetailLists.size();
    }


    private class DescTableHolder extends RecyclerView.ViewHolder {

        private final TextView descTableResult;
        private final TextView descTableName;


        public DescTableHolder(View itemView) {
            super(itemView);

            descTableResult = itemView.findViewById(R.id.descTableResult);
            descTableName = itemView.findViewById(R.id.descTableName);

        }
    }
}
