package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.TestDialogPojos.CatDetailsPojo;
import com.webkype.healthsol.model.TestDialogPojos.TestRecordDetailsList;

import java.util.List;

public class TestDialogAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<TestRecordDetailsList> dialogMedPres;
    private List<CatDetailsPojo> innerList;

    public TestDialogAdapter(Context context, List<TestRecordDetailsList> dialogMedPres) {

        this.context = context;
        this.dialogMedPres = dialogMedPres;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_dialog_main_layout, parent, false);
        return new FirstRowHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        FirstRowHolder firstRowHolder = (FirstRowHolder) holder;

        String catName = dialogMedPres.get(position).getCategoryName();
        firstRowHolder.catName.setText("Category : " + catName);

        innerList = dialogMedPres.get(position).getCatDetails();

        firstRowHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        TestDialogInnerAdapter testDialogInnerAdapter = new TestDialogInnerAdapter(context, innerList);
        firstRowHolder.recyclerView.setAdapter(testDialogInnerAdapter);

    }

    @Override
    public int getItemCount() {
        return dialogMedPres.size();
    }

    class FirstRowHolder extends RecyclerView.ViewHolder {

        private TextView catName;
        private RecyclerView recyclerView;

        public FirstRowHolder(View itemView) {
            super(itemView);
            catName = itemView.findViewById(R.id.testCatTxt);
            recyclerView = itemView.findViewById(R.id.testInnerRecycler);
        }
    }


}
