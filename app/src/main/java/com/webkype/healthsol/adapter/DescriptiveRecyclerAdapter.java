package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.DescriptiveModel;
import com.webkype.healthsol.utility.CommonUtils;

import java.util.ArrayList;

import butterknife.BindView;

public class DescriptiveRecyclerAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<DescriptiveModel> list;
    int count;

    public DescriptiveRecyclerAdapter(Context context, ArrayList<DescriptiveModel> list, int count) {

        this.context = context;
        this.list = list;
        this.count = count;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.desc_recyler_items, null, false);
        return new DescriptiveViewHolde(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        DescriptiveViewHolde descriptiveViewHolde = (DescriptiveViewHolde) holder;

        descriptiveViewHolde.descCatName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String desCat = charSequence.toString();

                CommonUtils.descTestCatMap.put(position, desCat);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        descriptiveViewHolde.descRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String remark = charSequence.toString();

                CommonUtils.desTestResult.put(position, remark);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class DescriptiveViewHolde extends RecyclerView.ViewHolder {



        EditText descCatName;
        EditText descRemark;

        public DescriptiveViewHolde(View itemView) {
            super(itemView);

            descCatName=itemView.findViewById(R.id.descCatName);
            descRemark=itemView.findViewById(R.id.descRemark);
        }
    }

}
