package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.MedicDetail;

import java.util.ArrayList;
import java.util.List;

public class AddMedicineRecyclerAdapter extends RecyclerView.Adapter<AddMedicineRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<MedicDetail> medicineList = new ArrayList<>();

    public AddMedicineRecyclerAdapter(Context context, List<MedicDetail> medicineList) {
        this.context = context;
        this.medicineList = medicineList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_medicine_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.medicineNameEnter_edit.setText(medicineList.get(position).getMedicineName());
        holder.timesEnter_edit.setText(medicineList.get(position).getNoOfTime());
        holder.daysEnter_edit.setText(medicineList.get(position).getNoofDays());

    }

    public void insert(MedicDetail data) {
        medicineList.add(data);
        notifyItemInserted(medicineList.size());
    }

    @Override
    public int getItemCount() {

        return medicineList.size();
        //return (medicineList != null ? medicineList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private EditText medicineNameEnter_edit, daysEnter_edit, timesEnter_edit;

        public ViewHolder(View itemView) {
            super(itemView);

            timesEnter_edit = itemView.findViewById(R.id.timesEnter_edit);
            medicineNameEnter_edit = itemView.findViewById(R.id.medicineNameEnter_edit);
            daysEnter_edit = itemView.findViewById(R.id.daysEnter_edit);
        }
    }
}
