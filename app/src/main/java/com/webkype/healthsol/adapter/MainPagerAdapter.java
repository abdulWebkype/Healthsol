package com.webkype.healthsol.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.webkype.healthsol.R;
import com.webkype.healthsol.model.HomeBannerModels.BannersList;

import java.util.List;

public class MainPagerAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<BannersList> sliderModelList;

    public MainPagerAdapter(Context context, List<BannersList> images) {
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sliderModelList = images;
    }

    @Override
    public int getCount() {
        return sliderModelList.size();

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.slider_layout, container, false);

        ProgressBar progressBar = itemView.findViewById(R.id.progressBarPager);
        progressBar.setVisibility(View.VISIBLE);
        ImageView sliderImage = itemView.findViewById(R.id.sliderImageView);
        TextView bannerTIittle = itemView.findViewById(R.id.bannerTIittle);

        BannersList sliderModel = sliderModelList.get(position);
        bannerTIittle.setText(Html.fromHtml(sliderModel.getBannerText()));


        Glide.with(mContext)
                .load(sliderModel.getBannerImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        //progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(sliderImage);


        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
