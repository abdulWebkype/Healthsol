package com.webkype.healthsol.adapter;

import android.app.ActivityManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webkype.healthsol.MeFamilyModule.MeFamilyActivity;
import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.AddMemberActivity;
import com.webkype.healthsol.activity.HealthiliciousActivity;
import com.webkype.healthsol.activity.HomeActivity;
import com.webkype.healthsol.activity.NotificationActivity;
import com.webkype.healthsol.activity.PrescriptionActivity;
import com.webkype.healthsol.activity.PrescriptionRecordActivity;
import com.webkype.healthsol.activity.ProfileActivity;
import com.webkype.healthsol.activity.TestRecordActivity;
import com.webkype.healthsol.activity.TestReportActivity;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;

import java.util.List;

public class RecyclerAdpaterProfile extends RecyclerView.Adapter<RecyclerAdpaterProfile.ViewHolder> {

    private Context context;
    private List<FamilyListPojo> memberProfileList;
    private String profileName;
    String userId;

    public RecyclerAdpaterProfile(Context context, List<FamilyListPojo> memberProfileList) {
        this.context = context;
        this.memberProfileList = memberProfileList;
        userId = Preference.getString(context, PrefManager.USER_ID);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final FamilyListPojo memberProfile = memberProfileList.get(position);
        String memId = memberProfileList.get(position).getMemberId();
        holder.profileName.setText(memberProfile.getName());

        if (memId.equals(userId)) {
            String topName = memberProfile.getName();

        }


        holder.profileLayoutHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileName = holder.profileName.getText().toString();


                ProfileData.setMemberProfile(context, profileName);
                ProfileData.setMemberId(context, memberProfileList.get(position).getMemberId());

                ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> alltasks = am.getRunningTasks(1);
                for (ActivityManager.RunningTaskInfo aTask : alltasks) {

                    if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.HomeActivity")) {
                        ((HomeActivity) context).selectProfile_textView.setText(profileName);
                        ((HomeActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.MeFamilyActivity")) {
                        ((MeFamilyActivity) context).selectProfileTextView.setText(profileName);
                        ((MeFamilyActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.PrescriptionActivity")) {
                        ((PrescriptionActivity) context).selectProfile_textView.setText(profileName);
                        ((PrescriptionActivity) context).selectProfileLayout.setVisibility(View.GONE);

                        ((PrescriptionActivity) context).getMemberData(memberProfileList.get(position).getMemberId());

                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.PrescriptionRecordActivity")) {
                        ((PrescriptionRecordActivity) context).selectProfile_textView.setText(profileName);
                        ((PrescriptionRecordActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.TestReportActivity")) {
                        ((TestReportActivity) context).selectProfile_textView.setText(profileName);
                        ((TestReportActivity) context).selectProfileLayout.setVisibility(View.GONE);
                        ((TestReportActivity) context).getMemberData(memberProfileList.get(position).getMemberId());
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.TestRecordActivity")) {
                        ((TestRecordActivity) context).selectProfile_textView.setText(profileName);
                        ((TestRecordActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.NotificationActivity")) {
                        ((NotificationActivity) context).selectProfile_textView.setText(profileName);
                        ((NotificationActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.HealthiliciousActivity")) {
                        ((HealthiliciousActivity) context).selectProfile_textView.setText(profileName);
                        ((HealthiliciousActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.ProfileActivity")) {
                        ((ProfileActivity) context).selectProfileTextView.setText(profileName);
                        ((ProfileActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    } else if (aTask.topActivity.getClassName().equals("com.webkype.healthsol.activity.AddMemberActivity")) {
                        ((AddMemberActivity) context).selectProfile_textView.setText(profileName);
                        ((AddMemberActivity) context).selectProfileLayout.setVisibility(View.GONE);
                    }

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return (memberProfileList != null ? memberProfileList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView profileName;
        private RelativeLayout profileLayoutHome;

        public ViewHolder(View itemView) {
            super(itemView);

            profileName = itemView.findViewById(R.id.profileName);
            profileLayoutHome = itemView.findViewById(R.id.profileLayoutHome);
        }
    }
}
