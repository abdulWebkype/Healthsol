package com.webkype.healthsol.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.MeFamilyModule.MeFamilyActivity;
import com.webkype.healthsol.R;
import com.webkype.healthsol.activity.AddMemberActivity;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;

import java.util.List;

public class RecyclerAdapterAllMembers extends RecyclerView.Adapter<RecyclerAdapterAllMembers.ViewHolder> {

    private Context context;
    private List<FamilyListPojo> allMembersList;
    MeFamilyActivity meFamilyActivity;


    public RecyclerAdapterAllMembers(Context context, List<FamilyListPojo> allMembersList, MeFamilyActivity meFamilyActivity) {
        this.context = context;
        this.allMembersList = allMembersList;
        this.meFamilyActivity = meFamilyActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_member_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        final FamilyListPojo listPojo = allMembersList.get(position);
        Glide.with(context).load(listPojo.getMemberImage())
                .into(holder.memberImage_imageView);


        String name = listPojo.getName();
        holder.memberName_textView.setText(name);
        String relation = allMembersList.get(position).getRelation();
        holder.memberRelation_textView.setText(relation);

        if (relation.equals("self")) {
            holder.deleteButtonLayout.setVisibility(View.GONE);
        } else {
            holder.deleteButtonLayout.setVisibility(View.VISIBLE);
        }

        holder.deleteButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialg(position);

            }
        });
        holder.editButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddMemberActivity.class);
                intent.putExtra("memberId", listPojo.getMemberId());
                intent.putExtra("forEdit", true);
                context.startActivity(intent);
            }
        });
    }

    private void showDialg(int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to delete this member?");


        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                meFamilyActivity.deleteFamilyMember(allMembersList, position);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();

    }

    @Override
    public int getItemCount() {
        if (allMembersList != null) {
            return allMembersList.size();
        } else {
            return 0;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView memberImage_imageView;
        private TextView memberName_textView, memberRelation_textView;
        private RelativeLayout editButtonLayout, deleteButtonLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            memberImage_imageView = itemView.findViewById(R.id.memberImage_imageView);
            memberName_textView = itemView.findViewById(R.id.memberName_textView);
            memberRelation_textView = itemView.findViewById(R.id.memberRelation_textView);
            editButtonLayout = itemView.findViewById(R.id.editButtonLayout);
            deleteButtonLayout = itemView.findViewById(R.id.deleteButtonLayout);
        }
    }
}
