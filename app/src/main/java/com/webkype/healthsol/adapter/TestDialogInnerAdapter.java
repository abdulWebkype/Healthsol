package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.TestDialogPojos.CatDetailsPojo;

import java.util.List;

public class TestDialogInnerAdapter extends RecyclerView.Adapter<TestDialogInnerAdapter.DialogViewHolder> {

    private List<CatDetailsPojo> innerList;
    private Context context;

    public TestDialogInnerAdapter(Context context, List<CatDetailsPojo> innerList) {

        this.context = context;
        this.innerList = innerList;

    }

    @NonNull
    @Override
    public DialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medicine_list, parent, false);
        return new DialogViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull DialogViewHolder holder, int position) {

        holder.medicineName_textView.setText(innerList.get(position).getTestName());
        holder.medicineDays_textView.setText(innerList.get(position).getRemarks());
        holder.medicineFreq_textView.setText(innerList.get(position).getResult());

    }


    @Override
    public int getItemCount() {
        return innerList.size();
    }

    class DialogViewHolder extends RecyclerView.ViewHolder {

        private final TextView medicineName_textView;
        private final TextView medicineDays_textView;
        private final TextView medicineFreq_textView;


        public DialogViewHolder(View itemView) {
            super(itemView);

            medicineName_textView = itemView.findViewById(R.id.medicineName_textView);
            medicineDays_textView = itemView.findViewById(R.id.medicineDays_textView);
            medicineFreq_textView = itemView.findViewById(R.id.medicineFreq_textView);

        }
    }
}
