package com.webkype.healthsol.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.TestDetails;
import com.webkype.healthsol.utility.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class OuterRecyclerAdapter extends RecyclerView.Adapter {
//    List<TestDetails> list=new ArrayList<>();
//    HashMap<Integer, List<TestDetails>> hashMap = new HashMap<>();

    private final RecyclerView.RecycledViewPool viewPool;
    Context context;
    int count;
    List<TestDetails> testDetails;


    AddTestResultAdapter addMedicineRecyclerAdapter;

    public OuterRecyclerAdapter(Context context, int count) {
//        if (CommonUtils.hashMap.get(0) != null) {
//
//        } else {
//            CommonUtils.hashMap.put(0, CommonUtils.list);
//        }

        //hashMap.put(1,list);
        //hashMap.put(2,list);
        this.context = context;
        this.count = count;
        viewPool = new RecyclerView.RecycledViewPool();


    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outer_recycler_item, null, false);
        return new OuterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        OuterViewHolder viewHolder = (OuterViewHolder) holder;


        testDetails = new ArrayList<>();
        testDetails = CommonUtils.hashMap.get(position);
//
        viewHolder.testReportRecycler.setRecycledViewPool(viewPool);


        AddTestResultAdapter addMedicineRecyclerAdapter = new AddTestResultAdapter(context, testDetails);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        viewHolder.testReportRecycler.setLayoutManager(layoutManager);
        viewHolder.testReportRecycler.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        viewHolder.testReportRecycler.setItemAnimator(new DefaultItemAnimator());
        viewHolder.testReportRecycler.setAdapter(addMedicineRecyclerAdapter);
        CommonUtils.adapterHashMap.put(position, addMedicineRecyclerAdapter);

        viewHolder.addMoreMedicine_tV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddTestResultAdapter addTestResultAdapter = CommonUtils.adapterHashMap.get(position);
                if (viewHolder.testCatEditInner.getText().toString().length() == 0) {
                    Toast.makeText(context, "Please add Test category name", Toast.LENGTH_SHORT).show();
                } else {
                    showAddDialog(viewHolder, position, testDetails, addTestResultAdapter);
                }


            }
        });

        if (CommonUtils.stringMap.get(position) != null) {
            viewHolder.testCatEditInner.setText(CommonUtils.stringMap.get(position));
        }
        viewHolder.testCatEditInner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                CommonUtils.stringMap.put(position, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

                CommonUtils.stringMap.put(position, editable.toString());
            }
        });

    }

    private void showAddDialog(OuterViewHolder viewHolder, int position, List<TestDetails> testDetails,
                               AddTestResultAdapter addMedicineRecyclerAdapter) {


        View view1 = LayoutInflater.from(context).inflate(R.layout.add_new_report_dilog, null, false);

        final android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(context)
                .setView(view1)
                .setTitle("Add Test Result")
                .setPositiveButton("Save", null) //Set to null. We override the onclick
                .setNegativeButton("Cancel", null)
                .create();

        final EditText dialogTestNameEdit = view1.findViewById(R.id.dialogTestNameEdit);
        final EditText dialogTestResultEdit = view1.findViewById(R.id.dialogTestResultEdit);
        final EditText dialogTestRemarkEdit = view1.findViewById(R.id.dialogTestRemarkEdit);


        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {


                Button button = ((android.app.AlertDialog) dialog).getButton(android.app.AlertDialog.BUTTON_POSITIVE);
                Button negButton = ((android.app.AlertDialog) dialog).getButton(android.app.AlertDialog.BUTTON_NEGATIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        String item1 = dialogTestNameEdit.getText().toString();
                        String item2 = dialogTestResultEdit.getText().toString();
                        String item3 = dialogTestRemarkEdit.getText().toString();


                        if (item1.length() == 0) {
                            Toast.makeText(context, "Please Enter Test Name", Toast.LENGTH_SHORT).show();
                        } else if (item2.length() == 0) {
                            Toast.makeText(context, "Please Enter Test Result", Toast.LENGTH_SHORT).show();
                        } else {
                            // testDetails.add(new TestDetails(item1, item3, item2));
                            // addMedicineRecyclerAdapter.notifyDataSetChanged();
                            List<TestDetails> tempList = new ArrayList<>();
                            String cat = viewHolder.testCatEditInner.getText().toString();
                            tempList.add(new TestDetails(item1, item3, item2, cat));
//                            testDetails.addAll(tempList);
                            List<TestDetails> mainList = CommonUtils.hashMap.get(position);
                            mainList.addAll(tempList);
                            CommonUtils.hashMap.put(position, mainList);
                            CommonUtils.stringMap.put(position, cat);
                            //      testDetails.add(new TestDetails(item1, item3, item2));
                            //  addMedicineRecyclerAdapter.notifyDataSetChanged();

                            notifyItemChanged(position);
                            notifyDataSetChanged();
                            addMedicineRecyclerAdapter.notifyItemInserted(position);

                            dialog.dismiss();

                        }
                    }
                });

            }
        });

        dialog.show();
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public void notifyOuterADapter(int pos) {


        notifyItemChanged(pos);
    }

    class OuterViewHolder extends RecyclerView.ViewHolder {

        RecyclerView testReportRecycler;
        TextView addMoreMedicine_tV;
        EditText testCatEditInner;

        public OuterViewHolder(View itemView) {
            super(itemView);

            testReportRecycler = itemView.findViewById(R.id.testReportRecycler1);

            addMoreMedicine_tV = itemView.findViewById(R.id.addMoreRow);
            testCatEditInner = itemView.findViewById(R.id.testCatEditInner);


        }
    }
}
