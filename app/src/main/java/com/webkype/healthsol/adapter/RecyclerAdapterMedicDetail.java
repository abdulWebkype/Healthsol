package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.PresDialogPojos.DialogMedPres;

import java.util.List;

public class RecyclerAdapterMedicDetail extends RecyclerView.Adapter<RecyclerAdapterMedicDetail.ViewHolder> {

    private Context context;
    private List<DialogMedPres> medicDetailList;

    public RecyclerAdapterMedicDetail(Context context, List<DialogMedPres> medicDetailList) {
        this.context = context;
        this.medicDetailList = medicDetailList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.medicine_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DialogMedPres medicDetail = medicDetailList.get(position);

        holder.medicineName_textView.setText(medicDetail.getMedicine_name());
        holder.medicineDays_textView.setText(medicDetail.getNo_of_days());
        holder.medicineFreq_textView.setText(medicDetail.getNo_of_times() + " Times");
    }

    @Override
    public int getItemCount() {
        return (medicDetailList != null ? medicDetailList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView medicineName_textView, medicineDays_textView, medicineFreq_textView;

        public ViewHolder(View itemView) {
            super(itemView);

            medicineName_textView = itemView.findViewById(R.id.medicineName_textView);
            medicineDays_textView = itemView.findViewById(R.id.medicineDays_textView);
            medicineFreq_textView = itemView.findViewById(R.id.medicineFreq_textView);
        }
    }
}
