package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.R;
import com.webkype.healthsol.model.ImageList;

import java.util.ArrayList;
import java.util.List;


/*
*
* ADAPTER NOT IN USE...
*
* */
public class RecyclerAdapterReprotFiles extends RecyclerView.Adapter<RecyclerAdapterReprotFiles.ViewHolder> {

    private Context context;
    private List<ImageList> imageListList = new ArrayList<>();

    public RecyclerAdapterReprotFiles(Context context, List<ImageList> imageListList){
        this.context = context;
        this.imageListList = imageListList;
    }

    @NonNull
    @Override
    public RecyclerAdapterReprotFiles.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_report_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterReprotFiles.ViewHolder holder, final int position) {
        final ImageList imageList = imageListList.get(position);
        Glide.with(context).load(imageList.getImageList()).into(holder.imageList_imageView);
        holder.closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageListList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, imageListList.size());
              /* UploadPrescpFragmnet uploadTestReportFragment = new UploadPrescpFragmnet();
                uploadTestReportFragment.remove(position);
                notifyItemChanged(position);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return (imageListList != null ? imageListList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView closeImage, imageList_imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            closeImage = itemView.findViewById(R.id.closeImage);
            imageList_imageView = itemView.findViewById(R.id.imageList_imageView);
        }
    }
}
