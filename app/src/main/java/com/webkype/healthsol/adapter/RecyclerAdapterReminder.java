package com.webkype.healthsol.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.model.ReminderListPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class RecyclerAdapterReminder extends RecyclerView.Adapter<RecyclerAdapterReminder.ViewHolder> {

    private Context context;
    private List<ReminderListPojo> reminderModelList;
    private ApiInterface apiInterface;
    private String userId;
    ProgressDialog progressDialog;

    public RecyclerAdapterReminder(Context context, List<ReminderListPojo> reminderModelList, ApiInterface apiInterface, String userId) {
        this.context = context;
        this.reminderModelList = reminderModelList;
        this.apiInterface = apiInterface;
        this.userId = userId;

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Deleting Reminder...");

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reminder_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        String date = reminderModelList.get(position).getReminderDate();
        if (date.contains("-")) {
            String[] adad = date.split("-");
            String day = adad[0];
            String month = adad[1];

            holder.reminderDateText.setText(day);
            holder.reminderMonthText.setText(month);
        }

        holder.reminderTitleText.setText(reminderModelList.get(position).getTitle());
        holder.reminderDescText.setText(reminderModelList.get(position).getReminderFor());


        holder.deleteReminderImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //hit delete api.................................................
                deleteReminder(reminderModelList.get(position).getReminderId(), position);
                /*reminderModelList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, reminderModelList.size());*/
            }
        });
    }

    private void deleteReminder(String reminderId, int pos) {
        progressDialog.show();

        apiInterface.deleteReminder(userId, reminderId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody value) {

                        progressDialog.dismiss();
                        try {
                            String res = value.string().toString();
                            JSONObject jsonObject = new JSONObject(res);
                            String status = jsonObject.getString("status");
                            if (status.equals("200")) {
                                reminderModelList.remove(pos);
                                notifyDataSetChanged();
                            }
                            String msg = jsonObject.getString("msg");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    @Override
    public int getItemCount() {
        return (reminderModelList != null ? reminderModelList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView reminderDateText, reminderMonthText, reminderTitleText, reminderDescText, reminderDescSecondText;
        private ImageView deleteReminderImage;

        public ViewHolder(View itemView) {
            super(itemView);

            reminderDateText = itemView.findViewById(R.id.reminderDateText);
            reminderMonthText = itemView.findViewById(R.id.reminderMonthText);
            reminderTitleText = itemView.findViewById(R.id.reminderTitleText);
            reminderDescText = itemView.findViewById(R.id.reminderDescText);
            deleteReminderImage = itemView.findViewById(R.id.deleteReminderImage);
        }
    }
}
