package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.model.NotificationListInner;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterNotifications extends RecyclerView.Adapter<RecyclerAdapterNotifications.ViewHolder> {

    private Context context;
    private List<NotificationListInner> notifyList = new ArrayList<>();

    public RecyclerAdapterNotifications(Context context, List<NotificationListInner> notifyList) {
        this.context = context;
        this.notifyList = notifyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationListInner notify = notifyList.get(position);
        holder.notificationDateText.setText(notify.getNotificationDate());
        //holder.notificationMonthText.setText(notify.getMonth());
        holder.notificationTitleText.setText(notify.getNotificationTitle());
    }

    @Override
    public int getItemCount() {
        return (notifyList != null ? notifyList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView notificationTitleText, notificationMonthText, notificationDateText;

        public ViewHolder(View itemView) {
            super(itemView);

            notificationDateText = itemView.findViewById(R.id.notificationDateText);
            notificationMonthText = itemView.findViewById(R.id.notificationMonthText);
            notificationTitleText = itemView.findViewById(R.id.notificationTitleText);
        }
    }
}
