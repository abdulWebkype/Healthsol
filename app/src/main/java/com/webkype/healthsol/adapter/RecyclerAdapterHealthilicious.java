package com.webkype.healthsol.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.webkype.healthsol.R;
import com.webkype.healthsol.model.HealthiliciousModel;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterHealthilicious extends RecyclerView.Adapter<RecyclerAdapterHealthilicious.ViewHolder> {

    private Context context;
    private List<HealthiliciousModel> healthiliciousModelList = new ArrayList<>();
    int c = 0;

    public RecyclerAdapterHealthilicious(Context context, List<HealthiliciousModel> healthiliciousModelList){
        this.context = context;
        this.healthiliciousModelList = healthiliciousModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.healthy_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HealthiliciousModel healthiliciousModel = healthiliciousModelList.get(position);
        Glide.with(context).load(healthiliciousModel.getImage()).into(holder.healthyImage);
        holder.healthyText.setText(healthiliciousModel.getText());
        holder.readMoreButton_textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.healthyLayout.setVisibility(View.VISIBLE);
                holder.readMoreButton_textView.setText("Hide..");
                c++;
                if(c%2 == 0){
                    holder.healthyLayout.setVisibility(View.GONE);
                    holder.readMoreButton_textView.setText("Read More..");
                } else {
                    holder.healthyLayout.setVisibility(View.VISIBLE);
                    holder.readMoreButton_textView.setText("Hide..");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (healthiliciousModelList != null ? healthiliciousModelList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView healthyImage;
        private TextView healthyText, readMoreButton_textView, healthyDetails_textView;
        private RelativeLayout healthyLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            healthyImage = itemView.findViewById(R.id.healthyImage);
            healthyText = itemView.findViewById(R.id.healthyText);
            readMoreButton_textView = itemView.findViewById(R.id.readMoreButton_textView);
            healthyLayout = itemView.findViewById(R.id.healthyLayout);
        }
    }
}
