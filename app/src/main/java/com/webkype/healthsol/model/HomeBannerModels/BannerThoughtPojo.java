package com.webkype.healthsol.model.HomeBannerModels;

public class BannerThoughtPojo {


    private String title;

    private String thought;

    private String publishBy;


    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getThought ()
    {
        return thought;
    }

    public void setThought (String thought)
    {
        this.thought = thought;
    }

    public String getPublishBy ()
    {
        return publishBy;
    }

    public void setPublishBy (String publishBy)
    {
        this.publishBy = publishBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", thought = "+thought+", publishBy = "+publishBy+"]";
    }
}
