package com.webkype.healthsol.model;

public class HealthiliciousModel {

    private int image;
    private String text;

    public HealthiliciousModel(int image, String text){
        this.image = image;
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public String getText() {
        return text;
    }
}
