package com.webkype.healthsol.model;

public class TestRecord {

    private String date;
    private String doctorName;
    private String labName;

    public TestRecord(String date, String labName, String doctorName){
        this.date = date;
        this.doctorName = doctorName;
        this.labName = labName;
    }

    public String getDate() {
        return date;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getLabName() {
        return labName;
    }
}
