package com.webkype.healthsol.model.TestReportRecords;

public class TestRecordListPojo {

    private String testDate;

    private String prescrdBy;

    private String testId;

    private String labName;

    public String getTestDate ()
    {
        return testDate;
    }

    public void setTestDate (String testDate)
    {
        this.testDate = testDate;
    }

    public String getPrescrdBy ()
    {
        return prescrdBy;
    }

    public void setPrescrdBy (String prescrdBy)
    {
        this.prescrdBy = prescrdBy;
    }

    public String getTestId ()
    {
        return testId;
    }

    public void setTestId (String testId)
    {
        this.testId = testId;
    }

    public String getLabName ()
    {
        return labName;
    }

    public void setLabName (String labName)
    {
        this.labName = labName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [testDate = "+testDate+", prescrdBy = "+prescrdBy+", testId = "+testId+", labName = "+labName+"]";
    }

}
