package com.webkype.healthsol.model;

import java.util.List;

public class ReminderMainPojo {


    private String status;

    private List<ReminderListPojo> reminderList;

    private String msg;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<ReminderListPojo> getReminderList ()
    {
        return reminderList;
    }

    public void setReminderList (List<ReminderListPojo> reminderList)
    {
        this.reminderList = reminderList;
    }

    public String getMsg ()
    {
        return msg;
    }

    public void setMsg (String msg)
    {
        this.msg = msg;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", reminderList = "+reminderList+", msg = "+msg+"]";
    }

}
