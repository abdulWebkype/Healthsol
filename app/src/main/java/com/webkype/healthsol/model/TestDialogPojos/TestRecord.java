package com.webkype.healthsol.model.TestDialogPojos;

import com.webkype.healthsol.model.TestReportRecords.DocumentList;

import java.util.List;

public class TestRecord {

    private String testDate;

    private String prescrdBy;

    private String patientName;

    private String testId;

    private String[] documentList;

    private String labName;

    private String remarks;

    private List<DescDetailList> descpDetList;

    private List<TestRecordDetailsList> testDetList;

    private String sampleType;

    public String getTestDate ()
    {
        return testDate;
    }

    public void setTestDate (String testDate)
    {
        this.testDate = testDate;
    }

    public String getPrescrdBy ()
    {
        return prescrdBy;
    }

    public void setPrescrdBy (String prescrdBy)
    {
        this.prescrdBy = prescrdBy;
    }

    public String getPatientName ()
    {
        return patientName;
    }

    public void setPatientName (String patientName)
    {
        this.patientName = patientName;
    }

    public String getTestId ()
    {
        return testId;
    }

    public void setTestId (String testId)
    {
        this.testId = testId;
    }

    public String[] getDocumentList ()
    {
        return documentList;
    }

    public void setDocumentList (String[] documentList)
    {
        this.documentList = documentList;
    }

    public String getLabName ()
    {
        return labName;
    }

    public void setLabName (String labName)
    {
        this.labName = labName;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public List<DescDetailList> getDescpDetList ()
    {
        return descpDetList;
    }

    public void setDescpDetList (List<DescDetailList> descpDetList)
    {
        this.descpDetList = descpDetList;
    }

    public List<TestRecordDetailsList> getTestDetList ()
    {
        return testDetList;
    }

    public void setTestDetList (List<TestRecordDetailsList> testDetList)
    {
        this.testDetList = testDetList;
    }

    public String getSampleType ()
    {
        return sampleType;
    }

    public void setSampleType (String sampleType)
    {
        this.sampleType = sampleType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [testDate = "+testDate+", prescrdBy = "+prescrdBy+", patientName = "+patientName+", testId = "+testId+", documentList = "+documentList+", labName = "+labName+", remarks = "+remarks+", descpDetList = "+descpDetList+", testDetList = "+testDetList+", sampleType = "+sampleType+"]";
    }
}
