package com.webkype.healthsol.model.TestDialogPojos;

public class CatDetailsPojo {

    private String id;

    private String result;

    private String testName;

    private String remarks;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getResult ()
    {
        return result;
    }

    public void setResult (String result)
    {
        this.result = result;
    }

    public String getTestName ()
    {
        return testName;
    }

    public void setTestName (String testName)
    {
        this.testName = testName;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", result = "+result+", testName = "+testName+", remarks = "+remarks+"]";
    }
}
