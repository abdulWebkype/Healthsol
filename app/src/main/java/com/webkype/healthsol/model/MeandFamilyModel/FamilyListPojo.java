package com.webkype.healthsol.model.MeandFamilyModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FamilyListPojo {


    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("relation")
    private String relation;

    @Expose
    @SerializedName("memberId")
    private String memberId;

    @Expose
    @SerializedName("memberImage")
    private String memberImage;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getRelation ()
    {
        return relation;
    }

    public void setRelation (String relation)
    {
        this.relation = relation;
    }

    public String getMemberId ()
    {
        return memberId;
    }

    public void setMemberId (String memberId)
    {
        this.memberId = memberId;
    }

    public String getMemberImage ()
    {
        return memberImage;
    }

    public void setMemberImage (String memberImage)
    {
        this.memberImage = memberImage;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", relation = "+relation+", memberId = "+memberId+", memberImage = "+memberImage+"]";
    }
}
