package com.webkype.healthsol.model.TestDialogPojos;

import java.util.List;

public class TestRecordDetailsList {

    private String categoryName;

    private List<CatDetailsPojo> catDetails;

    public String getCategoryName ()
    {
        return categoryName;
    }

    public void setCategoryName (String categoryName)
    {
        this.categoryName = categoryName;
    }

    public List<CatDetailsPojo> getCatDetails ()
    {
        return catDetails;
    }

    public void setCatDetails (List<CatDetailsPojo> catDetails)
    {
        this.catDetails = catDetails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [categoryName = "+categoryName+", catDetails = "+catDetails+"]";
    }
}
