package com.webkype.healthsol.model.PresDialogPojos;

import java.util.List;

public class PresDialogMain {

    private String status;

    private PresDialogRecords presRecord;

    private String header;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public PresDialogRecords getPresRecord ()
    {
        return presRecord;
    }

    public void setPresRecord (PresDialogRecords presRecord)
    {
        this.presRecord = presRecord;
    }

    public String getHeader ()
    {
        return header;
    }

    public void setHeader (String header)
    {
        this.header = header;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", presRecord = "+presRecord+", header = "+header+"]";
    }
}
