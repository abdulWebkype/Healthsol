package com.webkype.healthsol.model.TestReportRecords;

public class DocumentList {

    private String id;

    private String t_id;

    private String category_name;

    private String description;

    private String adddatetime;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getT_id ()
    {
        return t_id;
    }

    public void setT_id (String t_id)
    {
        this.t_id = t_id;
    }

    public String getCategory_name ()
    {
        return category_name;
    }

    public void setCategory_name (String category_name)
    {
        this.category_name = category_name;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getAdddatetime ()
    {
        return adddatetime;
    }

    public void setAdddatetime (String adddatetime)
    {
        this.adddatetime = adddatetime;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", t_id = "+t_id+", category_name = "+category_name+", description = "+description+", adddatetime = "+adddatetime+"]";
    }
}
