package com.webkype.healthsol.model;

public class AllMembers {

    private String memberImage;
    private String memberName;
    private String memberRelation;

    public AllMembers(String memberImage, String memberName, String memberRelation){
        this.memberImage = memberImage;
        this.memberName = memberName;
        this.memberRelation = memberRelation;
    }

    public String getMemberImage() {
        return memberImage;
    }

    public String getMemberName() {
        return memberName;
    }

    public String getMemberRelation() {
        return memberRelation;
    }

}
