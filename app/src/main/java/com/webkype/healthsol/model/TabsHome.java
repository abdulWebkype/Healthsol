package com.webkype.healthsol.model;

public class TabsHome {

    private int tabImage;
    private String tabName;
    private String id;

    public TabsHome(int tabImage, String tabName, String id){
        this.tabImage = tabImage;
        this.tabName = tabName;
        this.id = id;
    }

    public int getTabImage() {
        return tabImage;
    }

    public String getTabName() {
        return tabName;
    }

    public String getId() {
        return id;
    }
}
