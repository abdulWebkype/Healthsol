package com.webkype.healthsol.model.PrescriptionRecords;

public class PresRecordsListPojo {


    private String doctorExp;

    private String presDate;

    private String doctorName;

    private String presId;

    public String getDoctorExp ()
    {
        return doctorExp;
    }

    public void setDoctorExp (String doctorExp)
    {
        this.doctorExp = doctorExp;
    }

    public String getPresDate ()
    {
        return presDate;
    }

    public void setPresDate (String presDate)
    {
        this.presDate = presDate;
    }

    public String getDoctorName ()
    {
        return doctorName;
    }

    public void setDoctorName (String doctorName)
    {
        this.doctorName = doctorName;
    }

    public String getPresId ()
    {
        return presId;
    }

    public void setPresId (String presId)
    {
        this.presId = presId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [doctorExp = "+doctorExp+", presDate = "+presDate+", doctorName = "+doctorName+", presId = "+presId+"]";
    }
}
