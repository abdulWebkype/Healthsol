package com.webkype.healthsol.model;

public class MedicDetail {

    private String medicineName;
    private String noofDays;
    private String noOfTime;

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getNoofDays() {
        return noofDays;
    }

    public void setNoofDays(String noofDays) {
        this.noofDays = noofDays;
    }

    public String getNoOfTime() {
        return noOfTime;
    }

    public void setNoOfTime(String noOfTime) {
        this.noOfTime = noOfTime;
    }

    public  MedicDetail(String medicineName, String noofDays, String noOfTime){
        this.medicineName = medicineName;
        this.noofDays = noofDays;
        this.noOfTime = noOfTime;
    }


}
