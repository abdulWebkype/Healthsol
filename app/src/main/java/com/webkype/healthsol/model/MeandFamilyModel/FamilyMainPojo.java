package com.webkype.healthsol.model.MeandFamilyModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FamilyMainPojo {

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("members")
    private List<FamilyListPojo> members;

    @Expose
    @SerializedName("header")
    private String header;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FamilyListPojo> getMembers() {
        return members;
    }

    public void setMembers(List<FamilyListPojo> members) {
        this.members = members;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", members = " + members + ", header = " + header + "]";
    }
}
