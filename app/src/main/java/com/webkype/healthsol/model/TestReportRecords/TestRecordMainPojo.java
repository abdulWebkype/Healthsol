package com.webkype.healthsol.model.TestReportRecords;

import java.util.List;

public class TestRecordMainPojo {


    private String status;

    private List<TestRecordListPojo> presRecord;

    private String header;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<TestRecordListPojo> getPresRecord ()
    {
        return presRecord;
    }

    public void setPresRecord (List<TestRecordListPojo> presRecord)
    {
        this.presRecord = presRecord;
    }

    public String getHeader ()
    {
        return header;
    }

    public void setHeader (String header)
    {
        this.header = header;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", presRecord = "+presRecord+", header = "+header+"]";
    }
}
