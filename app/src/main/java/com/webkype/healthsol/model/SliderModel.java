package com.webkype.healthsol.model;

public class SliderModel {

    private String slideImage;

    public SliderModel(String slideImage){
        this.slideImage = slideImage;
    }

    public String getSlideImage() {
        return slideImage;
    }
}
