package com.webkype.healthsol.model;

public class Notify {

    private String date;
    private String month;
    private String notification;

    public Notify(String date, String month, String notification){
        this.date = date;
        this.month = month;
        this.notification = notification;
    }

    public String getDate() {
        return date;
    }

    public String getMonth() {
        return month;
    }

    public String getNotification() {
        return notification;
    }
}
