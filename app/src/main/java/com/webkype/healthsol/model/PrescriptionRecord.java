package com.webkype.healthsol.model;

public class PrescriptionRecord {

    private String date;
    private String doctorName;
    private String doctorExpertise;

    public PrescriptionRecord(String date, String doctorName, String doctorExpertise){
        this.date = date;
        this.doctorName = doctorName;
        this.doctorExpertise = doctorExpertise;
    }

    public String getDate() {
        return date;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getDoctorExpertise() {
        return doctorExpertise;
    }
}
