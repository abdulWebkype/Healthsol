package com.webkype.healthsol.model.TestDialogPojos;

public class TestRecordDMain {

    private String status;

    private TestRecord testRecord;

    private String header;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public TestRecord getTestRecord ()
    {
        return testRecord;
    }

    public void setTestRecord (TestRecord testRecord)
    {
        this.testRecord = testRecord;
    }

    public String getHeader ()
    {
        return header;
    }

    public void setHeader (String header)
    {
        this.header = header;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", testRecord = "+testRecord+", header = "+header+"]";
    }
}
