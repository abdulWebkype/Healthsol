package com.webkype.healthsol.model;

public class ReminderListPojo {


    private String title;

    private String reminderFor;

    private String reminderDate;

    private String reminderId;

    private String isRead;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReminderFor() {
        return reminderFor;
    }

    public void setReminderFor(String reminderFor) {
        this.reminderFor = reminderFor;
    }

    public String getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(String reminderDate) {
        this.reminderDate = reminderDate;
    }

    public String getReminderId() {
        return reminderId;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    @Override
    public String toString() {
        return "ClassPojo [title = " + title + ", reminderFor = " + reminderFor + ", reminderDate = " + reminderDate + ", reminderId = " + reminderId + ", isRead = " + isRead + "]";
    }

}
