package com.webkype.healthsol.model;

public class DescriptiveModel {

    public String getDesCatName() {
        return desCatName;
    }

    public void setDesCatName(String desCatName) {
        this.desCatName = desCatName;
    }

    public String getDesResult() {
        return desResult;
    }

    public void setDesResult(String desResult) {
        this.desResult = desResult;
    }

    String desCatName, desResult;
}
