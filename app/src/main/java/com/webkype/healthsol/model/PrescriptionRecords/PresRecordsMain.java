package com.webkype.healthsol.model.PrescriptionRecords;

import java.util.List;

public class PresRecordsMain {


    private String status;

    private List<PresRecordsListPojo> presRecord;

    private String header;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<PresRecordsListPojo> getPresRecord ()
    {
        return presRecord;
    }

    public void setPresRecord (List<PresRecordsListPojo> presRecord)
    {
        this.presRecord = presRecord;
    }

    public String getHeader ()
    {
        return header;
    }

    public void setHeader (String header)
    {
        this.header = header;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", presRecord = "+presRecord+", header = "+header+"]";
    }

}
