package com.webkype.healthsol.model;

public class NotificationListInner {


    private String notificationId;

    private String notificationDate;

    private String notificationTitle;

    public String getNotificationId ()
    {
        return notificationId;
    }

    public void setNotificationId (String notificationId)
    {
        this.notificationId = notificationId;
    }

    public String getNotificationDate ()
    {
        return notificationDate;
    }

    public void setNotificationDate (String notificationDate)
    {
        this.notificationDate = notificationDate;
    }

    public String getNotificationTitle ()
    {
        return notificationTitle;
    }

    public void setNotificationTitle (String notificationTitle)
    {
        this.notificationTitle = notificationTitle;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [notificationId = "+notificationId+", notificationDate = "+notificationDate+", notificationTitle = "+notificationTitle+"]";
    }
}
