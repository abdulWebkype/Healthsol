package com.webkype.healthsol.model;

import java.util.List;

public class NotificationListMain {


    private List<NotificationListInner> notificationList;

    public List<NotificationListInner> getNotificationList ()
    {
        return notificationList;
    }

    public void setNotificationList (List<NotificationListInner> notificationList)
    {
        this.notificationList = notificationList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [notificationList = "+notificationList+"]";
    }
}
