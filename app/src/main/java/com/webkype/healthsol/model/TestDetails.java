package com.webkype.healthsol.model;

public class TestDetails {

    String testName;
    String testResult;
    String testRemark;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    String category;

    public TestDetails(String testName, String testResult, String testRemark,String category) {

        this.testName = testName;
        this.testResult = testResult;
        this.testRemark = testRemark;
        this.category = category;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public String getTestRemark() {
        return testRemark;
    }

    public void setTestRemark(String testRemark) {
        this.testRemark = testRemark;
    }
}
