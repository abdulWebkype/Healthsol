package com.webkype.healthsol.model;

public class ImageList {

    private String imageList;

    public ImageList(String imageList){
        this.imageList = imageList;
    }

    public String getImageList() {
        return imageList;
    }
}
