package com.webkype.healthsol.model.HomeBannerModels;

import java.util.List;

public class BannerMainPojo {


    private String status;

    private BannerThoughtPojo thought;

    private String header;

    private List<BannersList> banners;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public BannerThoughtPojo getThought ()
    {
        return thought;
    }

    public void setThought (BannerThoughtPojo thought)
    {
        this.thought = thought;
    }

    public String getHeader ()
    {
        return header;
    }

    public void setHeader (String header)
    {
        this.header = header;
    }

    public List<BannersList> getBanners ()
    {
        return banners;
    }

    public void setBanners (List<BannersList> banners)
    {
        this.banners = banners;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", thought = "+thought+", header = "+header+", banners = "+banners+"]";
    }

}
