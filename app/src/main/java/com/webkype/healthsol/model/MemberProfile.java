package com.webkype.healthsol.model;

public class MemberProfile {

    private String profileName;

    public MemberProfile(String profileName){
        this.profileName = profileName;
    }

    public String getProfileName() {
        return profileName;
    }
}
