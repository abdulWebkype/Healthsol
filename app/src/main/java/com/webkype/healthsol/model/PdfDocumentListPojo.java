package com.webkype.healthsol.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PdfDocumentListPojo implements Parcelable {

    private String id;
    private String p_id;
    private String document;
    private String adddate;

    protected PdfDocumentListPojo(Parcel in) {
        id = in.readString();
        p_id = in.readString();
        document = in.readString();
        adddate = in.readString();
    }

    public static final Creator<PdfDocumentListPojo> CREATOR = new Creator<PdfDocumentListPojo>() {
        @Override
        public PdfDocumentListPojo createFromParcel(Parcel in) {
            return new PdfDocumentListPojo(in);
        }

        @Override
        public PdfDocumentListPojo[] newArray(int size) {
            return new PdfDocumentListPojo[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getAdddate() {
        return adddate;
    }

    public void setAdddate(String adddate) {
        this.adddate = adddate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(p_id);
        parcel.writeString(document);
        parcel.writeString(adddate);
    }
}
