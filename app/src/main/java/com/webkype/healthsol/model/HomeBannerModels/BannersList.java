package com.webkype.healthsol.model.HomeBannerModels;

public class BannersList {

    private String bannerImage;

    private String bannername;

    private String bannerText;

    public String getBannerText() {
        return bannerText;
    }

    public void setBannerText(String bannerText) {
        this.bannerText = bannerText;
    }


    public String getBannerImage ()
    {
        return bannerImage;
    }

    public void setBannerImage (String bannerImage)
    {
        this.bannerImage = bannerImage;
    }

    public String getBannername ()
    {
        return bannername;
    }

    public void setBannername (String bannername)
    {
        this.bannername = bannername;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bannerImage = "+bannerImage+", bannername = "+bannername+"]";
    }

}
