package com.webkype.healthsol.model;

public class ReminderModel {

    private String date, month, title, descfirst, descsecond;

    public ReminderModel(String date, String month, String title, String descfirst, String descsecond){
        this.date = date;
        this.month = month;
        this.title = title;
        this.descfirst = descfirst;
        this.descsecond = descsecond;
    }

    public String getDate() {
        return date;
    }

    public String getMonth() {
        return month;
    }

    public String getDescfirst() {
        return descfirst;
    }

    public String getDescsecond() {
        return descsecond;
    }

    public String getTitle() {
        return title;
    }
}
