package com.webkype.healthsol.model.PresDialogPojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkype.healthsol.model.PdfDocumentListPojo;

import java.util.List;

public class PresDialogRecords {

    private String testAdvised;

    private String docName;

    private String patAge;

    private String patGender;

    private String specialComment;

    private String docAddress;

    private String adddat;

    private String presId;

    private String presType;

    private String docDiagnosis;

    private String docExp;

    private String patComplain;

    private String docMobile;

    private String userId;

    private String patName;

    private String memberId;

    private String proceConducted;

    private String docPhone;

    public List<PdfDocumentListPojo> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<PdfDocumentListPojo> documentList) {
        this.documentList = documentList;
    }

    private List<PdfDocumentListPojo> documentList;

    @Expose
    @SerializedName("medicinePres")
    private List<DialogMedPres> medicinePres;

    public String getTestAdvised() {
        return testAdvised;
    }

    public void setTestAdvised(String testAdvised) {
        this.testAdvised = testAdvised;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getPatAge() {
        return patAge;
    }

    public void setPatAge(String patAge) {
        this.patAge = patAge;
    }

    public String getPatGender() {
        return patGender;
    }

    public void setPatGender(String patGender) {
        this.patGender = patGender;
    }

    public String getSpecialComment() {
        return specialComment;
    }

    public void setSpecialComment(String specialComment) {
        this.specialComment = specialComment;
    }

    public String getDocAddress() {
        return docAddress;
    }

    public void setDocAddress(String docAddress) {
        this.docAddress = docAddress;
    }

    public String getAdddat() {
        return adddat;
    }

    public void setAdddat(String adddat) {
        this.adddat = adddat;
    }

    public String getPresId() {
        return presId;
    }

    public void setPresId(String presId) {
        this.presId = presId;
    }

    public String getPresType() {
        return presType;
    }

    public void setPresType(String presType) {
        this.presType = presType;
    }

    public String getDocDiagnosis() {
        return docDiagnosis;
    }

    public void setDocDiagnosis(String docDiagnosis) {
        this.docDiagnosis = docDiagnosis;
    }

    public String getDocExp() {
        return docExp;
    }

    public void setDocExp(String docExp) {
        this.docExp = docExp;
    }

    public String getPatComplain() {
        return patComplain;
    }

    public void setPatComplain(String patComplain) {
        this.patComplain = patComplain;
    }

    public String getDocMobile() {
        return docMobile;
    }

    public void setDocMobile(String docMobile) {
        this.docMobile = docMobile;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getProceConducted() {
        return proceConducted;
    }

    public void setProceConducted(String proceConducted) {
        this.proceConducted = proceConducted;
    }

    public String getDocPhone() {
        return docPhone;
    }

    public void setDocPhone(String docPhone) {
        this.docPhone = docPhone;
    }

    public List<DialogMedPres> getMedicinePres() {
        return medicinePres;
    }

    public void setMedicinePres(List<DialogMedPres> medicinePres) {
        this.medicinePres = medicinePres;
    }

    @Override
    public String toString() {
        return "ClassPojo [testAdvised = " + testAdvised
                + ", docName = " + docName
                + ", documentList = " + documentList
                + ", patAge = " + patAge + ", patGender = " + patGender + ", specialComment = " + specialComment + ", docAddress = " + docAddress + ", adddat = " + adddat + ", presId = " + presId + ", presType = " + presType + ", docDiagnosis = " + docDiagnosis + ", docExp = " + docExp + ", patComplain = " + patComplain + ", docMobile = " + docMobile + ", userId = " + userId + ", patName = " + patName + ", memberId = " + memberId + ", proceConducted = " + proceConducted + ", docPhone = " + docPhone + ", medicinePres = " + medicinePres + "]";
    }
}
