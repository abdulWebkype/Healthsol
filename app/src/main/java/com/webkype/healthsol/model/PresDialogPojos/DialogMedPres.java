package com.webkype.healthsol.model.PresDialogPojos;

public class DialogMedPres {

    private String id;

    private String no_of_days;

    private String adddate;

    private String p_id;

    private String no_of_times;

    private String medicine_name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getNo_of_days ()
    {
        return no_of_days;
    }

    public void setNo_of_days (String no_of_days)
    {
        this.no_of_days = no_of_days;
    }

    public String getAdddate ()
    {
        return adddate;
    }

    public void setAdddate (String adddate)
    {
        this.adddate = adddate;
    }

    public String getP_id ()
    {
        return p_id;
    }

    public void setP_id (String p_id)
    {
        this.p_id = p_id;
    }

    public String getNo_of_times ()
    {
        return no_of_times;
    }

    public void setNo_of_times (String no_of_times)
    {
        this.no_of_times = no_of_times;
    }

    public String getMedicine_name ()
    {
        return medicine_name;
    }

    public void setMedicine_name (String medicine_name)
    {
        this.medicine_name = medicine_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", no_of_days = "+no_of_days+", adddate = "+adddate+", p_id = "+p_id+", no_of_times = "+no_of_times+", medicine_name = "+medicine_name+"]";
    }
}
