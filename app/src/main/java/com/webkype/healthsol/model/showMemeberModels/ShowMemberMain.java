package com.webkype.healthsol.model.showMemeberModels;

public class ShowMemberMain {



    private String status;

    private ShowMemberList memberProfile;

    private String header;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ShowMemberList getMemberProfile ()
    {
        return memberProfile;
    }

    public void setMemberProfile (ShowMemberList memberProfile)
    {
        this.memberProfile = memberProfile;
    }

    public String getHeader ()
    {
        return header;
    }

    public void setHeader (String header)
    {
        this.header = header;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", memberProfile = "+memberProfile+", header = "+header+"]";
    }
}
