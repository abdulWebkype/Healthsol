package com.webkype.healthsol.MeFamilyModule;

import android.content.Context;
import android.util.Log;

import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyMainPojo;
import com.webkype.healthsol.utility.Preference;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.webkype.healthsol.utility.PrefManager.USER_ID;

public class MeFamilyPresenterImp implements MeFamilyPresenter,
        MeFamilyInteractor.onGettingFamilyData, Observer {

    MeFamilyView meFamilyView;
    MeFamilyInteractor meFamilyInteractorImp;
    Context context;
    String userId;

    public MeFamilyPresenterImp(MeFamilyView meFamilyView, Context context) {
        this.meFamilyInteractorImp = new MeFamilyInteractorImp();
        this.meFamilyView = meFamilyView;
        this.context = context;
        userId = Preference.getString(context, USER_ID);
    }


    @Override
    public void hitAPIForData(String userId) {

        if (meFamilyView != null) {
            meFamilyView.showProgress();
            meFamilyInteractorImp.getFamilyData(this, context, userId, MeFamilyPresenterImp.this);
        }
    }

    @Override
    public void deleteRow(List<FamilyListPojo> listPojo, int position) {

        if (meFamilyInteractorImp != null) {
            meFamilyView.showProgress();
            meFamilyInteractorImp.deleteRow(this, listPojo, position, context);
        }
    }

    @Override
    public void onFailed(String s) {
        meFamilyView.hideProgress();
        meFamilyView.onFailed(s);
    }


    @Override
    public void deleteSuccess(List<FamilyListPojo> listPojo, int position) {
        meFamilyView.hideProgress();
        meFamilyView.onDeleteSuccess(listPojo, position);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object object) {

        if (object != null && object instanceof FamilyMainPojo) {
            FamilyMainPojo familyMainPojo = (FamilyMainPojo) object;
            if (familyMainPojo != null) {
                String status = familyMainPojo.getStatus();
                String header = familyMainPojo.getHeader();
                if (status.equals("200")) {
                    List<FamilyListPojo> list = familyMainPojo.getMembers();
                    List<FamilyListPojo> list1 = new ArrayList<>();
                    for (int i = 0; i < list.size(); i++) {
                        if (!list.get(i).getMemberId().equals(userId)) {
                            list1.add(list.get(i));
                        }
                    }
                    meFamilyView.hideProgress();
                    meFamilyView.onSuccess(list1);
                    Log.e("currentThreadId", String.valueOf(Thread.currentThread().getId()));
                    Log.e("currentThreadName", String.valueOf(Thread.currentThread().getName()));

                } else {
                    // Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

        }

    }

    @Override
    public void onError(Throwable e) {
        meFamilyView.onFailed(e.toString());
    }

    @Override
    public void onComplete() {

    }
}
