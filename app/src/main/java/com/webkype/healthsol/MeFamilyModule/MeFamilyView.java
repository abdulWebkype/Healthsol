package com.webkype.healthsol.MeFamilyModule;

import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;

import java.util.List;

public interface MeFamilyView {

    void showProgress();
    void hideProgress();

    void onFailed(String s);

    void onSuccess(List<FamilyListPojo> list);
    void onDeleteSuccess(List<FamilyListPojo> listPojo,int position);
}
