package com.webkype.healthsol.MeFamilyModule;

import android.content.Context;
import android.widget.Toast;

import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeFamilyInteractorImp implements MeFamilyInteractor {

    ApiInterface apiInterface;

    @Override
    public void deleteRow(final onGettingFamilyData listener, final List<FamilyListPojo> listPojo,
                          final int position, final Context context) {

        String userId = Preference.getString(context, PrefManager.USER_ID);
        Call<ResponseBody> bodyCall = apiInterface.deleteMember(userId, listPojo.get(position).getMemberId());
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (status.equals("200")) {

                            listener.deleteSuccess(listPojo, position);
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void getFamilyData(final onGettingFamilyData listener, final Context context, String userId, Observer observer) {


        apiInterface = new Preference().getInstance();
        apiInterface.myFamilyList(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}
