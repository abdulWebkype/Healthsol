package com.webkype.healthsol.MeFamilyModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.activity.AddMemberActivity;
import com.webkype.healthsol.activity.BaseActivity;
import com.webkype.healthsol.activity.HomeActivity;
import com.webkype.healthsol.adapter.RecyclerAdapterAllMembers;
import com.webkype.healthsol.model.AllMembers;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class MeFamilyActivity extends BaseActivity implements MeFamilyView {

    @BindView(R.id.sideMenu)
    ImageView sideMenu;
    @BindView(R.id.backToHomeMenu)
    ImageView backToHomeMenu;
    @BindView(R.id.actionLAy)
    RelativeLayout actionLAy;
    @BindView(R.id.appTitle)
    TextView appTitle;
    @BindView(R.id.homeItem)
    TextView homeItem;
    @BindView(R.id.homeLayout)
    RelativeLayout homeLayout;
    @BindView(R.id.topBar)
    RelativeLayout topBar;
    @BindView(R.id.selectProfile_textView)
    public TextView selectProfileTextView;
    @BindView(R.id.downArrow)
    ImageView downArrow;
    @BindView(R.id.selectedProfileCard)
    CardView selectedProfileCard;
    @BindView(R.id.profileRecycleView)
    RecyclerView profileRecycleView;
    @BindView(R.id.selectProfileLayout)
    public RelativeLayout selectProfileLayout;
    @BindView(R.id.allMembersRecyclerView)
    RecyclerView allMembersRecyclerView;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.addNewMemberLayout)
    RelativeLayout addNewMemberLayout;

    // --Commented out by Inspection (10-Aug-18 12:33 PM):private List<AllMembers> allMembersList = new ArrayList<>();

    // --Commented out by Inspection (10-Aug-18 12:33 PM):private ApiInterface apiInterface;
    private ProgressDialog progressDialog;

    private MeFamilyPresenter meFamilyPresenter;

    /*Action bar and Profile selection*/


    // --Commented out by Inspection (10-Aug-18 12:33 PM):List<FamilyListPojo> list;
    private int c = 0;
    private RecyclerAdapterAllMembers recyclerAdapterAllMembers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me_family);
        ButterKnife.bind(this);

        /*Action bar and profile selection*/
        sideMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        appTitle.setText("Me and Family");
        backToHomeMenu.setOnClickListener(view -> onBackPressed());
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        progressDialog = new ProgressDialog(MeFamilyActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        profileRecycleView.setNestedScrollingEnabled(false);
        setHeaderData(profileRecycleView);

        meFamilyPresenter = new MeFamilyPresenterImp(this, MeFamilyActivity.this);
        String userId = Preference.getString(MeFamilyActivity.this, PrefManager.USER_ID);
        meFamilyPresenter.hitAPIForData(userId);
        String name = Preference.getString(MeFamilyActivity.this, USER_NAME);
        selectProfileTextView.setText(name);//get last set profile name


        selectedProfileCard.setOnClickListener(view -> {

            selectProfileLayout.setVisibility(View.VISIBLE);
            c++;
            if (c % 2 == 0) {
                selectProfileLayout.setVisibility(View.GONE);
            } else {
                selectProfileLayout.setVisibility(View.VISIBLE);
            }

        });



        homeItem.setOnClickListener(view -> startActivity(new Intent(MeFamilyActivity.this, HomeActivity.class)));
        /**/


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        allMembersRecyclerView.setLayoutManager(layoutManager);
        allMembersRecyclerView.setItemAnimator(new DefaultItemAnimator());
        allMembersRecyclerView.setNestedScrollingEnabled(false);
        allMembersRecyclerView.setHasFixedSize(false);

        addNewMemberLayout.setOnClickListener(view -> {
            startActivity(new Intent(MeFamilyActivity.this, AddMemberActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MeFamilyActivity.this, HomeActivity.class));
        overridePendingTransition(0, 0);
        MeFamilyActivity.this.finish();
    }

    public void deleteFamilyMember(final List<FamilyListPojo> listPojo, final int position) {

        meFamilyPresenter.deleteRow(listPojo, position);


    }

    @Override
    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onFailed(String s) {
        Toast.makeText(MeFamilyActivity.this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccess(final List<FamilyListPojo> list) {

        recyclerAdapterAllMembers = new RecyclerAdapterAllMembers(MeFamilyActivity.this, list, MeFamilyActivity.this);
        allMembersRecyclerView.setAdapter(recyclerAdapterAllMembers);

    }


    @Override
    public void onDeleteSuccess(List<FamilyListPojo> listPojo, int position) {

        listPojo.remove(position);
        recyclerAdapterAllMembers.notifyItemRemoved(position);
        recyclerAdapterAllMembers.notifyItemRangeChanged(position, listPojo.size());
    }


}
