package com.webkype.healthsol.MeFamilyModule;

import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;

import java.util.List;

public interface MeFamilyPresenter {

    void hitAPIForData(String userId);

    void deleteRow(List<FamilyListPojo> listPojo, int position);
}
