package com.webkype.healthsol.MeFamilyModule;

import android.content.Context;

import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;

import java.util.List;

import io.reactivex.Observer;

public interface MeFamilyInteractor {

    void deleteRow(onGettingFamilyData listener,List<FamilyListPojo> listPojo, int position,Context context);

    interface onGettingFamilyData {

        void onFailed(String s);

        void deleteSuccess(List<FamilyListPojo> listPojo,int position);
    }

    void getFamilyData(onGettingFamilyData listener, Context context,String userId,Observer observer);

}
