package com.webkype.healthsol.LoginModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RegisterModule.SignUpActivity;
import com.webkype.healthsol.activity.ForgotPassActivity;
import com.webkype.healthsol.activity.HomeActivity;

public class SignInActivity extends AppCompatActivity implements LoginView {

    private Button signin_button;
    private TextView signUp_textView, textForgotPass;
    LoginPresenter loginPresenter;

    ProgressDialog progressDialog;

    EditText emailSignIn_editText, passwordSignIn_editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        signin_button = findViewById(R.id.signin_button);
        signUp_textView = findViewById(R.id.signUp_textView);
        textForgotPass = findViewById(R.id.textForgotPass);
        emailSignIn_editText = findViewById(R.id.emailSignIn_editText);
        passwordSignIn_editText = findViewById(R.id.passwordSignIn_editText);

        progressDialog = new ProgressDialog(SignInActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);

        loginPresenter = new LoginPresenterImp(this, new LoginInteractorImp(), SignInActivity.this);

        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginPresenter.validateCredentials(emailSignIn_editText.getText().toString(), passwordSignIn_editText.getText().toString());
                //startActivity(new Intent(SignInActivity.this, HomeActivity.class));
            }
        });

        signUp_textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        textForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SignInActivity.this, ForgotPassActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        loginPresenter.onBack();
    }

    @Override
    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void setUsernameError() {

        Toast.makeText(SignInActivity.this, "Please enter user name", Toast.LENGTH_SHORT).show();
        // emailSignIn_editText.setError("Please enter user Name");
    }

    @Override
    public void setPasswordError() {
        // passwordSignIn_editText.setError("Please enter Password");
        Toast.makeText(SignInActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void navigateToHome() {
        Toast.makeText(SignInActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

        startActivity(new Intent(SignInActivity.this, HomeActivity.class));
    }

    @Override
    public void onFailed(String error) {

        Toast.makeText(SignInActivity.this, error, Toast.LENGTH_SHORT).show();
    }

}
