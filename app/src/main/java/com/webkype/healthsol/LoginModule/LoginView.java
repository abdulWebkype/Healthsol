package com.webkype.healthsol.LoginModule;

public interface LoginView {


    void showProgress();
    void hideProgress();

    void setUsernameError();

    void setPasswordError();

    void navigateToHome();

    void onFailed(String s);

}
