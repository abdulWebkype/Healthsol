package com.webkype.healthsol.LoginModule;


import android.content.Context;
import android.widget.Toast;

import com.webkype.healthsol.RetrofitUtils.APIClient;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractorImp implements LoginInteractor {


    OnLoginFinishedListener listener;
    ApiInterface apiInterface;


    @Override
    public void login(String username, String password, final OnLoginFinishedListener listener, final Context context) {

        this.listener = listener;
        apiInterface = APIClient.getClient().create(ApiInterface.class);

        //hit API

        Call<ResponseBody> observableCall = apiInterface.loginUser(username, password);
        observableCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");

                        if (status.equals("200")) {
                            listener.onSuccess(jsonObject);
                        } else {
                            listener.onFailed(msg);
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                listener.onFailed(t.toString());
            }
        });


    }
}
