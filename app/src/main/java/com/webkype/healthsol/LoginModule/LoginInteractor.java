package com.webkype.healthsol.LoginModule;

import android.content.Context;

import org.json.JSONObject;

public interface LoginInteractor {


    interface OnLoginFinishedListener {

        void onFailed(String s);

        void onSuccess(JSONObject jsonObject);
    }

    void login(String username, String password, OnLoginFinishedListener listener, Context context);
}
