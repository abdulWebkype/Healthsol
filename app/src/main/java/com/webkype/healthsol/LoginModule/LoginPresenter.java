package com.webkype.healthsol.LoginModule;

public interface LoginPresenter {

    void validateCredentials(String username, String password);

    void onDestroy();


    void onBack();
}
