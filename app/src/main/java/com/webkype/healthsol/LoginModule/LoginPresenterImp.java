package com.webkype.healthsol.LoginModule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.webkype.healthsol.activity.LoginActivity;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import static com.webkype.healthsol.utility.PrefManager.APP_LOGO;
import static com.webkype.healthsol.utility.PrefManager.IS_LOGGED_IN;
import static com.webkype.healthsol.utility.PrefManager.USER_EMAIL;
import static com.webkype.healthsol.utility.PrefManager.USER_ID;
import static com.webkype.healthsol.utility.PrefManager.USER_IMAGE;
import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class LoginPresenterImp implements LoginPresenter, LoginInteractor.OnLoginFinishedListener {

    LoginView loginView;
    LoginInteractor loginInteractor;
    Context context;
    SharedPreferences sharedPreferences;

    public LoginPresenterImp(LoginView loginView, LoginInteractorImp loginInteractorImp, Context context) {

        this.loginView = loginView;
        this.loginInteractor = loginInteractorImp;
        this.context = context;
        sharedPreferences = context.getSharedPreferences("userLoginPref", 0);
    }

    @Override
    public void validateCredentials(String username, String password) {

        if (loginView != null) {
            loginView.showProgress();
        }

        if (username.length() != 0 && password.length() != 0) {
            loginInteractor.login(username, password, this, context);
        } else if (username.length() == 0) {
            loginView.hideProgress();
            loginView.setUsernameError();

        } else if (password.length() == 0) {
            loginView.hideProgress();
            loginView.setPasswordError();
        }

    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void onBack() {

        if (loginView != null && context instanceof SignInActivity) {
            context.startActivity(new Intent(context, LoginActivity.class));
            ((SignInActivity) context).finish();
        }
    }


    @Override
    public void onFailed(String s) {

        if (loginView != null) {
            loginView.hideProgress();
            loginView.onFailed(s);
        }

    }

    @Override
    public void onSuccess(JSONObject jsonObject) {
        if (loginView != null) {
            //save user Data
            try {
                JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                String userId = jsonObject1.getString("userId");
                String fullname = jsonObject1.getString("fullname");
                String email = jsonObject1.getString("email");
                String userLogo = jsonObject1.getString("userLogo");
                String appLogo = jsonObject1.getString("logo");

                Preference.saveString(context, USER_ID, userId);
                Preference.saveString(context, USER_NAME, fullname);
                Preference.saveString(context, USER_EMAIL, email);
                Preference.saveString(context, USER_IMAGE, userLogo);
                Preference.saveString(context, APP_LOGO, appLogo);
                Preference.saveBoolean(context, IS_LOGGED_IN, true);

                loginView.hideProgress();
                loginView.navigateToHome();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
