package com.webkype.healthsol.RetrofitUtils;


import com.webkype.healthsol.model.HomeBannerModels.BannerMainPojo;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyMainPojo;
import com.webkype.healthsol.model.NotificationListMain;
import com.webkype.healthsol.model.Notify;
import com.webkype.healthsol.model.PresDialogPojos.PresDialogMain;
import com.webkype.healthsol.model.PrescriptionRecords.PresRecordsMain;
import com.webkype.healthsol.model.ReminderMainPojo;
import com.webkype.healthsol.model.TestDialogPojos.TestRecordDMain;
import com.webkype.healthsol.model.TestReportRecords.TestRecordMainPojo;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberMain;
import com.webkype.healthsol.utility.RetrofitBodyClass;
import com.webkype.healthsol.utility.TestReportBodyClass;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ApiInterface {

    @POST("customerLogin.php")
    @FormUrlEncoded
    Call<ResponseBody> loginUser(@Field("email") String userName, @Field("password") String password);


    @POST("customerRegistration.php")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("fullName") String fullName, @Field("email") String email,
                                    @Field("password") String password, @Field("contactNo") String contactNo);

    @POST("resetPasswordByOTP.php")
    @FormUrlEncoded
    Call<ResponseBody> resetUserPass(@Field("otp") String otp, @Field("email") String email,
                                     @Field("password") String password);


    @POST("forgotPassword.php")
    @FormUrlEncoded
    Call<ResponseBody> forgotPass(@Field("email") String email);

    @POST("homePage.php")
    @FormUrlEncoded
    Call<BannerMainPojo> homePageBanner(@Field("homePage") String homePage);

    @POST("deleteMember.php")
    @FormUrlEncoded
    Call<ResponseBody> deleteMember(@Field("userId") String userId, @Field("memberId") String memberId);

    @POST("showMemberProfile.php")
    @FormUrlEncoded
    Call<ShowMemberMain> showMemberProfile(@Field("userId") String userId, @Field("memberId") String memberId);

    @POST("meAndMyFamily.php")
    @FormUrlEncoded
    Observable<FamilyMainPojo> myFamilyList(@Field("userId") String userId);

    @POST("selectFamilyMember.php")
    @FormUrlEncoded
    Call<FamilyMainPojo> getMyFamilyMember(@Field("userId") String userId);

    @Multipart
    @POST
    Call<ResponseBody> addMemeber(@Url String url, @Part MultipartBody.Part part, @PartMap HashMap<String, RequestBody> hashMap);


    @Multipart
    @POST("savePrescriptionWithDocument.php")
    Observable<ResponseBody> sendDocuments(@Part MultipartBody.Part[] part, @PartMap HashMap<String, RequestBody> hashMap);


    @Headers({"Content-Type: application/json"})
    @POST("savePrescription.php")
    Call<ResponseBody> savePrescription(@Body RetrofitBodyClass retrofitBodyClass);

    @POST("saveTestReport.php")
    Call<ResponseBody> saveReport(@Body TestReportBodyClass testReportBodyClass);

    @Multipart
    @POST("saveTestReportWithDocument.php")
    Call<ResponseBody> saveReportDocuments(@Part MultipartBody.Part[] part, @PartMap HashMap<String, RequestBody> hashMap);

    @POST("prescriptionRecords.php")
    @FormUrlEncoded
    Observable<PresRecordsMain> getPrescritionRecords(@Field("userId") String userId);

    @POST("testRecords.php")
    @FormUrlEncoded
    Observable<TestRecordMainPojo> getTestRecords(@Field("userId") String userId);

    @POST("prescriptionRecordByPresId.php")
    @FormUrlEncoded
    Observable<PresDialogMain> getPrescritionRecordsByUserId(@Field("userId") String userId, @Field("presId") String presId);

    @POST("testReportRecordByTestId.php")
    @FormUrlEncoded
    Observable<TestRecordDMain> getTestRecordsByUserId(@Field("userId") String userId, @Field("testId") String presId);

    @POST("searchPrescription.php")
    @FormUrlEncoded
    Observable<PresRecordsMain> getPresSearchRecord(@Field("userId") String userId, @Field("termName") String termName,
                                                    @Field("startDate") String startDate, @Field("endDate") String endDate);


    @POST("googleLogin.php")
    @FormUrlEncoded
    Observable<ResponseBody> checkGoogleInfo(@Field("firstName") String firstName, @Field("lastName") String lastName,
                                             @Field("email") String email);

    @POST("addReminder.php")
    @FormUrlEncoded
    Observable<ResponseBody> addNewReminder(@Field("userId") String userId, @Field("title") String title,
                                            @Field("dateTime") String dateTime, @Field("reminder") String reminder,
                                            @Field("noOfDays") String noOfDays,
                                            @Field("memberId") String memberId);

    @POST("reminderList.php")
    @FormUrlEncoded
    Observable<ReminderMainPojo> getReminderList(@Field("userId") String userId);

    @POST("deleteReminder.php")
    @FormUrlEncoded
    Observable<ResponseBody> deleteReminder(@Field("userId") String userId, @Field("reminderId") String reminderId);

    @POST("deletePrescriptionById.php")
    @FormUrlEncoded
    Observable<ResponseBody> deletePrescription(@Field("userId") String userId, @Field("presId") String presId);

    @POST("deleteTestReportById.php")
    @FormUrlEncoded
    Observable<ResponseBody> deleteTestReport(@Field("userId") String userId, @Field("testId") String testId);

    @POST("notificationList.php")
    @FormUrlEncoded
    Observable<NotificationListMain> getNotificationList(@Field("userId") String userId);

    @POST("searchTestReport.php")
    @FormUrlEncoded
    Observable<TestRecordMainPojo> searchTestReport(@Field("userId") String userId,
                                                    @Field("termName") String termName,
                                                    @Field("startDate") String startDate,
                                                    @Field("endDate") String endDate);
}
