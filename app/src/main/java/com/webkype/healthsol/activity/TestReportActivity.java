package com.webkype.healthsol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.fragment.UploadTestReportFragment;
import com.webkype.healthsol.fragment.WriteTestReportFragment;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberMain;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class TestReportActivity extends BaseActivity {

    private TextView reoprtUpload_text, writeReport_text;

    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;
    int c = 0;
    private UploadTestReportFragment fragment;
    private WriteTestReportFragment fragment1;
    private ApiInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_report);

        testReportViews();
        apiInterface = new Preference().getInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragment = new UploadTestReportFragment();
        fragmentTransaction.replace(R.id.frameLayout_report, fragment, "activites");
        fragmentTransaction.commit();


        writeReport_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragment1 = new WriteTestReportFragment();
                fragmentTransaction.replace(R.id.frameLayout_report, fragment1, "activites");
                fragmentTransaction.commit();

                writeReport_text.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
                writeReport_text.setBackgroundResource(R.drawable.ly_tab_second);

                reoprtUpload_text.setTextColor(getResources().getColor(R.color.white));
                reoprtUpload_text.setBackgroundResource(R.color.transparent);
                fragment = null;
            }
        });

        reoprtUpload_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragment = new UploadTestReportFragment();
                fragmentTransaction.replace(R.id.frameLayout_report, fragment, "activites");
                fragmentTransaction.commit();

                reoprtUpload_text.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
                reoprtUpload_text.setBackgroundResource(R.drawable.ly_tab);

                writeReport_text.setTextColor(getResources().getColor(R.color.white));
                writeReport_text.setBackgroundResource(R.color.transparent);
                fragment1 = null;

            }
        });

        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Test Report");
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        profileRecycleView.setNestedScrollingEnabled(false);
        setHeaderData(profileRecycleView);
        String name = Preference.getString(TestReportActivity.this, USER_NAME);
        selectProfile_textView.setText(name);//get last set profile name

        selectProfile_textView.setText(ProfileData.getMemberProfile(this));//get last set profile name
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if (c % 2 == 0) {
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });


        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TestReportActivity.this, HomeActivity.class));
            }
        });
        /**/
    }

    private void testReportViews() {
        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileTestReport);
        homeItem = findViewById(R.id.homeItem);

        reoprtUpload_text = findViewById(R.id.reoprtUpload_text);
        writeReport_text = findViewById(R.id.writeReport_text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(TestReportActivity.this, HomeActivity.class));
        overridePendingTransition(0, 0);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragment.onActivityResult(requestCode, resultCode, data);

    }

    public void getMemberData(String memberId) {

        showProgress();


        Call<ShowMemberMain> mainCall = apiInterface.showMemberProfile(memberId, memberId);

        mainCall.enqueue(new Callback<ShowMemberMain>() {
            @Override
            public void onResponse(Call<ShowMemberMain> call, Response<ShowMemberMain> response) {

                hideProgress();
                if (response != null) {
                    ShowMemberMain showMemberMain = response.body();
                    String status = showMemberMain.getStatus();
                    String header = showMemberMain.getHeader();
                    ShowMemberList showMemberList = showMemberMain.getMemberProfile();
                    if (showMemberList != null) {

                        if (fragment1 != null) {
                            fragment1.refreshData(showMemberList);
                        } else if (fragment != null) {
                            fragment.refreshData(showMemberList);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ShowMemberMain> call, Throwable t) {

                hideProgress();
                Toast.makeText(TestReportActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
