package com.webkype.healthsol.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.MeFamilyModule.MeFamilyActivity;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberMain;
import com.webkype.healthsol.utility.ImageFilePath;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMemberActivity extends BaseActivity {

    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;

    private int c = 0;
    private ImageView uploadProfileImage_imageView, userImageView;
    private EditText profileName, profileDateOfBirth, profileContact, profileLocation, profileEmail,
            memberBio_editText;
    private Button button_updateProfile;

    private String REQUEST_URL;

    private Spinner profileRelationSpinner;
    private Spinner bloodGroupSpinner;
    private Spinner profileStatusSpinner;
    private Spinner profileGenderSpinner;
    private ArrayList<String> maritialSatus;
    private ArrayList<String> bloodGroup;
    private ArrayList<String> RelationShip;
    private ArrayList<String> genderList;

    private Bitmap img;
    private int size;
    private int SELECT_FILE = 1;
    private File imageFile;
    CardView cardView;

    private LinearLayout relationLinear;

    private ApiInterface apiInterface;
    private boolean forEdit;

    private String selectedName, selectedRelation, selectedGender, selectedDob, selectedMobileNo,
            selectedLocation, selectedEmail, selectedBloodGroup,
            selectedStatsu, selectedBio;
    private String userId;
    private ArrayAdapter<String> relationAdapter, statusAdapter, bloodAdapter, genderAdapter;
    private CardView card1profile;

    private void addMemberViews() {

        maritialSatus = new ArrayList<>();
        bloodGroup = new ArrayList<>();
        RelationShip = new ArrayList<>();
        genderList = new ArrayList<>();

        maritialSatus.add("Single");
        maritialSatus.add("Married");

        bloodGroup.add("O-");
        bloodGroup.add("O+");
        bloodGroup.add("A-");
        bloodGroup.add("A+");
        bloodGroup.add("B-");
        bloodGroup.add("B+");
        bloodGroup.add("AB-");
        bloodGroup.add("AB+");

        RelationShip.add("Brother");
        RelationShip.add("Sister");
        RelationShip.add("Wife");
        RelationShip.add("Husband");
        RelationShip.add("Son");
        RelationShip.add("Daughter");
        RelationShip.add("Father");
        RelationShip.add("Mother");
        RelationShip.add("Uncle");
        RelationShip.add("Aunt");
        RelationShip.add("nephew");

        genderList.add("Male");
        genderList.add("Female");

        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileLayout);
        homeItem = findViewById(R.id.homeItem);
        card1profile = findViewById(R.id.card1profile);

        uploadProfileImage_imageView = findViewById(R.id.uploadProfileImage_imageView);
        userImageView = findViewById(R.id.userImageView);
        profileName = findViewById(R.id.profileName);
        profileDateOfBirth = findViewById(R.id.profileDateOfBirth);
        profileContact = findViewById(R.id.profileContact);
        profileLocation = findViewById(R.id.profileLocation);
        profileEmail = findViewById(R.id.profileEmail);
        memberBio_editText = findViewById(R.id.memberBio_editText);
        button_updateProfile = findViewById(R.id.button_updateProfile);


        profileRelationSpinner = findViewById(R.id.profileRelationSpinner);
        bloodGroupSpinner = findViewById(R.id.bloodGroupSpinner);
        profileStatusSpinner = findViewById(R.id.profileStatusSpinner);
        profileGenderSpinner = findViewById(R.id.profileGenderSpinner);

        relationLinear = findViewById(R.id.relationLinear);

        setSpinnerData();
    }

    private void setSpinnerData() {

        relationAdapter = new ArrayAdapter<>(AddMemberActivity.this, android.R.layout.simple_list_item_1, RelationShip);
        profileRelationSpinner.setAdapter(relationAdapter);

        bloodAdapter = new ArrayAdapter<>(AddMemberActivity.this, android.R.layout.simple_list_item_1, bloodGroup);
        bloodGroupSpinner.setAdapter(bloodAdapter);

        statusAdapter = new ArrayAdapter<>(AddMemberActivity.this, android.R.layout.simple_list_item_1, maritialSatus);
        profileStatusSpinner.setAdapter(statusAdapter);

        genderAdapter = new ArrayAdapter<>(AddMemberActivity.this, android.R.layout.simple_list_item_1, genderList);
        profileGenderSpinner.setAdapter(genderAdapter);

    }

    private String memberId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        addMemberViews();


        apiInterface = new Preference().getInstance();

        card1profile.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, SELECT_FILE);
        });

        profileName.setText("");
        profileDateOfBirth.setText("");
        profileContact.setText("");
        profileLocation.setText("");
        profileEmail.setText("");
        memberBio_editText.setText("");

        userId = Preference.getString(AddMemberActivity.this, PrefManager.USER_ID);

        forEdit = getIntent().getBooleanExtra("forEdit", false);
        if (forEdit) {
            memberId = getIntent().getStringExtra("memberId");
            showProgress();
            getPreviousData(memberId);
        }


        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Add Member");
        backToHomeMenu.setOnClickListener(view -> onBackPressed());
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());

        profileRecycleView.setNestedScrollingEnabled(false);

        setHeaderData(profileRecycleView);
        String name = Preference.getString(AddMemberActivity.this, PrefManager.USER_NAME);
        selectProfile_textView.setText(name);//get last set profile name
        selectedProfileCard.setOnClickListener(view -> {
            selectProfileLayout.setVisibility(View.VISIBLE);
            c++;
            if (c % 2 == 0) {
                selectProfileLayout.setVisibility(View.GONE);
            } else {
                selectProfileLayout.setVisibility(View.VISIBLE);
            }
        });
        homeItem.setOnClickListener(view -> startActivity(new Intent(AddMemberActivity.this, HomeActivity.class)));
        /**/

        profileDateOfBirth.setOnClickListener(view -> getDate());

        button_updateProfile.setOnClickListener(view -> {

            selectedDob = profileDateOfBirth.getText().toString();
            selectedName = profileName.getText().toString();

            selectedMobileNo = profileContact.getText().toString();
            selectedLocation = profileLocation.getText().toString();
            selectedEmail = profileEmail.getText().toString();
            selectedBio = memberBio_editText.getText().toString();

            if (selectedName.length() == 0) {
                Toast.makeText(AddMemberActivity.this, "Please enter Name", Toast.LENGTH_SHORT).show();
            } else if (selectedGender.length() == 0) {
                Toast.makeText(AddMemberActivity.this, "Please enter Gender", Toast.LENGTH_SHORT).show();
            } else if (selectedMobileNo.length() == 0) {
                Toast.makeText(AddMemberActivity.this, "Please enter Contact Number", Toast.LENGTH_SHORT).show();
            } else if (selectedEmail.length() == 0) {
                Toast.makeText(AddMemberActivity.this, "Please enter Email", Toast.LENGTH_SHORT).show();
            } else {
                showProgress();
                AddMemeberAPi();

            }


        });

        profileRelationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedRelation = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        bloodGroupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedBloodGroup = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        profileStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedStatsu = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        profileGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedGender = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    //convert date from dd-mm-yyyy to yyyy-mm-dd....................................................
    private void convertDate() {

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        String inputDateStr = selectedDob;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        selectedDob = outputFormat.format(date);
    }


    private void getPreviousData(String memberId) {

        Call<ShowMemberMain> mainCall = apiInterface.showMemberProfile(userId, memberId);

        mainCall.enqueue(new Callback<ShowMemberMain>() {
            @Override
            public void onResponse(Call<ShowMemberMain> call, Response<ShowMemberMain> response) {

                hideProgress();
                if (response != null) {
                    ShowMemberMain showMemberMain = response.body();
                    String status = showMemberMain.getStatus();
                    String header = showMemberMain.getHeader();
                    ShowMemberList showMemberList = showMemberMain.getMemberProfile();
                    String memberName = showMemberList.getMemberName();
                    String memberId = showMemberList.getMemberId();
                    String relation = showMemberList.getRelation();
                    String gender = showMemberList.getGender();
                    String DateOfBirth = showMemberList.getDateOfBirth();
                    String contactNo = showMemberList.getContactNo();
                    String location = showMemberList.getLocation();
                    String email = showMemberList.getEmail();
                    String bloodGroup = showMemberList.getBloodGroup();
                    String maritalStatus = showMemberList.getMaritalStatus();
                    String bio = showMemberList.getBio();
                    String memberImage = showMemberList.getMemberImage();

                    if (relation.equals("self")) {
                        relationLinear.setVisibility(View.GONE);
                        profileEmail.setFocusable(false);
                        profileEmail.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
                        profileEmail.setClickable(false);
                        findViewById(R.id.relationView).setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.relationView).setVisibility(View.VISIBLE);
                        relationLinear.setVisibility(View.VISIBLE);
                        profileEmail.setFocusable(true);
                        profileEmail.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
                        profileEmail.setClickable(true);
                    }

                    int relationIndex = RelationShip.indexOf(relation);
                    int bloodIndex = bloodGroup.indexOf(bloodGroup);
                    int statusIndex = maritialSatus.indexOf(relation);

                    profileRelationSpinner.setSelection(relationAdapter.getPosition(relation));
                    bloodGroupSpinner.setSelection(bloodAdapter.getPosition(bloodGroup));
                    profileStatusSpinner.setSelection(statusAdapter.getPosition(maritalStatus));
                    profileGenderSpinner.setSelection(genderAdapter.getPosition(gender));

                    profileName.setText(memberName);
                    profileDateOfBirth.setText(DateOfBirth);
                    profileContact.setText(contactNo);
                    profileLocation.setText(location);
                    profileEmail.setText(email);
                    memberBio_editText.setText(bio);

                    uploadProfileImage_imageView.setVisibility(View.GONE);
                    if (memberImage == null || memberImage.equals(" ")) {
                        userImageView.setImageResource(R.drawable.ic_camera_alt_black_36dp);
                    } else {
                        Glide.with(AddMemberActivity.this).load(memberImage).into(userImageView);
                    }
                }


            }

            @Override
            public void onFailure(Call<ShowMemberMain> call, Throwable t) {

                hideProgress();
                Toast.makeText(AddMemberActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void AddMemeberAPi() {

        MultipartBody.Part fileToUpload = null;
        if (imageFile != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("*image/*"), imageFile);

            fileToUpload = MultipartBody.Part.createFormData("memberImage", imageFile.getName(), requestBody);
        }

        RequestBody userIdBody = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), selectedName);
        RequestBody relationBody = RequestBody.create(MediaType.parse("text/plain"), selectedRelation);
        RequestBody genderBody = RequestBody.create(MediaType.parse("text/plain"), selectedGender);
        RequestBody dobBody = RequestBody.create(MediaType.parse("text/plain"), selectedDob);
        RequestBody mobileBody = RequestBody.create(MediaType.parse("text/plain"), selectedMobileNo);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), selectedLocation);
        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), selectedEmail);
        RequestBody bloodBody = RequestBody.create(MediaType.parse("text/plain"), selectedBloodGroup);
        RequestBody statusody = RequestBody.create(MediaType.parse("text/plain"), selectedStatsu);
        RequestBody bioBody = RequestBody.create(MediaType.parse("text/plain"), selectedBio);


        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put("userId", userIdBody);
        hashMap.put("memberName", nameBody);
        hashMap.put("relation", relationBody);
        hashMap.put("gender", genderBody);
        hashMap.put("dateOfBirth", dobBody);
        hashMap.put("contactNo", mobileBody);
        hashMap.put("location", locationBody);
        hashMap.put("memberEmail", emailBody);
        hashMap.put("bloodGroup", bloodBody);
        hashMap.put("maritalStatus", statusody);
        hashMap.put("bio", bioBody);

        if (forEdit) {
            RequestBody memberIdBody = RequestBody.create(MediaType.parse("text/plain"), memberId);
            hashMap.put("memberId", memberIdBody);
            REQUEST_URL = "editMember.php";
        } else {
            REQUEST_URL = "addMember.php";
        }

        Call<ResponseBody> bodyCall = apiInterface.addMemeber(REQUEST_URL, fileToUpload, hashMap);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideProgress();
                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String statu = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (statu.equals("200")) {

                            Toast.makeText(AddMemberActivity.this, msg, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(AddMemberActivity.this, MeFamilyActivity.class));
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        } else {
                            Toast.makeText(AddMemberActivity.this, msg, Toast.LENGTH_LONG).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(AddMemberActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void getDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddMemberActivity.this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    profileDateOfBirth.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                   // profileDateOfBirth.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                }, mYear, mMonth, mDay);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
        }
    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            Uri mFileURI = data.getData();


            final String docId;

            String realPath = ImageFilePath.getPath(AddMemberActivity.this, data.getData());
//                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            Log.i("Peofile TAG", "onActivityResult: file path : " + realPath);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mFileURI);
                // Log.d(TAG, String.valueOf(bitmap));


                uploadProfileImage_imageView.setVisibility(View.GONE);
                Glide.with(this).load(mFileURI).into(userImageView);

                if (realPath != null) {
                    imageFile = new File(realPath);

                }


            /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                docId = DocumentsContract.getDocumentId(mFileURI);

                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                String filePath = getPath(AddMemberActivity.this, contentUri);

                uploadProfileImage_imageView.setVisibility(View.GONE);
                Glide.with(this).load(mFileURI).into(userImageView);

                if (filePath != null) {
                    imageFile = new File(filePath);

                }
            } else {


                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                int columnindex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String file_path = cursor.getString(columnindex);
                String file_path1 = getRealPathFromURI(mFileURI);
                Log.d(getClass().getName(), "file_path" + file_path);
                Uri fileUri = Uri.parse("file://" + file_path);
                cursor.close();

                uploadProfileImage_imageView.setVisibility(View.GONE);
                Glide.with(this).load(mFileURI).into(userImageView);

                if (file_path1 != null) {
                    imageFile = new File(file_path1);

                }


                try {
                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    //saveImageFile(bm);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getRealPathFromURI(Uri mFileURI) {

        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(mFileURI, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;

    }




    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private String getFilename() {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "task");
        if (!file.exists()) {
            file.mkdirs();
        }
        String mUriString = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");

        Log.e("uri string", mUriString);
        return mUriString;

    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 10, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                switch (type) {
                    case "image":
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        break;
                    case "video":
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        break;
                    case "audio":
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        break;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

}
