package com.webkype.healthsol.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.webkype.healthsol.R;

public class FaqActivity extends AppCompatActivity {

    private ImageView backToHomeActionBar;
    private TextView titleActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        backToHomeActionBar = findViewById(R.id.backToHomeActionBar);
        titleActionBar = findViewById(R.id.titleActionBar);
        titleActionBar.setText("Help & FAQ");
        backToHomeActionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FaqActivity.this, HomeActivity.class));
        overridePendingTransition(0,0);
        this.finish();
    }
}
