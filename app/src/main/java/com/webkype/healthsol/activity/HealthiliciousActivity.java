package com.webkype.healthsol.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.adapter.RecyclerAdapterHealthilicious;
import com.webkype.healthsol.adapter.RecyclerAdpaterProfile;
import com.webkype.healthsol.model.HealthiliciousModel;
import com.webkype.healthsol.model.MemberProfile;
import com.webkype.healthsol.utility.ProfileData;

import java.util.ArrayList;
import java.util.List;

public class HealthiliciousActivity extends BaseActivity {

    private RecyclerView healthiliciousRecycler;
    private RecyclerAdapterHealthilicious recyclerAdapterHealthilicious;
    private List<HealthiliciousModel> healthiliciousModelList = new ArrayList<>();

    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;

    int c = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_healthilicious);
        healthActivityViews();


        healthiliciousRecycler.setHasFixedSize(false);
        healthiliciousRecycler.setFocusable(false);
        recyclerAdapterHealthilicious = new RecyclerAdapterHealthilicious(HealthiliciousActivity.this, healthiliciousModelList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        healthiliciousRecycler.setLayoutManager(layoutManager);
        healthiliciousRecycler.setItemAnimator(new DefaultItemAnimator());
        healthiliciousRecycler.setAdapter(recyclerAdapterHealthilicious);
        showData();
        recyclerAdapterHealthilicious.notifyDataSetChanged();
        healthiliciousRecycler.setNestedScrollingEnabled(false);

        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Healthilicious");
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        profileRecycleView.setNestedScrollingEnabled(false);
        setHeaderData(profileRecycleView);

        selectProfile_textView.setText(ProfileData.getMemberProfile(this));//get last set profile name
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if(c%2 == 0){
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HealthiliciousActivity.this, HomeActivity.class));
            }
        });
        /**/
    }

    private void showData(){
       // healthiliciousModelList.add(new HealthiliciousModel(R.drawable.photoicon, "Visited Nidan Mother and Child Care for Consultantion"));
       // healthiliciousModelList.add(new HealthiliciousModel(R.drawable.photoicon, "Visited Nidan Mother and Child Care for Consultantion"));
        healthiliciousModelList.add(new HealthiliciousModel(R.drawable.photoicon, "Visited Nidan Mother and Child Care for Consultantion"));
        healthiliciousModelList.add(new HealthiliciousModel(R.drawable.photoicon, "Visited Nidan Mother and Child Care for Consultantion"));
        healthiliciousModelList.add(new HealthiliciousModel(R.drawable.photoicon, "Visited Nidan Mother and Child Care for Consultantion"));
        healthiliciousModelList.add(new HealthiliciousModel(R.drawable.photoicon, "Visited Nidan Mother and Child Care for Consultantion"));

    }

    private void healthActivityViews(){
        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileLayout);
        homeItem = findViewById(R.id.homeItem);

        healthiliciousRecycler = findViewById(R.id.healthiliciousRecycler);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(HealthiliciousActivity.this, HomeActivity.class));
        overridePendingTransition(0,0);
        HealthiliciousActivity.this.finish();
    }
}
