package com.webkype.healthsol.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.RecyclerAdapterMedicDetail;
import com.webkype.healthsol.adapter.RecyclerAdapterPresVault;
import com.webkype.healthsol.model.PdfDocumentListPojo;
import com.webkype.healthsol.model.PresDialogPojos.DialogMedPres;
import com.webkype.healthsol.model.PresDialogPojos.PresDialogMain;
import com.webkype.healthsol.model.PresDialogPojos.PresDialogRecords;
import com.webkype.healthsol.model.PrescriptionRecord;
import com.webkype.healthsol.model.PrescriptionRecords.PresRecordsListPojo;
import com.webkype.healthsol.model.PrescriptionRecords.PresRecordsMain;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class PrescriptionRecordActivity extends BaseActivity {

    private RecyclerView prescriptionVaultRecycler;
    private RecyclerAdapterPresVault recyclerAdapterPresVault;
    private List<PrescriptionRecord> prescriptionRecordList = new ArrayList<>();

    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;
    int c = 0;
    ImageView calenderImg;


    private View content;
    private RelativeLayout view;
    Bitmap bitmap;


    ApiInterface apiInterface;
    private Button searchPrescriptionButton, startSearchPres_btn;
    private EditText startDatePres_editText, endDatePres_editText, presTermEdit;
    private LinearLayout searchLayoutPrescription;
    private DatePickerDialog datePickerDialog;
    private String userId;
    private Context context;
    private List<PresRecordsListPojo> listPojos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_record);
        context = PrescriptionRecordActivity.this;
        initViews();

        apiInterface = new Preference().getInstance();

        prescriptionRecordList.clear();
        prescriptionVaultRecycler.setHasFixedSize(false);
        prescriptionVaultRecycler.setFocusable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        prescriptionVaultRecycler.setLayoutManager(layoutManager);
        prescriptionVaultRecycler.setItemAnimator(new DefaultItemAnimator());

        prescriptionVaultRecycler.setNestedScrollingEnabled(false);

        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Prescription Record");
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());

        profileRecycleView.setNestedScrollingEnabled(false);
        userId = Preference.getString(PrescriptionRecordActivity.this, PrefManager.USER_ID);


        setHeaderData(profileRecycleView);
        String name = Preference.getString(PrescriptionRecordActivity.this, USER_NAME);
        selectProfile_textView.setText(name);//get last set profile name
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if (c % 2 == 0) {
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PrescriptionRecordActivity.this, HomeActivity.class));
            }
        });
        /**/


        calenderImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getStartDate();
            }
        });

        startSearchPres_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkValidations();

            }
        });

        showProgress();
        getPrescriptionRecords();
    }

    private void checkValidations() {


        String d1 = startDatePres_editText.getText().toString();
        String d2 = endDatePres_editText.getText().toString();
        String d3 = presTermEdit.getText().toString();


        if (d1.length() == 0 && d2.length() == 0 && d3.length() == 0) {
            Toast.makeText(context, "Please enter term  or select date to search", Toast.LENGTH_SHORT).show();
        } else if (d1.length() != 0 && d2.length() == 0) {
            Toast.makeText(context, "Please select End date", Toast.LENGTH_SHORT).show();
            getEndDate();
        } else if (d1.length() == 0 && d2.length() == 0 && d3.length() != 0) {

            //hit API................
            //  Toast.makeText(context, "Hit API", Toast.LENGTH_SHORT).show();
            searchRecord(d1, d2, d3);

        } else if (d1.length() != 0 && d2.length() != 0) {
            Date date1 = new Date(d1);
            Date date2 = new Date(d2);
            if (date1.after(date2)) {
                Toast.makeText(context, "End Date should be greater or equal to start date", Toast.LENGTH_SHORT).show();
                getEndDate();
            } else {

                searchRecord(d1, d2, d3);
                // Toast.makeText(context, "Hit API", Toast.LENGTH_SHORT).show();
            }

        } else {
            //......hit API..............only on term search.....................
            searchRecord(d1, d2, d3);
            // Toast.makeText(context, "Hit API", Toast.LENGTH_SHORT).show();
        }


    }

    private void searchRecord(String d1, String d2, String d3) {

        showProgress();
        //  searchLayoutPrescription.setVisibility(View.GONE);

        String startDate = "", endDate = "", searchTerm = "";
        if (d1.length() != 0 && d2.length() != 0) {
            startDate = d1;
            endDate = d2;
        }

        if (d3.length() != 0) {
            searchTerm = d3;
        }

        apiInterface.getPresSearchRecord(userId, searchTerm, startDate, endDate)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PresRecordsMain>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PresRecordsMain value) {
                        hideProgress();
                        if (value != null) {
                            String status = value.getStatus();
                            String message = value.getHeader();
                            if (status.equals("200")) {
                                listPojos = value.getPresRecord();
                                if (listPojos != null) {
                                    if (listPojos.size() == 0) {
                                        recyclerAdapterPresVault.notifyDataSetChanged();
                                        Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
                                    } else {
                                        recyclerAdapterPresVault = new RecyclerAdapterPresVault(PrescriptionRecordActivity.this, listPojos, PrescriptionRecordActivity.this);
                                        prescriptionVaultRecycler.setAdapter(recyclerAdapterPresVault);
                                    }
                                } else {
                                    Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                prescriptionVaultRecycler.setAdapter(null);
                                Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
                            }


                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideProgress();
                        Toast.makeText(PrescriptionRecordActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        hideProgress();

                    }
                });

    }

    private void getPrescriptionRecords() {


        apiInterface.getPrescritionRecords(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PresRecordsMain>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.v("onSubscribe", "onSubscribe");
                    }

                    @Override
                    public void onNext(PresRecordsMain value) {

                        hideProgress();
                        if (value != null) {
                            listPojos = value.getPresRecord();

                            recyclerAdapterPresVault = new RecyclerAdapterPresVault(PrescriptionRecordActivity.this, listPojos, PrescriptionRecordActivity.this);
                            prescriptionVaultRecycler.setAdapter(recyclerAdapterPresVault);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideProgress();
                        Toast.makeText(PrescriptionRecordActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        hideProgress();
                        Log.v("OnComplete", "onComplete");
                    }
                });

    }

    public void getStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        datePickerDialog = new DatePickerDialog(PrescriptionRecordActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        searchLayoutPrescription.setVisibility(View.VISIBLE);
                        startDatePres_editText.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);

                        getEndDate();
                    }
                }, mYear, mMonth, mDay);
        TextView title = new TextView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(16, 16, 16, 16);
        title.setLayoutParams(layoutParams);
        title.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
        title.setTextSize(18);
        Typeface typeface = Typeface.create("Bold", Typeface.BOLD);
        title.setTypeface(typeface);
        title.setPadding(32, 32, 0, 32);
        title.setText("Select Start Date");
        datePickerDialog.setCustomTitle(title);
        datePickerDialog.show();
    }

    public void getEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        datePickerDialog = new DatePickerDialog(PrescriptionRecordActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDatePres_editText.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                    }
                }, mYear, mMonth, mDay);
        TextView title = new TextView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(16, 16, 16, 16);
        title.setLayoutParams(layoutParams);
        title.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
        title.setTextSize(18);
        Typeface typeface = Typeface.create("Bold", Typeface.BOLD);
        title.setTypeface(typeface);
        title.setPadding(32, 32, 0, 32);
        title.setText("Select End Date");
        datePickerDialog.setCustomTitle(title);
        datePickerDialog.show();
    }

    private void initViews() {
        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileLayout);
        homeItem = findViewById(R.id.homeItem);

        prescriptionVaultRecycler = findViewById(R.id.prescriptionVaultRecycler);
        searchPrescriptionButton = findViewById(R.id.searchPrescriptionButton);
        startSearchPres_btn = findViewById(R.id.startSearchPres_btn);
        startDatePres_editText = findViewById(R.id.startDatePres_editText);
        endDatePres_editText = findViewById(R.id.endDatePres_editText);
        presTermEdit = findViewById(R.id.presTermEdit);
        searchLayoutPrescription = findViewById(R.id.searchLayoutPrescription);
        calenderImg = findViewById(R.id.calenderImg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PrescriptionRecordActivity.this, HomeActivity.class));
        overridePendingTransition(0, 0);
        this.finish();
    }

    public void showDetailsInDialog(String presId) {


        showProgress();
        LayoutInflater layoutInflater = LayoutInflater.from(PrescriptionRecordActivity.this);
        final View view = layoutInflater.inflate(R.layout.pres_dialog_layout, null, false);
        ScrollView scrollView = view.findViewById(R.id.scrollVIew3);
        LinearLayout adapterDetailRecordVault_linLay = view.findViewById(R.id.adapterDetailRecordVault_linLay);
        AlertDialog.Builder builder = new AlertDialog.Builder(PrescriptionRecordActivity.this);
        //  ImageView imageView = view.findViewById(R.id.bitmapImg);
        //builder.setTitle("Prescritpion Details");
        builder.setView(view);
        AlertDialog alertDialog = builder.create();

        View view2 = layoutInflater.inflate(R.layout.pres_dialog_layout, null, false);
        RelativeLayout relativeLayout = view2.findViewById(R.id.ReportRelative);

        TextView dialogDocName = view.findViewById(R.id.dialogDocName);
        TextView prescDateTxt = view.findViewById(R.id.prescDateTxt);
        TextView dialogDocSpecialization = view.findViewById(R.id.dialogDocSpecialization);
        TextView dialogDocMobile = view.findViewById(R.id.dialogDocMobile);
        TextView dialogDocAddress = view.findViewById(R.id.dialogDocAddress);
        TextView dialogDocPhone = view.findViewById(R.id.dialogDocPhone);
        TextView dialogPatName = view.findViewById(R.id.dialogPatName);
        TextView dialogPatAgeGender = view.findViewById(R.id.dialogPatAgeGender);
        TextView dialogPatComplain = view.findViewById(R.id.dialogPatComplain);
        TextView diagnosisTxt = view.findViewById(R.id.diagnosisTxt);
        TextView procedureValue = view.findViewById(R.id.procedureValue);
        TextView adviseTxt = view.findViewById(R.id.adviseTxt);
        ImageView closeDialog = view.findViewById(R.id.closeDialog);
        ImageView logoImgPres = view.findViewById(R.id.logoImgPres);
        RecyclerView medicineDetailRecycler = view.findViewById(R.id.medicineDialogRecycler);
        LinearLayout timesHeaderLinear = view.findViewById(R.id.timesHeaderLinear);
        Button downloadPdf = view.findViewById(R.id.downloadPdf);
        Button ShareFile = view.findViewById(R.id.ShareFile);
        Button showImages = view.findViewById(R.id.showImages);


        medicineDetailRecycler.setHasFixedSize(false);
        medicineDetailRecycler.setFocusable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        medicineDetailRecycler.setLayoutManager(layoutManager);
        medicineDetailRecycler.setItemAnimator(new DefaultItemAnimator());
        medicineDetailRecycler.setNestedScrollingEnabled(false);
        medicineDetailRecycler.setHasFixedSize(false);
        medicineDetailRecycler.setFocusable(false);


        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
            }
        });

        downloadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {

                showProgress();
                closeDialog.setVisibility(View.GONE);
                logoImgPres.setVisibility(View.VISIBLE);
                downloadPdf.setVisibility(View.GONE);
                ShareFile.setVisibility(View.GONE);
                CreatePDf(view, alertDialog, presId, adapterDetailRecordVault_linLay);
            }
        });


        ShareFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {

                showProgress();
                closeDialog.setVisibility(View.GONE);
                logoImgPres.setVisibility(View.VISIBLE);
                downloadPdf.setVisibility(View.GONE);
                ShareFile.setVisibility(View.GONE);
                Bitmap bitmap = createBitmapFromView(view.findViewById(R.id.ReportRelative));
                hideProgress();
                alertDialog.dismiss();
                shareBitmap(bitmap);
            }
        });

        apiInterface.getPrescritionRecordsByUserId(userId, presId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PresDialogMain>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PresDialogMain value) {

                        hideProgress();
                        if (value != null) {

                            String status = value.getStatus();
                            if (status.equals("200")) {

                                PresDialogRecords presDialogRecords = value.getPresRecord();
                                //previewBill(presDialogRecords, PrescriptionRecordActivity.this);

                                List<PdfDocumentListPojo> documentList = presDialogRecords.getDocumentList();

                                showImages.setOnClickListener(view1 -> {

                                    if (documentList != null && documentList.size() != 0) {
                                        Intent intent = new Intent(PrescriptionRecordActivity.this, MedicineImagesActivity.class);
                                        intent.putParcelableArrayListExtra("documentList", (ArrayList<? extends Parcelable>) documentList);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(PrescriptionRecordActivity.this, "No images uploaded yet", Toast.LENGTH_SHORT).show();
                                    }


                                });

                                dialogDocName.setText(presDialogRecords.getDocName());
                                dialogDocSpecialization.setText(presDialogRecords.getDocExp());
                                dialogDocMobile.setText(presDialogRecords.getDocMobile());
                                dialogDocAddress.setText(presDialogRecords.getDocAddress());
                                dialogDocPhone.setText(presDialogRecords.getDocPhone());
                                dialogPatName.setText(presDialogRecords.getPatName());
                                dialogPatAgeGender.setText(presDialogRecords.getPatAge() + "/" + presDialogRecords.getPatGender());
                                dialogPatComplain.setText(presDialogRecords.getPatComplain());
                                prescDateTxt.setText(presDialogRecords.getAdddat());


                                diagnosisTxt.setText(presDialogRecords.getDocDiagnosis());
                                adviseTxt.setText(presDialogRecords.getTestAdvised());
                                procedureValue.setText(presDialogRecords.getProceConducted());

                                List<DialogMedPres> dialogMedPres = presDialogRecords.getMedicinePres();
                                if (dialogMedPres == null) {
                                    timesHeaderLinear.setVisibility(View.GONE);
                                    medicineDetailRecycler.setVisibility(View.GONE);
                                } else {
                                    timesHeaderLinear.setVisibility(View.VISIBLE);
                                    medicineDetailRecycler.setVisibility(View.VISIBLE);

                                    RecyclerAdapterMedicDetail recyclerAdapterMedicDetail = new RecyclerAdapterMedicDetail(context, dialogMedPres);
                                    medicineDetailRecycler.setAdapter(recyclerAdapterMedicDetail);
                                }

                                alertDialog.show();
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(PrescriptionRecordActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void shareBitmap(Bitmap bitmap) {

        try {
            File file = new File(this.getExternalCacheDir(), "Report.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share image via"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CreatePDf(View content,
                           AlertDialog alertDialog,
                           String presId,
                           LinearLayout adapterDetailRecordVault_linLay) {

        String userName = Preference.getString(context, PrefManager.USER_NAME);

        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/HealthSol/" + userName + "Report" + presId + ".pdf";
        File file = new File(filePah);

        try {
            if (file.exists()) {
                file.delete();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;

                int height2 = adapterDetailRecordVault_linLay.getHeight();

                pageInfo = new PdfDocument.PageInfo.Builder(width, height2 + 400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                // int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 1200, View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

                content.measure(measureWidth, measuredHeight);
               /* content.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        1500));*/
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // CommonUtils.snackBar("Bill Generated", amountEdit, "200");
                // getActivity().finish();
            } else {
                // CommonUtils.snackBar("cant create PDF on this Device", amountEdit, "300");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        hideProgress();
        alertDialog.dismiss();
        Toast.makeText(context, "File saved to " + filePah, Toast.LENGTH_SHORT).show();

    }

    public void deletePrescription(String presId, int position) {

        progressDialog.show();

        apiInterface.deletePrescription(userId, presId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody value) {

                        if (value != null) {
                            try {
                                String res = value.string().toString();
                                JSONObject jsonObject = new JSONObject(res);
                                String status = jsonObject.getString("status");

                                if (status.equals("200")) {

                                    listPojos.remove(position);
                                    recyclerAdapterPresVault.notifyDataSetChanged();
                                }

                                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        progressDialog.dismiss();
                    }
                });
    }


    public Bitmap createBitmapFromView(RelativeLayout v) {
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return bitmap;
    }

}
