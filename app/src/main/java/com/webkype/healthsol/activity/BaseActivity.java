package com.webkype.healthsol.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.RecyclerAdpaterProfile;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyMainPojo;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseActivity extends AppCompatActivity {

    List<FamilyListPojo> listPojo;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

    }

    protected List<FamilyListPojo> setHeaderData(final RecyclerView profileRecycleView) {
        String userId = Preference.getString(BaseActivity.this, PrefManager.USER_ID);

        ApiInterface apiInterface = new Preference().getInstance();
        if (userId != null && !userId.equals(" ")) {
            Call<FamilyMainPojo> familyMainPojoCall = apiInterface.getMyFamilyMember(userId);
            familyMainPojoCall.enqueue(new Callback<FamilyMainPojo>() {
                @Override
                public void onResponse(Call<FamilyMainPojo> call, Response<FamilyMainPojo> response) {
                    if (response != null) {
                        FamilyMainPojo familyMainPojo = response.body();
                        try {
                            String status = familyMainPojo.getStatus();
                            if (status.equals("200")) {
                                listPojo = familyMainPojo.getMembers();

                                RecyclerAdpaterProfile recyclerAdpaterProfile = new RecyclerAdpaterProfile(BaseActivity.this, listPojo);
                                profileRecycleView.setAdapter(recyclerAdpaterProfile);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }


                    }
                }

                @Override
                public void onFailure(Call<FamilyMainPojo> call, Throwable t) {

                }
            });
        }

        return listPojo;

    }

    void showProgress() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
