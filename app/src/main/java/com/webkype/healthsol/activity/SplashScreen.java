package com.webkype.healthsol.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

import com.webkype.healthsol.R;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

public class SplashScreen extends AppCompatActivity {

    private PrefManager prefManager;
    boolean isLoggedIn;
    ProgressBar progressBar;
    private int progress = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        progressBar = findViewById(R.id.progressBar2);

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            // will update the "progress" propriety of seekbar until it reaches progress
            ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", progress);
            animation.setDuration(2500); // 0.5 second
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        } else {
            progressBar.setProgress(progress);
        }


        prefManager = new PrefManager(SplashScreen.this);
        isLoggedIn = Preference.getBoolean(SplashScreen.this, PrefManager.IS_LOGGED_IN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (!prefManager.isFirstTimeLaunch()) {

                    if (isLoggedIn) {
                        launchHomeScreen();
                        finish();
                    } else {
                        launchLoginScreeen();
                        finish();
                    }


                } else {
                    //check is LoggedIn

                    launchIntorActivity();
                    finish();
                }
            }
        }, 3000);
    }

    private void launchLoginScreeen() {

        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(SplashScreen.this, LoginActivity.class));
        finish();
    }

    private void launchIntorActivity() {


        startActivity(new Intent(SplashScreen.this, IntroActivity.class));
        finish();
    }

    private void launchHomeScreen() {

        startActivity(new Intent(SplashScreen.this, HomeActivity.class));
    }
}
