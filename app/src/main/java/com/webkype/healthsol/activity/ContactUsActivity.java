package com.webkype.healthsol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.webkype.healthsol.R;

public class ContactUsActivity extends AppCompatActivity {

    private TextView contactUs_textView, titleActionBar;
    private ImageView backToHomeActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        contactUs_textView = findViewById(R.id.contactUs_textView);
        backToHomeActionBar = findViewById(R.id.backToHomeActionBar);
        titleActionBar = findViewById(R.id.titleActionBar);

        titleActionBar.setText("Contact Us");

        contactUs_textView.setText("Nko \n\nPOSTAL ADDRESS :\n" + "\nNko\n" + "F-54, F Block\n" +
                "Sector - 8\n" + "Noida 201301\n" + "India" +
                "\n \n \nALL ENQUIRES :\n" + "\nTEL :  0124-4444444\n" +
                "MOB : +91-9999999999\n" + "\nEmail :  info@Nko.in");

        backToHomeActionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ContactUsActivity.this, HomeActivity.class));
        overridePendingTransition(0,0);
        this.finish();
    }
}
