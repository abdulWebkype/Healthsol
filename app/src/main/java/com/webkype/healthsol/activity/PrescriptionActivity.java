package com.webkype.healthsol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.fragment.UploadPrescpFragmnet;
import com.webkype.healthsol.fragment.WritePrescriptionFragment;
import com.webkype.healthsol.model.MemberProfile;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberMain;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class PrescriptionActivity extends BaseActivity {

    private TextView prescriptionUpload_text, writePrescription_text;

    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;
    int c = 0;
    private List<MemberProfile> memberProfileList = new ArrayList<>();
    private UploadPrescpFragmnet fragment;
    private WritePrescriptionFragment fragment1;
    String memberId;
    private ApiInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription);

        prescriptionViews();

        apiInterface = new Preference().getInstance();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragment = new UploadPrescpFragmnet();
        fragmentTransaction.replace(R.id.frameLayout_prescription, fragment, "activites");
        fragmentTransaction.commit();

        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Prescription");
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());

        profileRecycleView.setNestedScrollingEnabled(false);
        setHeaderData(profileRecycleView);
        String name = Preference.getString(PrescriptionActivity.this, USER_NAME);
        selectProfile_textView.setText(name);//get last set profile name

        memberId = ProfileData.getMemberId(PrescriptionActivity.this);
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if (c % 2 == 0) {
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PrescriptionActivity.this, HomeActivity.class));
            }
        });
        /**/


        writePrescription_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragment1 = new WritePrescriptionFragment();
                fragmentTransaction.replace(R.id.frameLayout_prescription, fragment1, "activites");
                fragmentTransaction.commit();

                writePrescription_text.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
                writePrescription_text.setBackgroundResource(R.drawable.ly_tab_second);

                prescriptionUpload_text.setTextColor(getResources().getColor(R.color.white));
                prescriptionUpload_text.setBackgroundResource(R.color.transparent);
            }
        });

        prescriptionUpload_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragment = new UploadPrescpFragmnet();
                fragmentTransaction.replace(R.id.frameLayout_prescription, fragment, "activites");
                fragmentTransaction.commit();

                prescriptionUpload_text.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
                prescriptionUpload_text.setBackgroundResource(R.drawable.ly_tab);

                writePrescription_text.setTextColor(getResources().getColor(R.color.white));
                writePrescription_text.setBackgroundResource(R.color.transparent);

            }
        });
    }

    private void prescriptionViews() {
        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileLayout);
        homeItem = findViewById(R.id.homeItem);

        prescriptionUpload_text = findViewById(R.id.prescriptionUpload_text);
        writePrescription_text = findViewById(R.id.writePrescription_text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PrescriptionActivity.this, HomeActivity.class));
        overridePendingTransition(0, 0);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragment.onActivityResult(requestCode, resultCode, data);


    }

    public void getMemberData(String memberId) {

        showProgress();
        this.memberId = memberId;

        Call<ShowMemberMain> mainCall = apiInterface.showMemberProfile(memberId, memberId);

        mainCall.enqueue(new Callback<ShowMemberMain>() {
            @Override
            public void onResponse(Call<ShowMemberMain> call, Response<ShowMemberMain> response) {

                hideProgress();
                if (response != null) {
                    ShowMemberMain showMemberMain = response.body();
                    String status = showMemberMain.getStatus();
                    String header = showMemberMain.getHeader();
                    ShowMemberList showMemberList = showMemberMain.getMemberProfile();
                    if (showMemberList != null) {

                        if (fragment1 != null) {
                            fragment1.refreshData(showMemberList);
                        } else if (fragment != null) {
                            fragment.refreshData(showMemberList);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ShowMemberMain> call, Throwable t) {

                hideProgress();
                Toast.makeText(PrescriptionActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            //Nothing
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
