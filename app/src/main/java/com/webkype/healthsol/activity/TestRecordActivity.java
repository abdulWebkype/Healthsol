package com.webkype.healthsol.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.DesTableAdapter;
import com.webkype.healthsol.adapter.RecyclerAdapterTestVault;
import com.webkype.healthsol.adapter.TestDialogAdapter;
import com.webkype.healthsol.model.TestDialogPojos.DescDetailList;
import com.webkype.healthsol.model.TestDialogPojos.TestRecord;
import com.webkype.healthsol.model.TestDialogPojos.TestRecordDMain;
import com.webkype.healthsol.model.TestDialogPojos.TestRecordDetailsList;
import com.webkype.healthsol.model.TestReportRecords.TestRecordListPojo;
import com.webkype.healthsol.model.TestReportRecords.TestRecordMainPojo;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class TestRecordActivity extends BaseActivity {

    private RecyclerView testVaultRecycler;
    private RecyclerAdapterTestVault recyclerAdapterTestVault;
    private List<com.webkype.healthsol.model.TestRecord> testRecordList = new ArrayList<>();


    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;
    private ImageView calenderImg;

    int c = 0;
    // private ImageView sortTest_calendarIV;
    private Button searchTestButton, startSearchTest_btn;
    private EditText startDateTest_editText, endDateTest_editText, termNameEdit;
    private LinearLayout searchLayoutTest;
    private DatePickerDialog datePickerDialog;

    ApiInterface apiInterface;
    private String userId;
    private Context context;
    private List<TestRecordListPojo> recordListPojos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_record);
        context = TestRecordActivity.this;
        apiInterface = new Preference().getInstance();
        userId = Preference.getString(context, PrefManager.USER_ID);
        testRecordViews();

        testRecordList.clear();
        testVaultRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(TestRecordActivity.this);
        testVaultRecycler.setLayoutManager(layoutManager);
        testVaultRecycler.setItemAnimator(new DefaultItemAnimator());


        testVaultRecycler.setItemViewCacheSize(30);
        testVaultRecycler.setDrawingCacheEnabled(true);
        testVaultRecycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Test Record");
        String name = Preference.getString(TestRecordActivity.this, USER_NAME);
        //  selectProfileTextView.setText(name);//get last set profile name
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        setHeaderData(profileRecycleView);
        profileRecycleView.setNestedScrollingEnabled(false);
        String name1 = Preference.getString(TestRecordActivity.this, USER_NAME);
        selectProfile_textView.setText(name1);//get last set profile name
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if (c % 2 == 0) {
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TestRecordActivity.this, HomeActivity.class));
            }
        });
        /**/

        searchTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLayoutTest.setVisibility(View.VISIBLE);
            }
        });

        calenderImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getStartDate();
            }
        });

        startSearchTest_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkValidations();
            }
        });

        showProgress();
        getTestReports();
    }

    private void checkValidations() {


        String d1 = startDateTest_editText.getText().toString();
        String d2 = endDateTest_editText.getText().toString();
        String d3 = termNameEdit.getText().toString();

        if (d1.length() == 0 && d2.length() == 0 && d3.length() == 0) {
            Toast.makeText(context, "Please enter term  or select date to search", Toast.LENGTH_SHORT).show();
        } else if (d1.length() != 0 && d2.length() == 0) {
            Toast.makeText(context, "Please select End date", Toast.LENGTH_SHORT).show();
        } else if (d1.length() == 0 && d2.length() == 0 && d3.length() != 0) {

            //hit API................
            // Toast.makeText(context, "Hit API", Toast.LENGTH_SHORT).show();
            searchRecord(d1, d2, d3);

        } else if (d1.length() != 0 && d2.length() != 0) {
            Date date1 = new Date(d1);
            Date date2 = new Date(d2);
            if (date1.after(date2)) {
                Toast.makeText(context, "End Date should be greater or equal to start date", Toast.LENGTH_SHORT).show();
            } else {

                searchRecord(d1, d2, d3);
                //Toast.makeText(context, "Hit API", Toast.LENGTH_SHORT).show();
            }

        } else {
            //......hit API..............only on term search.....................
            searchRecord(d1, d2, d3);
            //Toast.makeText(context, "Hit API", Toast.LENGTH_SHORT).show();
        }

    }

    private void searchRecord(String d1, String d2, String d3) {
        //searchLayoutTest.setVisibility(View.GONE);
        String startDate = "", endDate = "", searchTerm = "";
        if (d1.length() != 0 && d2.length() != 0) {
            startDate = d1;
            endDate = d2;
        } else if (d3.length() != 0) {
            searchTerm = d3;
        }

        showProgress();
        apiInterface.searchTestReport(userId, searchTerm, startDate, endDate)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TestRecordMainPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TestRecordMainPojo value) {

                        if (value != null) {
                            String status = value.getStatus();
                            String header = value.getHeader();
                            if (status.equals("200")) {

                                recordListPojos = value.getPresRecord();
                                recyclerAdapterTestVault = new RecyclerAdapterTestVault(context, recordListPojos, TestRecordActivity.this);
                                testVaultRecycler.setAdapter(recyclerAdapterTestVault);

                            } else {
                                Toast.makeText(context, header, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        hideProgress();
                    }
                });

    }

    private void getTestReports() {


        apiInterface.getTestRecords(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TestRecordMainPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TestRecordMainPojo value) {
                        hideProgress();
                        if (value != null) {
                            String status = value.getStatus();
                            String header = value.getHeader();
                            if (status.equals("200")) {

                                recordListPojos = value.getPresRecord();
                                recyclerAdapterTestVault = new RecyclerAdapterTestVault(context, recordListPojos, TestRecordActivity.this);
                                testVaultRecycler.setAdapter(recyclerAdapterTestVault);

                            } else {
                                Toast.makeText(context, header, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideProgress();
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        hideProgress();
                    }
                });
    }

    public void getStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        datePickerDialog = new DatePickerDialog(TestRecordActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        //startDateTest_editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        startDateTest_editText.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                        getEndDate();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setTitle("Select Start Date");
        datePickerDialog.show();
    }

    public void getEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        // date picker dialog
        datePickerDialog = new DatePickerDialog(TestRecordActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        searchLayoutTest.setVisibility(View.VISIBLE);
                        // endDateTest_editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        endDateTest_editText.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setTitle("Select End Date");
        datePickerDialog.show();
    }

    private void testRecordViews() {
        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileTest);
        homeItem = findViewById(R.id.homeItem);
        calenderImg = findViewById(R.id.calenderImg);

        testVaultRecycler = findViewById(R.id.testVaultRecycler);
        searchTestButton = findViewById(R.id.searchTestButton);
        startSearchTest_btn = findViewById(R.id.startSearchTest_btn);
        startDateTest_editText = findViewById(R.id.startDateTest_editText);
        endDateTest_editText = findViewById(R.id.endDateTest_editText);
        termNameEdit = findViewById(R.id.termNameEdit);
        searchLayoutTest = findViewById(R.id.searchLayoutTest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(TestRecordActivity.this, HomeActivity.class));
        overridePendingTransition(0, 0);
        this.finish();
    }


    public void showTestDetails(String testId) {

        showProgress();

        LayoutInflater layoutInflater = LayoutInflater.from(TestRecordActivity.this);
        View view = layoutInflater.inflate(R.layout.test_dialog_layout, null, false);
        LinearLayout testReportLinearLayout = view.findViewById(R.id.testReportLinearLayout);
        AlertDialog.Builder builder = new AlertDialog.Builder(TestRecordActivity.this);
        //builder.setTitle("Prescritpion Details");
        builder.setView(view);
        AlertDialog alertDialog = builder.create();

        TextView dialogPatName = view.findViewById(R.id.dialogPatName);
        TextView dialogLabName = view.findViewById(R.id.dialogLabName);
        TextView dialogPresBy = view.findViewById(R.id.dialogPresBy);
        TextView dialogSampleType = view.findViewById(R.id.dialogSampleType);
        TextView dialogTestDate = view.findViewById(R.id.dialogTestDate);
        TextView dialogPatRemark = view.findViewById(R.id.dialogPatRemark);


        ImageView closeTestDialog = view.findViewById(R.id.closeTestDialog);
        ImageView logoImgTest = view.findViewById(R.id.logoImgTest);
        RecyclerView testDialogRecycler = view.findViewById(R.id.testDialogRecycler);
        RecyclerView descrTableRecycler = view.findViewById(R.id.descrTableRecycler);
        LinearLayout dialogHeaderLinear = view.findViewById(R.id.dialogHeaderLinear);
        LinearLayout dialogHeaderLinear2 = view.findViewById(R.id.dialogHeaderLinear2);
        Button downloadTestPdf = view.findViewById(R.id.downloadTestPdf);
        Button shareTestPdf = view.findViewById(R.id.shareTestPdf);


        testDialogRecycler.setHasFixedSize(false);
        testDialogRecycler.setFocusable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        testDialogRecycler.setLayoutManager(layoutManager);
        testDialogRecycler.setItemAnimator(new DefaultItemAnimator());
        testDialogRecycler.setNestedScrollingEnabled(false);


        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        descrTableRecycler.setLayoutManager(layoutManager1);
        descrTableRecycler.setItemAnimator(new DefaultItemAnimator());
        descrTableRecycler.setNestedScrollingEnabled(false);


        closeTestDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
            }
        });

        downloadTestPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {

                showProgress();
                closeTestDialog.setVisibility(View.GONE);
                logoImgTest.setVisibility(View.VISIBLE);
                shareTestPdf.setVisibility(View.GONE);
                downloadTestPdf.setVisibility(View.GONE);
                CreatePDf(view, alertDialog, testId, testReportLinearLayout);
            }
        });


        shareTestPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {

                showProgress();
                closeTestDialog.setVisibility(View.GONE);
                logoImgTest.setVisibility(View.VISIBLE);
                shareTestPdf.setVisibility(View.GONE);
                downloadTestPdf.setVisibility(View.GONE);
                Bitmap bitmap = createBitmapFromView(view.findViewById(R.id.testReportRelative));

                alertDialog.dismiss();
                shareBitmap(bitmap);
            }
        });

        apiInterface.getTestRecordsByUserId(userId, testId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TestRecordDMain>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TestRecordDMain value) {

                        hideProgress();
                        if (value != null) {
                            String status = value.getStatus();
                            if (status.equals("200")) {
                                TestRecord presDialogRecords = value.getTestRecord();
                                dialogPatName.setText(presDialogRecords.getPatientName());
                                dialogLabName.setText(presDialogRecords.getLabName());
                                dialogPresBy.setText(presDialogRecords.getPrescrdBy());
                                dialogSampleType.setText(presDialogRecords.getSampleType());
                                dialogTestDate.setText(presDialogRecords.getTestDate());
                                dialogPatRemark.setText(presDialogRecords.getRemarks());


                                List<TestRecordDetailsList> dialogMedPres = presDialogRecords.getTestDetList();
                                if (dialogMedPres == null) {
                                    dialogHeaderLinear2.setVisibility(View.GONE);
                                    testDialogRecycler.setVisibility(View.GONE);
                                } else {
                                    dialogHeaderLinear2.setVisibility(View.VISIBLE);
                                    testDialogRecycler.setVisibility(View.VISIBLE);

                                    TestDialogAdapter recyclerAdapterMedicDetail = new TestDialogAdapter(context, dialogMedPres);
                                    testDialogRecycler.setAdapter(recyclerAdapterMedicDetail);
                                }

                                List<DescDetailList> descDetailLists = presDialogRecords.getDescpDetList();

                                if (descDetailLists == null) {
                                    dialogHeaderLinear.setVisibility(View.GONE);
                                    descrTableRecycler.setVisibility(View.GONE);
                                } else {
                                    dialogHeaderLinear.setVisibility(View.VISIBLE);
                                    descrTableRecycler.setVisibility(View.VISIBLE);

                                    DesTableAdapter recyclerAdapterMedicDetail = new DesTableAdapter(context, descDetailLists);
                                    descrTableRecycler.setAdapter(recyclerAdapterMedicDetail);
                                }


                                alertDialog.show();
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void CreatePDf(View content, AlertDialog alertDialog, String testId, LinearLayout testReportLinearLayout) {

        String userName = Preference.getString(context, PrefManager.USER_NAME);

        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/HealthSol/" + userName + "Report" + testId + ".pdf";
        File file = new File(filePah);

        try {
            if (file.exists()) {
                file.delete();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                int height2 = testReportLinearLayout.getHeight();

                pageInfo = new PdfDocument.PageInfo.Builder(width, height2 + 600, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 400, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // CommonUtils.snackBar("Bill Generated", amountEdit, "200");
                // getActivity().finish();
            } else {
                // CommonUtils.snackBar("cant create PDF on this Device", amountEdit, "300");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        hideProgress();
        alertDialog.dismiss();
        Toast.makeText(context, "File saved to " + filePah, Toast.LENGTH_SHORT).show();


    }

    public void removeItem(String testId, int position, RecyclerAdapterTestVault recyclerAdapterTestVault) {


        showProgress();

        apiInterface.deleteTestReport(userId, testId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody value) {
                        if (value != null) {
                            try {
                                String res = value.string().toString();
                                JSONObject jsonObject = new JSONObject(res);
                                String status = jsonObject.getString("status");

                                if (status.equals("200")) {


                                    recordListPojos.remove(position);
                                    testVaultRecycler.getAdapter().notifyDataSetChanged();
                                }

                                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                        hideProgress();
                    }
                });
    }

    public Bitmap createBitmapFromView(RelativeLayout v) {
        v.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        v.measure(View.MeasureSpec.makeMeasureSpec(768, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
      //  v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return bitmap;
    }


    private void shareBitmap(Bitmap bitmap) {

        try {
            File file = new File(this.getExternalCacheDir(), "TestReport.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share image via"));
            hideProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
