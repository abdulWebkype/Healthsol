package com.webkype.healthsol.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyListPojo;
import com.webkype.healthsol.model.MeandFamilyModel.FamilyMainPojo;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewReminder extends AppCompatActivity {


    private LinearLayout timeLayout, dateLayout;
    private TextView selectedTime, selectedDate;
    private String memberId;

    private int mYear, mMonth, mDay, mHour, mMinute;

    private EditText titileEdit, reminderEdit;

    private Button submitReminder;

    private String requiredDate, requiredTime;

    private Spinner whomSpinner;

    private ArrayList<String> memberLists;

    private ProgressDialog progressDialog;
    private String userId;

    private ImageView reminderBack;
    private NumberPicker numberPikcer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_reminder);

        userId = Preference.getString(AddNewReminder.this, PrefManager.USER_ID);

        timeLayout = findViewById(R.id.timeLayout);
        dateLayout = findViewById(R.id.dateLayout);
        selectedTime = findViewById(R.id.selectedTime);
        selectedDate = findViewById(R.id.selectedDate);
        titileEdit = findViewById(R.id.titileEdit);
        reminderEdit = findViewById(R.id.reminderEdit);
        submitReminder = findViewById(R.id.submitReminder);
        whomSpinner = findViewById(R.id.whomSpinner);
        reminderBack = findViewById(R.id.reminderBack);
        numberPikcer = findViewById(R.id.numberPikcer);

        numberPikcer.setMinValue(1);
        numberPikcer.setMaxValue(7);


        memberLists = new ArrayList<>();

        progressDialog = new ProgressDialog(AddNewReminder.this);
        progressDialog.setMessage("Loading...");


        setSpinnerData();

        timeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                setCurrentTime();
            }
        });

        dateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setCurrentDate();
            }
        });

        reminderBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                gotoReminderList();
            }
        });

        submitReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //call submit API..........................
                if (titileEdit.length() == 0) {
                    Toast.makeText(AddNewReminder.this, "Please enter Title", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (selectedTime.getText().equals("Select Time")) {
                    Toast.makeText(AddNewReminder.this, "Please Select Time", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selectedDate.getText().equals("Select Date")) {
                    Toast.makeText(AddNewReminder.this, "Please Select Date", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (reminderEdit.length() == 0) {
                    Toast.makeText(AddNewReminder.this, "Please enter Reminder", Toast.LENGTH_SHORT).show();
                    return;
                }


                submitNewReminder();
            }
        });
    }

    ApiInterface apiInterface = new Preference().getInstance();

    private void submitNewReminder() {

        progressDialog.show();
        String title = titileEdit.getText().toString();
        String reminder = reminderEdit.getText().toString();
        String dateTime = requiredDate + " " + requiredTime;
        String noOfDays = String.valueOf(numberPikcer.getValue());

        apiInterface.addNewReminder(userId, title, dateTime, reminder,noOfDays,memberId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBody value) {

                        progressDialog.dismiss();
                        try {
                            String res = value.string().toString();
                            JSONObject jsonObject = new JSONObject(res);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("msg");
                            if (status.equals("200")) {

                                Toast.makeText(AddNewReminder.this, message, Toast.LENGTH_SHORT).show();
                                gotoReminderList();
                            } else {
                                Toast.makeText(AddNewReminder.this, message, Toast.LENGTH_SHORT).show();

                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        progressDialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void gotoReminderList() {

        Intent intent = new Intent(AddNewReminder.this, NotificationActivity.class);
        startActivity(intent);
    }

    private List<FamilyListPojo> familyListPojos;

    private void setSpinnerData() {
        progressDialog.show();


        if (userId != null && !userId.equals(" ")) {
            Call<FamilyMainPojo> familyMainPojoCall = apiInterface.getMyFamilyMember(userId);
            familyMainPojoCall.enqueue(new Callback<FamilyMainPojo>() {
                @Override
                public void onResponse(Call<FamilyMainPojo> call, Response<FamilyMainPojo> response) {
                    progressDialog.dismiss();
                    if (response != null) {
                        FamilyMainPojo familyMainPojo = response.body();
                        try {
                            String status = familyMainPojo.getStatus();
                            if (status.equals("200")) {

                                familyListPojos = familyMainPojo.getMembers();
                                for (FamilyListPojo familyListPojo : familyListPojos) {
                                    memberLists.add(familyListPojo.getName());
                                }


                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(AddNewReminder.this, android.R.layout.simple_list_item_1, memberLists);
                        whomSpinner.setAdapter(arrayAdapter);

                        whomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                                memberId = familyListPojos.get(i).getMemberId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });


                    }
                }

                @Override
                public void onFailure(Call<FamilyMainPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(AddNewReminder.this, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setCurrentDate() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        selectedDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        requiredDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void setCurrentTime() {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        selectedTime.setText(hourOfDay + ":" + minute);
                        requiredTime = hourOfDay + ":" + minute;

                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
