package com.webkype.healthsol.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.adapter.RecyclerAdpaterProfile;
import com.webkype.healthsol.fragment.NotificationFragment;
import com.webkype.healthsol.fragment.ReminderFragment;
import com.webkype.healthsol.model.MemberProfile;
import com.webkype.healthsol.utility.ProfileData;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseActivity {

    private TextView reminderOpen_text, notificationOpen_text;

    /*Action bar and Profile selection*/
    private ImageView icon_slidingMenu, backToHomeMenu;
    private TextView title_actionBar, homeItem;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    public RelativeLayout selectProfileLayout;
    private RecyclerView profileRecycleView;

    int c = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

       notificationViews();

        /*Action bar and profile selection*/
        icon_slidingMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        title_actionBar.setText("Reminders & Notifications");
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        profileRecycleView.setNestedScrollingEnabled(false);

        setHeaderData(profileRecycleView);
        selectProfile_textView.setText(ProfileData.getMemberProfile(this));//get last set profile name
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if(c%2 == 0){
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NotificationActivity.this, HomeActivity.class));
            }
        });
        /**/

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        ReminderFragment fragment =new ReminderFragment();
        fragmentTransaction.replace(R.id.frameLayout_notifications, fragment,"activites");
        fragmentTransaction.commit();


        notificationOpen_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                NotificationFragment fragment =new NotificationFragment();
                fragmentTransaction.replace(R.id.frameLayout_notifications, fragment,"activites");
                fragmentTransaction.commit();

                notificationOpen_text.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
                notificationOpen_text.setBackgroundResource(R.drawable.ly_tab_second);

                reminderOpen_text.setTextColor(getResources().getColor(R.color.white));
                reminderOpen_text.setBackgroundResource(R.color.transparent);
            }
        });

        reminderOpen_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                ReminderFragment fragment =new ReminderFragment();
                fragmentTransaction.replace(R.id.frameLayout_notifications, fragment,"activites");
                fragmentTransaction.commit();

                reminderOpen_text.setTextColor(getResources().getColor(R.color.colorPrimary_sp));
                reminderOpen_text.setBackgroundResource(R.drawable.ly_tab);

                notificationOpen_text.setTextColor(getResources().getColor(R.color.white));
                notificationOpen_text.setBackgroundResource(R.color.transparent);

            }
        });

    }
    private void notificationViews(){
        icon_slidingMenu = findViewById(R.id.sideMenu);
        backToHomeMenu = findViewById(R.id.backToHomeMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        selectProfileLayout = findViewById(R.id.selectProfileLayout);
        homeItem = findViewById(R.id.homeItem);

        reminderOpen_text = findViewById(R.id.reminderOpen_text);
        notificationOpen_text= findViewById(R.id.notificationOpen_text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(NotificationActivity.this, HomeActivity.class));
        overridePendingTransition(0,0);
        this.finish();
    }
}
