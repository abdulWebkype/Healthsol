package com.webkype.healthsol.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.rd.PageIndicatorView;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.adapter.MainPagerAdapter;
import com.webkype.healthsol.adapter.RecyclerAdapterTabsHome;
import com.webkype.healthsol.fragment.SideMenuFragment;
import com.webkype.healthsol.model.HomeBannerModels.BannerMainPojo;
import com.webkype.healthsol.model.HomeBannerModels.BannerThoughtPojo;
import com.webkype.healthsol.model.HomeBannerModels.BannersList;
import com.webkype.healthsol.model.SliderModel;
import com.webkype.healthsol.model.TabsHome;
import com.webkype.healthsol.utility.Preference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class HomeActivity extends BaseActivity {

    private ImageView icon_slidingMenu;
    private TextView title_actionBar;
    private SlidingMenu menu;
    private CardView selectedProfileCard;
    public TextView selectProfile_textView;
    private RecyclerView profileRecycleView;

    private int c = 0;
    private ApiInterface apiInterface;
    public RelativeLayout selectProfileLayout;
    private RelativeLayout homeLayout;

    private TextView dailyThought;
    private TextView publishBy;
    private RecyclerView tabHomeRecycleView;
    private RecyclerAdapterTabsHome recyclerAdapterTabsHome;
    private List<TabsHome> tabsHomeList = new ArrayList<>();

    /*View Pager*/
    private ViewPager viewPagerHome;
    private PageIndicatorView pageIndicatorView;
    private MainPagerAdapter pagerAdapter = null;
    private List<SliderModel> sliderModelList = new ArrayList<>();
    private int currentPage = 0;
    private int page = 0;
    private ProgressBar progressBarPager;
    private Timer timer;
    private final long DELAY_MS = 2000;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.
    /**/

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 401;
    private List<BannersList> bannersLists;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        homeActivityViews();
        apiInterface = new Preference().getInstance();
        if (checkAndRequestPermissions()) {

        }

        /*Action Bar*/
        setSideBar();
        SideMenuFragment sideMenuFragment = new SideMenuFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.side_menu_container, sideMenuFragment, "SideMenuFragment")
                .commit();

        homeLayout.setVisibility(View.GONE);
        icon_slidingMenu.setOnClickListener(view -> menu.toggle());
        /**/

        /*View Pager With Indicator*/


        showsliderdata();
        viewPagerHome.setPageTransformer(false, (page, position) -> {
            final float normalizedposition = Math.abs(Math.abs(position) - 1);
            page.setAlpha(normalizedposition);
        });

        final Handler handler = new Handler();
        final Runnable Update = () -> {
            if (currentPage == sliderModelList.size()) {
                currentPage = 0;
            }
            viewPagerHome.setCurrentItem(currentPage++, true);
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.postDelayed(Update, 3000);
            }
        }, DELAY_MS, PERIOD_MS);

        viewPagerHome.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {/*empty*/}

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {/*empty*/}
        });
        /**/

        selectedProfileCard.setOnClickListener(view -> {
            selectProfileLayout.setVisibility(View.VISIBLE);
            c++;
            if (c % 2 == 0) {
                selectProfileLayout.setVisibility(View.GONE);
            } else {
                selectProfileLayout.setVisibility(View.VISIBLE);
            }
        });

        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        profileRecycleView.setNestedScrollingEnabled(false);

        setHeaderData(profileRecycleView);
        String name = Preference.getString(HomeActivity.this, USER_NAME);
        selectProfile_textView.setText(name);//get last set profile name
        /**/

        /*HOME TABS*/
        tabHomeRecycleView.setHasFixedSize(true);
        tabHomeRecycleView.setFocusable(false);
        recyclerAdapterTabsHome = new RecyclerAdapterTabsHome(HomeActivity.this, tabsHomeList);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        tabHomeRecycleView.setLayoutManager(gridLayoutManager);
        /*RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(HomeActivity.this, 3);
        tabHomeRecycleView.setLayoutManager(mLayoutManager);
        tabHomeRecycleView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(3), true));*/
        tabHomeRecycleView.setItemAnimator(new DefaultItemAnimator());
        tabHomeRecycleView.setAdapter(recyclerAdapterTabsHome);
        showTabsData();
        recyclerAdapterTabsHome.notifyDataSetChanged();
        tabHomeRecycleView.setNestedScrollingEnabled(false);
        /**/
    }


    private boolean checkAndRequestPermissions() {
        int externalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (externalStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void showTabsData() {
        tabsHomeList.add(new TabsHome(R.drawable.img1, "Save Prescription", "1"));
        tabsHomeList.add(new TabsHome(R.drawable.img2, "Save Test Report", "2"));
        tabsHomeList.add(new TabsHome(R.drawable.me_family, "Me & Family", "6"));
        tabsHomeList.add(new TabsHome(R.drawable.img5, "Prescription Record", "4"));
        tabsHomeList.add(new TabsHome(R.drawable.img4, "Test Report Record", "5"));
        tabsHomeList.add(new TabsHome(R.drawable.img3, "Notify Me", "3"));
    }

    private void showsliderdata() {


        Call<BannerMainPojo> mainPojoCall = apiInterface.homePageBanner("homePage");

        mainPojoCall.enqueue(new Callback<BannerMainPojo>() {
            @Override
            public void onResponse(Call<BannerMainPojo> call, Response<BannerMainPojo> response) {
                if (response != null) {
                    progressBarPager.setVisibility(View.GONE);
                    try {
                        BannerMainPojo bannerMainPojo = response.body();
                        assert bannerMainPojo != null;
                        String status = bannerMainPojo.getStatus();
                        String header = bannerMainPojo.getHeader();

                        BannerThoughtPojo bannerThoughtPojo = bannerMainPojo.getThought();
                        dailyThought.setText(bannerThoughtPojo.getThought());
                        publishBy.setText(bannerThoughtPojo.getPublishBy());

                        bannersLists = bannerMainPojo.getBanners();
                        pageIndicatorView.setCount(bannersLists.size()); // specify total count of indicators
                        pageIndicatorView.setSelection(1);
                        pagerAdapter = new MainPagerAdapter(HomeActivity.this, bannersLists);
                        viewPagerHome.setAdapter(pagerAdapter);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<BannerMainPojo> call, Throwable t) {
                progressBarPager.setVisibility(View.GONE);
                Toast.makeText(HomeActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setSideBar() {
        menu = new SlidingMenu(this);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setFadeDegree(0.75f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.ly_frame_layout);
    }

    private void homeActivityViews() {
        icon_slidingMenu = findViewById(R.id.sideMenu);
        title_actionBar = findViewById(R.id.appTitle);
        selectedProfileCard = findViewById(R.id.selectedProfileCard);
        profileRecycleView = findViewById(R.id.profileRecycleView);
        viewPagerHome = findViewById(R.id.viewPagerHome);
        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        selectProfile_textView = findViewById(R.id.selectProfile_textView);
        homeLayout = findViewById(R.id.homeLayout);

        dailyThought = findViewById(R.id.dailyThought);
        publishBy = findViewById(R.id.publishBy);
        selectProfileLayout = findViewById(R.id.selectProfileLayout);
        tabHomeRecycleView = findViewById(R.id.tabHomeRecycleView);
        progressBarPager = findViewById(R.id.progressBarPager);
        progressBarPager.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }
}
