package com.webkype.healthsol.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassActivity extends AppCompatActivity {


    EditText editText;
    Button submitEmail;
    ApiInterface apiInterface;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);


        progressDialog = new ProgressDialog(ForgotPassActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);

        editText = findViewById(R.id.forgotEdit);
        submitEmail = findViewById(R.id.submit_button);


        submitEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitEmail.length() == 0) {
                    Toast.makeText(ForgotPassActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog.show();
                    submitUserEmail(editText.getText().toString());
                }
            }
        });
    }

    private void submitUserEmail(String s) {

        apiInterface = new Preference().getInstance();

        Call<ResponseBody> bodyCall = apiInterface.forgotPass(s);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                if (response != null) {
                    String res = null;
                    try {
                        res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (status.equals("200")) {
                            Toast.makeText(ForgotPassActivity.this, msg, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ForgotPassActivity.this, ResetPassViaOTP.class));
                        } else {
                            Toast.makeText(ForgotPassActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ForgotPassActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }
}
