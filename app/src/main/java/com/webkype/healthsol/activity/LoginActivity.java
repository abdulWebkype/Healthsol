package com.webkype.healthsol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.webkype.healthsol.LoginModule.LoginPresenter;
import com.webkype.healthsol.LoginModule.SignInActivity;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RegisterModule.SignUpActivity;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.webkype.healthsol.utility.PrefManager.IS_LOGGED_IN;
import static com.webkype.healthsol.utility.PrefManager.USER_ID;
import static com.webkype.healthsol.utility.PrefManager.USER_IMAGE;
import static com.webkype.healthsol.utility.PrefManager.USER_NAME;

public class LoginActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    private RelativeLayout customSignInLayout;
    private TextView signUp_textView;

    ApiInterface apiInterface;


    /*Google signIn*/
    private RelativeLayout googleSignInLayout;
    private GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 9001;
    String name, email, img_url;

    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginActivityViews();


        apiInterface = new Preference().getInstance();

        customSignInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignInActivity.class));
            }
        });

        signUp_textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        googleSignInLayout.setOnClickListener(this);
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.googleSignInLayout:
                signIn();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    public void logout() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
            }
        });
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            name = account.getDisplayName();
            String dName = account.getDisplayName();
            String gName = account.getGivenName();
            email = account.getEmail();


            String userId = account.getId();
            if (account.getPhotoUrl() != null) {
                img_url = account.getPhotoUrl().toString();
            }

            //Hit Google Login API Here ...................

            checkGoogleLoginAPI(name, email, img_url);


        } else {

            Toast.makeText(this, "Login Failed,Please try again", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkGoogleLoginAPI(String name, String email, String img_url) {

        String arr[] = name.split(" ");
        String fName = arr[0];
        String lName = arr[1];
        apiInterface.checkGoogleInfo(fName, lName, email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {


                    }

                    @Override
                    public void onNext(ResponseBody value) {

                        if (value != null) {
                            try {
                                String res = value.string().toString();
                                JSONObject jsonObject = new JSONObject(res);
                                String status = jsonObject.getString("status");
                                if (status.equals("200")) {

                                    String msg = jsonObject.getString("msg");
                                    Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                                    JSONObject userDetails = jsonObject.getJSONObject("user");
                                    String userId = userDetails.getString("id");
                                    String firstName = userDetails.getString("firstName");
                                    String lastName = userDetails.getString("lastName");
                                    String email = userDetails.getString("email");
                                    String userImge = userDetails.getString("userLogo");

                                    Preference.saveString(LoginActivity.this, USER_ID, userId);
                                    Preference.saveString(LoginActivity.this, USER_NAME, name);
                                    Preference.saveString(LoginActivity.this, PrefManager.USER_EMAIL, email);
                                    if (userImge != null && !userImge.equals("")) {
                                        Preference.saveString(LoginActivity.this, USER_IMAGE, userImge);

                                    } else {
                                        Preference.saveString(LoginActivity.this, USER_IMAGE, img_url);

                                    }
                                    // Preference.saveString(LoginActivity.this, APP_LOGO, appLogo);
                                    Preference.saveBoolean(LoginActivity.this, IS_LOGGED_IN, true);

                                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));

                                } else {
                                    Toast.makeText(LoginActivity.this, "Unable to login,Please try again", Toast.LENGTH_SHORT).show();
                                }


                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }

    private void loginActivityViews() {
        googleSignInLayout = findViewById(R.id.googleSignInLayout);
        customSignInLayout = findViewById(R.id.customSignInLayout);
        signUp_textView = findViewById(R.id.signUp_textView);
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }


}
