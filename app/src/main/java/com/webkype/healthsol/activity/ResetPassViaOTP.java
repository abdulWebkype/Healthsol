package com.webkype.healthsol.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.utility.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassViaOTP extends AppCompatActivity {

    EditText PassOTPEdit, PassPasswordEdit, PassCPassEdit, PassEmailEdit;
    Button reset_button;
    ApiInterface apiInterface;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass_via_otp);

        progressDialog = new ProgressDialog(ResetPassViaOTP.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);

        PassEmailEdit = findViewById(R.id.PassEmailEdit);
        PassOTPEdit = findViewById(R.id.PassOTPEdit);
        PassPasswordEdit = findViewById(R.id.PassPasswordEdit);
        PassCPassEdit = findViewById(R.id.PassCPassEdit);
        reset_button = findViewById(R.id.reset_button);

        reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = PassEmailEdit.getText().toString();
                String otp = PassOTPEdit.getText().toString();
                String pass = PassPasswordEdit.getText().toString();
                String cPass = PassCPassEdit.getText().toString();
                if (email.length() == 0) {
                    Toast.makeText(ResetPassViaOTP.this, "Please enter your email", Toast.LENGTH_SHORT).show();
                } else if (otp.length() == 0) {
                    Toast.makeText(ResetPassViaOTP.this, "Please enter OTP", Toast.LENGTH_SHORT).show();
                } else if (pass.length() == 0) {
                    Toast.makeText(ResetPassViaOTP.this, "Please enter new password", Toast.LENGTH_SHORT).show();
                } else if (cPass.length() == 0) {
                    Toast.makeText(ResetPassViaOTP.this, "Please confirm password", Toast.LENGTH_SHORT).show();
                } else if (!pass.equals(cPass)) {
                    Toast.makeText(ResetPassViaOTP.this, "Password and confirm password should be same", Toast.LENGTH_SHORT).show();

                } else {
                    progressDialog.show();
                    resetUserPassword(otp, email, pass);
                }

            }
        });
    }

    private void resetUserPassword(String otp, String email, String pass) {
        apiInterface = new Preference().getInstance();

        Call<ResponseBody> responseBodyCall = apiInterface.resetUserPass(otp, email, pass);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressDialog.dismiss();
                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String msg = jsonObject.getString("msg");
                        if (status.equals("200")) {
                            Toast.makeText(ResetPassViaOTP.this, msg, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ResetPassViaOTP.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        } else {
                            Toast.makeText(ResetPassViaOTP.this, msg, Toast.LENGTH_SHORT).show();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(ResetPassViaOTP.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
