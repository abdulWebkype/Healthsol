package com.webkype.healthsol.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.MyProfileModule.MyProfileInteractorImp;
import com.webkype.healthsol.MyProfileModule.MyProfilePresenter;
import com.webkype.healthsol.MyProfileModule.MyProfilePresenterImp;
import com.webkype.healthsol.MyProfileModule.MyProfileView;
import com.webkype.healthsol.R;
import com.webkype.healthsol.RetrofitUtils.ApiInterface;
import com.webkype.healthsol.model.showMemeberModels.ShowMemberList;
import com.webkype.healthsol.utility.ImageFilePath;
import com.webkype.healthsol.utility.PrefManager;
import com.webkype.healthsol.utility.Preference;
import com.webkype.healthsol.utility.ProfileData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.webkype.healthsol.utility.PrefManager.USER_ID;

public class ProfileActivity extends BaseActivity implements MyProfileView {

    private static final int SELECT_FILE = 266;
    @BindView(R.id.sideMenu)
    ImageView sideMenu;
    @BindView(R.id.backToHomeMenu)
    ImageView backToHomeMenu;
    @BindView(R.id.actionLAy)
    RelativeLayout actionLAy;
    @BindView(R.id.appTitle)
    TextView appTitle;
    @BindView(R.id.homeItem)
    TextView homeItem;
    @BindView(R.id.homeLayout)
    RelativeLayout homeLayout;
    @BindView(R.id.topBar)
    RelativeLayout topBar;
    @BindView(R.id.selectProfile_textView)
    public TextView selectProfileTextView;
    @BindView(R.id.downArrow)
    ImageView downArrow;
    @BindView(R.id.selectedProfileCard)
    CardView selectedProfileCard;
    @BindView(R.id.profileRecycleView)
    RecyclerView profileRecycleView;
    @BindView(R.id.selectProfileLayout)
    public RelativeLayout selectProfileLayout;
    @BindView(R.id.viewProfile_view)
    View viewProfileView;
    @BindView(R.id.userImageView)
    ImageView userImageView;
    @BindView(R.id.card1profile)
    CardView card1profile;
    @BindView(R.id.uploadProfileImage_imageView)
    ImageView uploadProfileImageImageView;
    @BindView(R.id.employeeID_text)
    TextView employeeIDText;
    @BindView(R.id.relativeLayProfileTop)
    RelativeLayout relativeLayProfileTop;
    @BindView(R.id.profileName)
    EditText profileName;
    @BindView(R.id.relationView)
    View relationView;
    @BindView(R.id.profileGenderSpinner)
    Spinner profileGenderSpinner;
    @BindView(R.id.profileDateOfBirth)
    EditText profileDateOfBirth;
    @BindView(R.id.profileContact)
    EditText profileContact;
    @BindView(R.id.profileLocation)
    EditText profileLocation;
    @BindView(R.id.profileEmail)
    EditText profileEmail;
    @BindView(R.id.bloodGroupSpinner)
    Spinner bloodGroupSpinner;
    @BindView(R.id.profileStatusSpinner)
    Spinner profileStatusSpinner;
    @BindView(R.id.memberBio_editText)
    EditText memberBioEditText;
    @BindView(R.id.linearLayProfile)
    LinearLayout linearLayProfile;
    @BindView(R.id.button_updateProfile)
    Button buttonUpdateProfile;
    @BindView(R.id.relationLinear)
    LinearLayout relationLinear;

    File imageFile;
    private int size;

    private String mUriString = "";
    private Uri mFileURI = null;

    int c = 0;
    ApiInterface apiInterface;
    private ProgressDialog progressDialog;
    private ArrayList<String> maritialSatus, bloodGroup, genderList;
    private ArrayAdapter<String> bloodAdapter, statusAdapter, genderAdapter;
    private String selectedName, selectedDob, selectedMobileNo, selectedLocation, selectedEmail,
            selectedBloodGroup, selectedBio, selectedGender, selectedStatsu;

    MyProfilePresenter myProfilePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        myProfilePresenter = new MyProfilePresenterImp(this, new MyProfileInteractorImp(), ProfileActivity.this);

        relationLinear.setVisibility(View.GONE);
        prepareSpiinerList();
        profileActivityViews();

        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        findViewById(R.id.relationView).setVisibility(View.GONE);

        apiInterface = new Preference().getInstance();

        card1profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, SELECT_FILE);
            }
        });

        buttonUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateUserData();
            }
        });

        profileStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedStatsu = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bloodGroupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedBloodGroup = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        profileGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedGender = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*Action bar and profile selection*/
        sideMenu.setVisibility(View.GONE);
        backToHomeMenu.setVisibility(View.VISIBLE);
        appTitle.setText("Profile");
        backToHomeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        /*PROFILE SELECTION*/
        profileRecycleView.setHasFixedSize(false);
        profileRecycleView.setFocusable(false);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        profileRecycleView.setLayoutManager(layoutManager2);
        profileRecycleView.setItemAnimator(new DefaultItemAnimator());
        profileRecycleView.setNestedScrollingEnabled(false);
        setHeaderData(profileRecycleView);

        selectProfileTextView.setText(ProfileData.getMemberProfile(this));//get last set profile name
        String userId = Preference.getString(ProfileActivity.this, USER_ID);
        myProfilePresenter.getPreviousUserData(userId);
        // getPreviousData(userId);
        selectedProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfileLayout.setVisibility(View.VISIBLE);
                c++;
                if (c % 2 == 0) {
                    selectProfileLayout.setVisibility(View.GONE);
                } else {
                    selectProfileLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        homeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, HomeActivity.class));
            }
        });
        /**/

        uploadProfileImageImageView.setVisibility(View.GONE);
    }

    private void updateUserData() {

        selectedName = profileName.getText().toString();
        selectedDob = profileDateOfBirth.getText().toString();
        selectedMobileNo = profileContact.getText().toString();
        selectedLocation = profileLocation.getText().toString();
        selectedEmail = profileEmail.getText().toString();
        selectedBio = memberBioEditText.getText().toString();

        if (selectedName.length() == 0) {
            Toast.makeText(ProfileActivity.this, "Please enter Name", Toast.LENGTH_SHORT).show();
        } else if (selectedGender.length() == 0) {
            Toast.makeText(ProfileActivity.this, "Please enter Gender", Toast.LENGTH_SHORT).show();
        } else if (selectedMobileNo.length() == 0) {
            Toast.makeText(ProfileActivity.this, "Please enter Contact Number", Toast.LENGTH_SHORT).show();
        } else if (selectedEmail.length() == 0) {
            Toast.makeText(ProfileActivity.this, "Please enter Email", Toast.LENGTH_SHORT).show();
        } else {
            // progressDialog.show();
            updateUserProfile();

        }
    }

    private void updateUserProfile() {

        String userId = Preference.getString(ProfileActivity.this, PrefManager.USER_ID);
        MultipartBody.Part fileToUpload = null;
        if (imageFile != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("*image/*"), imageFile);

            fileToUpload = MultipartBody.Part.createFormData("memberImage", imageFile.getName(), requestBody);
        }

        RequestBody userIdBody = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), selectedName);
        RequestBody relationBody = RequestBody.create(MediaType.parse("text/plain"), "self");
        RequestBody genderBody = RequestBody.create(MediaType.parse("text/plain"), selectedGender);
        RequestBody dobBody = RequestBody.create(MediaType.parse("text/plain"), selectedDob);
        RequestBody mobileBody = RequestBody.create(MediaType.parse("text/plain"), selectedMobileNo);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), selectedLocation);
        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), selectedEmail);
        RequestBody bloodBody = RequestBody.create(MediaType.parse("text/plain"), selectedBloodGroup);
        RequestBody statusody = RequestBody.create(MediaType.parse("text/plain"), selectedStatsu);
        RequestBody bioBody = RequestBody.create(MediaType.parse("text/plain"), selectedBio);


        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put("userId", userIdBody);
        hashMap.put("memberName", nameBody);
        hashMap.put("relation", relationBody);
        hashMap.put("gender", genderBody);
        hashMap.put("dateOfBirth", dobBody);
        hashMap.put("contactNo", mobileBody);
        hashMap.put("location", locationBody);
        hashMap.put("memberEmail", emailBody);
        hashMap.put("bloodGroup", bloodBody);
        hashMap.put("maritalStatus", statusody);
        hashMap.put("bio", bioBody);
        hashMap.put("memberId", userIdBody);


        String REQUEST_URL = "editMember.php";
        myProfilePresenter.updateUserProfile(REQUEST_URL, fileToUpload, hashMap);

    }

    private void prepareSpiinerList() {

        maritialSatus = new ArrayList<>();
        bloodGroup = new ArrayList<>();

        genderList = new ArrayList<>();

        maritialSatus.add("Single");
        maritialSatus.add("Married");

        bloodGroup.add("O-");
        bloodGroup.add("O+");
        bloodGroup.add("A-");
        bloodGroup.add("A+");
        bloodGroup.add("B-");
        bloodGroup.add("B+");
        bloodGroup.add("AB-");
        bloodGroup.add("AB+");


        genderList.add("Male");
        genderList.add("Female");

        bloodAdapter = new ArrayAdapter<>(ProfileActivity.this, android.R.layout.simple_list_item_1, this.bloodGroup);
        bloodGroupSpinner.setAdapter(bloodAdapter);

        statusAdapter = new ArrayAdapter<>(ProfileActivity.this, android.R.layout.simple_list_item_1, this.maritialSatus);
        profileStatusSpinner.setAdapter(statusAdapter);

        genderAdapter = new ArrayAdapter<>(ProfileActivity.this, android.R.layout.simple_list_item_1, this.genderList);
        profileGenderSpinner.setAdapter(genderAdapter);

        //  setSpinnerData(maritialSatus, bloodGroup, genderList);
    }


    private void profileActivityViews() {


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ProfileActivity.this, HomeActivity.class));
        overridePendingTransition(0, 0);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            mFileURI = data.getData();

            Uri uri = data.getData();


            String realPath = ImageFilePath.getPath(ProfileActivity.this, data.getData());
//                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            Log.i("Peofile TAG", "onActivityResult: file path : " + realPath);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));


                uploadProfileImageImageView.setVisibility(View.GONE);
                Glide.with(this).load(mFileURI).into(userImageView);

                if (realPath != null) {
                    imageFile = new File(realPath);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            final String docId;
           /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                docId = DocumentsContract.getDocumentId(mFileURI);

                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                String filePath = getPath(ProfileActivity.this, contentUri);

                uploadProfileImageImageView.setVisibility(View.GONE);
                Glide.with(this).load(mFileURI).into(userImageView);

                if (filePath != null) {
                    imageFile = new File(filePath);

                }
            } else {


                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                int columnindex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String file_path = cursor.getString(columnindex);
                String file_path1 = getRealPathFromURI(mFileURI);
                Log.d(getClass().getName(), "file_path" + file_path);
                Uri fileUri = Uri.parse("file://" + file_path);
                cursor.close();

                uploadProfileImageImageView.setVisibility(View.GONE);
                Glide.with(this).load(mFileURI).into(userImageView);

                if (file_path1 != null) {
                    imageFile = new File(file_path1);

                }


                try {
                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    //saveImageFile(bm);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/


        }
    }

    private String getRealPathFromURI(Uri mFileURI) {

        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(mFileURI, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;

    }


    private String getFilename() {
        File file = new File(Environment.getExternalStoragePublicDirectory("HealthSol"), "userProfile");
        if (file.exists()) {
            file.delete();
        }
        file.mkdir();
        imageFile = file;
        mUriString = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");

        Log.e("uri string", mUriString);
        return mUriString;

    }

    @Override
    public void recivedUserData(ShowMemberList showMemberList) {

        if (showMemberList != null) {
            String memberName = showMemberList.getMemberName();
            String memberId = showMemberList.getMemberId();
            String relation = showMemberList.getRelation();
            String gender = showMemberList.getGender();
            String DateOfBirth = showMemberList.getDateOfBirth();
            String contactNo = showMemberList.getContactNo();
            String location = showMemberList.getLocation();
            String email = showMemberList.getEmail();
            String bloodGroup = showMemberList.getBloodGroup();
            String maritalStatus = showMemberList.getMaritalStatus();
            String bio = showMemberList.getBio();
            String memberImage = showMemberList.getMemberImage();

            bloodGroupSpinner.setSelection(bloodAdapter.getPosition(bloodGroup));
            profileStatusSpinner.setSelection(statusAdapter.getPosition(maritalStatus));
            profileGenderSpinner.setSelection(genderAdapter.getPosition(gender));

            profileName.setText(memberName);
            profileDateOfBirth.setText(DateOfBirth);
            profileContact.setText(contactNo);
            profileLocation.setText(location);
            profileEmail.setText(email);
            memberBioEditText.setText(bio);

            uploadProfileImageImageView.setVisibility(View.GONE);
            if (memberImage == null || memberImage.equals(" ")) {
                userImageView.setImageResource(R.drawable.ic_camera_alt_black_36dp);
            } else {
                Glide.with(ProfileActivity.this).load(memberImage).into(userImageView);
            }
        }

    }

    @Override
    public void showViewProgress() {
        showProgress();
    }

    @Override
    public void hideViewProgress() {

        hideProgress();
    }

    @Override
    public void onFailed(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateSuccecss(String msg) {

        Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

}

