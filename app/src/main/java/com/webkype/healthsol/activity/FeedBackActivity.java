package com.webkype.healthsol.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webkype.healthsol.R;

public class FeedBackActivity extends AppCompatActivity {

    private ImageView reviewerPic, backToHomeActionBar;
    private TextView titleActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        reviewerPic = findViewById(R.id.reviewerPic);
        titleActionBar = findViewById(R.id.titleActionBar);
        backToHomeActionBar = findViewById(R.id.backToHomeActionBar);

        titleActionBar.setText("FeedBack");
        Glide.with(this).load("https://www.peproo.com/profilePhoto/11523791070_gajendra-singh%20copy.JPG").into(reviewerPic);

        backToHomeActionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FeedBackActivity.this, HomeActivity.class));
        overridePendingTransition(0,0);
        this.finish();
    }
}
