package com.webkype.healthsol.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.webkype.healthsol.R;
import com.webkype.healthsol.adapter.MedicineImagesAdapter;
import com.webkype.healthsol.model.PdfDocumentListPojo;

import java.util.ArrayList;

public class MedicineImagesActivity extends AppCompatActivity {


    private ViewPager viewPager;
    private ImageView nextImg, prevImg;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_images);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Images");

        viewPager = findViewById(R.id.gelleryPager22);
        prevImg = findViewById(R.id.prevImg);
        nextImg = findViewById(R.id.nextImg);
        ArrayList<PdfDocumentListPojo> listPojos = getIntent().getParcelableArrayListExtra("documentList");

        MedicineImagesAdapter medicineImagesAdapter = new MedicineImagesAdapter(MedicineImagesActivity.this, listPojos);
        viewPager.setAdapter(medicineImagesAdapter);


        nextImg.setOnClickListener(view -> {

            if (count < 12) {
                count = count + 1;
                viewPager.setCurrentItem(count);
            }

        });

        prevImg.setOnClickListener(view -> {

            if (count > 0) {
                count = count - 1;
                viewPager.setCurrentItem(count);
            }

        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

